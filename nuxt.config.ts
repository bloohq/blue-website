// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  // Site metadata configuration
  site: {
    url: 'https://www.blue.cc',
    name: 'Blue',
    description: 'Simple Online Project Management Software',
    defaultLocale: 'en', // not needed if you have @nuxtjs/i18n installed
  },
  vite: {
    plugins: [
      {
        // Add default alt attribute to images
        name: 'add-default-alt',
        transformIndexHtml(html) {
          return html.replace(/<img((?!alt=)[^>])*?>/gi, (match) => {
            if (match.includes('alt=')) return match;
            const src = match.match(/src="([^"]*)"/)?.[1] || '';
            const filename = src.split('/').pop()?.split('.')[0] || '';
            const defaultAlt = filename.replace(/[-_]/g, ' ');
            return match.replace('>', ` alt="${defaultAlt}">`);
          });
        },
      },
    ],
  },
  //Nuxt UI Configuration
  ui: {
    safelistColors: ['brand-blue']
  },
  hooks: {
    // Define `@nuxt/ui` components as global to use them in `.md` (feel free to add those you need)
    'components:extend': (components) => {
      const globals = components.filter(c => ['UButton', 'UIcon'].includes(c.pascalName))

      globals.forEach(c => c.global = true)
    }
  },

  colorMode: {
    disableTransition: true
  },

  // Content module configuration
  content: {
    markdown: {
      anchorLinks: true, // Enable anchor links in markdown
      tags: {
        youtube: 'YouTubeWrapper'
      }
    },
    documentDriven: false, // Disable document-driven mode
    api: {
      baseURL: '/resources' // Set base URL for content API
    },
  },

  // App configuration
  app: {
    head: {
      __dangerouslyDisableSanitizers: ['script'], // Disable sanitizers for scripts (use with caution)
      link: [
        { rel: 'stylesheet', href: '/inter.css' }, // Add Inter font stylesheet
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }, // Favicon
        { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
        { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
        { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/favicon-192x192.png' },
      ],
      meta: [
        // Open Graph meta tags for social media sharing
        { property: 'og:site_name', content: 'Blue' },
        { property: 'og:type', content: 'website' },
        { property: 'og:image', content: 'https://www.blue.cc/og.png' },
        // { property: 'og:title', content: 'Blue - Simple Online Project Management Software' },
        // { property: 'og:description', content: 'Streamline your projects with Blue, the intuitive online project management tool for teams of all sizes.' },
        { property: 'og:url', content: 'https://www.blue.cc' },
        // Twitter Card meta tags
        { name: 'twitter:card', content: 'summary_large_image' },
        // { name: 'twitter:title', content: 'Blue - Simple Online Project Management Software' },
        // { name: 'twitter:description', content: 'Streamline your projects with Blue, the intuitive online project management tool for teams of all sizes.' },
        { name: 'twitter:image', content: 'https://www.blue.cc/og.png' },
      ],
      script: [
        // Rewardful script
        {
          hid: 'rewardful-js',
          src: 'https://r.wdfl.co/rw.js',
          'data-rewardful': '2b826f',
          async: true,
          defer: true,
        },
        // Google Analytics script
        {
          hid: 'gtag-js',
          src: 'https://www.googletagmanager.com/gtag/js?id=G-LRK1HBRGTQ',
          async: true,
          defer: true
        },
        // Inline Google Analytics configuration
        {
          hid: 'gtag-inline',
          innerHTML: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-LRK1HBRGTQ');
          `,
          type: 'text/javascript',
          charset: 'utf-8'
        },
        {
          hid: 'clarity-js',
          innerHTML: `
            (function(c,l,a,r,i,t,y){
              c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
              t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
              y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
            })(window, document, "clarity", "script", "ktk2srt3zn");
          `,
          type: 'text/javascript'
        }
      ],
      // Disable sanitizer for specific script tag
      __dangerouslyDisableSanitizersByTagID: {
        'gtag-inline': ['innerHTML'],
        'clarity-js': ['innerHTML']
      }
    },
  },

  // Route rules for redirects
  routeRules: {
    '/releases': { redirect: '/platform/changelog', statusCode: 301 },
    '/platform/releases': { redirect: '/platform/changelog', statusCode: 301 },
    '/changelog': { redirect: '/platform/changelog', statusCode: 301 },
    '/blog': { redirect: '/insights', statusCode: 301 },
    '/insights': { redirect: '/resources/insights', statusCode: 301 },
    '/blog/deep-work': { redirect: '/insights/deep-work', statusCode: 301 },
    '/insights/deep-work': { redirect: '/resources/deep-work', statusCode: 301 },
    '/blog/great-teamwork': { redirect: '/insights/great-teamwork', statusCode: 301 },
    '/insights/great-teamwork': { redirect: '/resources/great-teamwork', statusCode: 301 },
    '/terms': { redirect: '/legal/terms', statusCode: 301 },
    '/roadmap': { redirect: '/platform/roadmap', statusCode: 301 },
    '/privacy': { redirect: '/legal/privacy', statusCode: 301 },
    '/privacy-policy': { redirect: '/legal/privacy', statusCode: 301 },
    '/security': { redirect: '/platform/security', statusCode: 301 },
    '/platform/security-scalability': { redirect: '/platform/security', statusCode: 301 },
    '/platform/automations': { redirect: '/platform/project-management-automation', statusCode: 301 },
    '/resources/key-features-real-estate-project-management-software': { redirect: '/resources/real-estate-pm-features', statusCode: 301 },
    '/resources/tips-&-tricks': { redirect: '/resources/tips-tricks', statusCode: 301 },
    '/resources/frequently-asked-questions': { redirect: '/resources/faqs', statusCode: 301 },
    '/alternatives/trello': { redirect: '/alternatives/trello-alternative', statusCode: 301 },
    '/alternatives/basecamp': { redirect: '/alternatives/basecamp-alternative', statusCode: 301 },
    '/resources/agency-success-guide': { redirect: '/resources/agency-success-playbook', statusCode: 301 },
    '/docs': {redirect: '/docs/welcome', statusCode: 301},
    '/api-docs': {redirect: '/api-docs/introduction', statusCode: 301},
    '/docs/dashboards': { redirect: '/docs/dashboards/introduction', statusCode: 301 },
    '/docs/custom-fields/introduction': { redirect: '/docs/custom-fields', statusCode: 301 },
    '/docs/automations/introduction': { redirect: '/docs/automations', statusCode: 301 },
    '/eran': { redirect: 'https://eran.link/blue', statusCode: 301 },
  },

  // Disable OG image generation
  ogImage: {
    enabled: false
  },

  // Disable link checker
  linkChecker: {
    enabled: false
  },

  // Enable Nuxt DevTools
  devtools: { enabled: true },

  // Nuxt modules
  modules: [
    // "@nuxtjs/tailwindcss", // TailwindCSS integration
    "@nuxtjs/seo", // SEO optimization
    '@nuxt/image', // Image optimization
    'nuxt-icon', // Icon support
    '@nuxt/content', // Content management
    '@nuxt/ui', // NuxtUI
  ],
  extends: ['@nuxt/ui-pro'],
  // Image module configuration
  image: {
    screens: {
      'xs': 320,
      'sm': 640,
      'md': 768,
      'lg': 1024,
      'xl': 1280,
      'xxl': 1536,
      '2xl': 1536
    },
    format: ['webp', 'png', 'jpeg'], // Supported image formats
  },
})