---
title: HIPAA
description: Details on how Blue handles Protected Health Information (PHI) in accordance with HIPAA regulations.
---

## 1. Introduction

This HIPAA Privacy Policy explains how Bloo, Inc. ("we", "us", or "Blue") protects the privacy and security of Protected Health Information (PHI) in accordance with the Health Insurance Portability and Accountability Act of 1996 (HIPAA) and its implementing regulations. This policy applies to our website www.Blue.cc and our B2B SaaS platform when used to handle PHI.

## 2. Definitions

- **Protected Health Information (PHI)**: Any information about health status, provision of health care, or payment for health care that can be linked to a specific individual.
- **Covered Entity**: Health care providers, health plans, and health care clearinghouses that transmit health information electronically.
- **Business Associate**: A person or entity that performs certain functions or activities that involve the use or disclosure of PHI on behalf of, or provides services to, a Covered Entity.

## 3. Our Role

Blue acts as a Business Associate to Covered Entities when our B2B SaaS platform is used to handle PHI. We are committed to complying with HIPAA regulations in this capacity.

## 4. PHI We May Handle

As a Business Associate, we may handle various types of PHI, including but not limited to:

- Patient names
- Addresses
- Dates (birth, admission, discharge, etc.)
- Phone numbers
- Email addresses
- Medical record numbers
- Account numbers
- Health plan beneficiary numbers

## 5. Use and Disclosure of PHI

We will only use or disclose PHI as permitted by our Business Associate Agreement with the Covered Entity and in compliance with HIPAA regulations. This may include:

- Providing our B2B SaaS platform services
- Conducting data analysis to improve our services
- Performing system maintenance and troubleshooting

We will not use or disclose PHI for marketing purposes or sell PHI unless explicitly authorized by the Covered Entity and the individual.

## 6. Data Security Measures

We implement robust security measures to protect PHI, including:

- Enterprise-level encryption (AES-256) for data at rest and in transit
- Advanced monitoring and alert systems
- Multi-factor authentication (MFA) for backend systems
- Regular third-party security audits
- Daily data backups
- Collaboration with external security researchers

## 7. Employee Training and Access

All our employees receive regular training on HIPAA compliance. Access to PHI is restricted to authorized personnel on a need-to-know basis.

## 8. Data Retention

We retain PHI only for as long as necessary to provide our services or as required by law. Once PHI is deleted, we keep it for 30 days before permanent deletion.

## 9. Data Storage and Transfer

PHI is stored encrypted at rest in AWS data centers. If we transfer PHI outside the United States (e.g., to Singapore), we ensure appropriate safeguards are in place and comply with all applicable laws and regulations.

## 10. Breach Notification

In the event of a breach of unsecured PHI, we will notify affected Covered Entities without unreasonable delay and in no case later than 60 calendar days after discovery of the breach.

## 11. Individual Rights

We will assist Covered Entities in fulfilling their obligations to provide individuals with their rights under HIPAA, including:

- Right to access their PHI
- Right to request amendments to their PHI
- Right to an accounting of disclosures
- Right to request restrictions on use and disclosure of their PHI
- Right to request confidential communications

Individuals should contact their healthcare provider (the Covered Entity) to exercise these rights.

## 12. Business Associate Agreements (BAAs)

As a Business Associate, Blue is committed to entering into Business Associate Agreements (BAAs) with Covered Entities as required by HIPAA. If you are a Covered Entity and wish to use our services for handling Protected Health Information (PHI), you will need to have a signed BAA with us.

To request a BAA:

1. Contact our sales team at sales@blue.cc or through our website contact form.
2. Specify that you are a Covered Entity requiring a BAA for HIPAA compliance.
3. Provide your organization's name and contact information.
4. Our team will respond within 2 business days with our standard BAA or to discuss any custom requirements.

Please note:
- We use a standard BAA template that has been reviewed by our legal team for HIPAA compliance.
- Any modifications to our standard BAA may require additional review and approval.
- We recommend that you have the BAA reviewed by your own legal counsel before signing.
- A signed BAA must be in place before any PHI is processed through our platform.

## 13. Changes to This Policy

We may update this HIPAA Privacy Policy from time to time. We will notify Covered Entities of any significant changes by posting the new Privacy Policy on this page. You can find all the version controlled changes on our [Gitlab Repository](https://gitlab.com/bloohq/blue-website)

## 14. Contact Us

If you have any questions about this HIPAA Privacy Policy, our data practices, or our BAA process, please contact our Privacy Officer:

Emanuele FAJA, CEO
Email: help@blue.cc