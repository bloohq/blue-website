---
title: Chat
description: Create on-topic chat discussions in each project.
---

Chats allow members to communicate with each other within the platform. It is a way to discuss topics, share updates, ask questions, and collaborate.

::callout
---

icon: i-heroicons-light-bulb
---

Note that Blue supports topic-based chats within projects, but not direct messages to individuals.
::

## Creating Chats

To start a new chat:
1. Click on the Chat icon in the left sidebar.
2. Click the "New Chat" button at the top.
3. Click "Create Chat" when ready.

You can create multiple chats to discuss different topics.

![Chat 1](/docs/projects/chat/Chat_1.png "Chat 1")

Use the "+" button to create a new chat and set your topic name

## Communicating in Chats

Once in a chat, members can:
- Send text messages
- Upload images by dragging and dropping files
- @mention other members to notify them
- Embed YouTube videos by pasting the video URL

![Chat 2](/docs/projects/chat/Chat_2.png "Chat 2")

Upload images and keep discussions in context

Chats allow fluid, ongoing discussions without lengthy email threads.

## Chat Features

- Search chat messages
- Edit earlier messages
- Delete messages

Using Blue's chats keeps key conversations and shared files organized in one place. With integrated notifications and mentions, they facilitate seamless team communication.
