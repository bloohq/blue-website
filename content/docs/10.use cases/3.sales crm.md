---
title: Sales CRM
description: 
---

Blue provides a robust set of tools to manage your sales workflow, giving you a powerful CRM system tailored to your needs.

## Sales Pipeline Management

Leverage Blue's **lists** to model your sales funnel by creating stages. As deals progress, sales reps can drag & drop records between lists to advance them down the pipeline. This will also provides entire team visibility into sales pipeline and key client information

**Sales Flow**

- Lead
- Qualified Lead
- In Contact
- Follow Up
- Closed Won
- Closed Lost

![Sales CRM  1](/docs/useCases/salesCRM/1.png "Sales CRM  1")

## Centralized Contact Database

Create a Contact custom field to store all your prospect and customer information. All contacts become easily accessible records. Switch between boards and database for viewing leads at a glance.

Storing sales database

- **Single Select Field**: Contact Person  
- **Phone Number Field**:  Phone number  
- **Email Field**: Contact Email  
- **Currency Field**: Client's budget
- **Files Field**: For contract, signed documents, client preferences.

![Sales CRM  1](/docs/useCases/salesCRM/2.png "Sales CRM  1")

## Activity Tracking & Team Communication

Log all sales activities like calls, emails, and meetings via the  comments on contact records. Follow up between team members to stay up to date to what the latest client's update.

![Sales CRM  3](/docs/useCases/salesCRM/3.png "Sales CRM  3")

## Email Integration

Sync email automatically into associated contact records. Email right from Blue using custom contact fields like {Email} and {FirstName} in the body.

Set triggers and send out email automation to communicate with clients for any quotation updates, informing clients on members that are active on the proposal.

- When a record is moved to "**Follow up**" list then send email to client.
- When a record is moved to "**Closed Won**" list then assign member to email client for aftercare.

![Sales CRM  4](/docs/useCases/salesCRM/4.png "Sales CRM  4")

## Reporting & Analytics

Build real-time sales reports on the dashboards to track deal volume, sales cycle analytics, win/loss metrics, activity frequency, and more using filters for analysis.

With custom fields tailored to your sales data, selectively shared access to confidential deal information, automation to drive productivity, and advanced reporting from multiple projects, Blue provides a complete sales CRM environment enhancing your workflow.
