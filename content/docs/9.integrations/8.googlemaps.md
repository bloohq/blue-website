---
title: Google Maps
description: Use Google Maps for the map view in Blue.
---

Blue allows you to use Google Maps for the map view instead of the default OpenStreetMap. Google maps often has more detailed maps of certain areas and more accurate location data. However, this requires a Google Maps API key and has to be entered in the company settings by the company owner. 

When you switch to Google maps, this effects all map views in Blue including the company map, the [map view in projects](/docs/views/map), and the [location custom field](/docs/custom-fields/location-map).

<video autoplay loop muted playsinline>
  <source src="/docs/integration/google-maps.mp4" type="video/mp4">
</video>

To use Google Maps in Blue, the following APIs are required to be turned on in Google Cloud Console:
- [Maps Tiles API](https://console.cloud.google.com/apis/library/tile.googleapis.com): This is required to render the map tiles.
- [Places API](https://console.cloud.google.com/apis/library/places.googleapis.com): This is required to search for places and get their coordinates.
- [Geocoding API](https://console.cloud.google.com/apis/library/geocoding-backend.googleapis.com): This is required to get the coordinates of a place.

You will need to sign up for a [Google Cloud account](https://console.cloud.google.com/) and create a project to get the API keys following the instructions [here](https://developers.google.com/maps/documentation/embed/get-api-key).

## Restricting API Key Usage

For security purposes, it's crucial to restrict your Google Maps API key to only work with authorized domains. This prevents unauthorized usage of your API key if it's exposed. You should restrict the API key to only work with:

- app.blue.cc - The main Blue application
- forms.blue.cc - For public forms
- Your custom domain if you're using [white label](/docs/company/white-label)

To restrict your API key:

1. Go to the [Google Cloud Console Credentials page](https://console.cloud.google.com/apis/credentials)
2. Find your API key and click "Edit" (pencil icon)
3. Under "Application restrictions", select "HTTP referrers (websites)"
4. Add the following referrers:
   - `*.app.blue.cc/*`
   - `*.forms.blue.cc/*`
   - `*.yourdomain.com/*` (if using white label)
5. Click "Save"


::callout
---

icon: i-heroicons-exclamation-triangle
---

**Important Note on Google Maps Charges**<br><br>

Google will charge you for using the Google Maps API separately from your Blue subscription. All charges, expenses, and fees associated with your Google Maps account and API usage are the sole responsibility of the organization administrator. Blue is not responsible for any charges incurred through your use of the Google Maps API, including but not limited to third-party integrations, API usage, or storage costs.


::




