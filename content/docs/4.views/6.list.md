---
title: List
description: The List View offers a simple checklist for managing records and tracking progress.
---


Records are displayed in a convenient checklist format with checkboxes for marking completion. You can scroll infinitely through lists containing thousands of records. 

![List](/docs/views/list/List.png "List")

The list allows sorting by record name, date, assignee, and more to help focus on relevant items. 

Additionally, [filters](/docs/views/filters) can be applied to narrow down records by timeline, assignee, tag, and any custom field.

## Limitations 

While the List View is a convenient tool for managing records, it does have some limitations. Records cannot be created directly in the list view, and users are unable to add, remove, or change the order of columns. For those seeking a more powerful version of the list view, the [Database View](/docs/views/database) is recommended as it offers greater flexibility and functionality.





