---
title: AI Content Summarization
description: 
---

AI Summaries leverages artificial intelligence to automatically summarize long comment threads within records and generate an action list. This is designed to help you easily keep track of important information, even in lengthy discussions.

![AI Content Summarization](/docs/records/AiContent/AIContentSummarization.png "AI Content Summarization")

To access the AI Summaries feature, follow these simple steps:

1. **Navigate to a Record**: Open any record within a project where you want to generate a summary of the comments and discussion.
2. **Click the Lightning Bolt Icon**: In the top-right corner of the record, you'll see a Lightning Bolt ⚡️ icon. Click on this icon to initiate the AI Summaries feature.
3. **Generate the Summary**: After clicking the Lightning Bolt icon, Blue's AI will analyze the entire comment thread and discussion within the record. It will then generate a concise summary, capturing the key points and action items discussed.
4. **Review the Summary**: The AI-generated summary will be displayed in the comment section within the record. This summary will provide an overview of the conversation, highlighting the main topics, decisions, and action items discussed.
5. **Action Item List**: In addition to the summary, the AI will also generate a list of action items based on the comments and discussion. This list will help you quickly identify tasks or follow-up actions that need to be addressed.
6. **Customize and Edit**: While the AI Summaries feature aims to provide an accurate and comprehensive summary, you have the ability to customize and edit the generated content if needed. This ensures that the summary aligns with your specific requirements and understanding of the discussion.

By leveraging the AI Summaries feature, you can quickly digest lengthy comment threads and discussions, saving time and effort in tracking important information and action items.
