---
title: Introduction
description: Records are the core building blocks in Blue that allow you to track anything - from tasks and projects to sales and support tickets. 
---

## Overview

Records in Blue serve two key purposes:

1. **Tracking Work Items**: You can use records as dynamic to-do lists to track the progress of tasks, projects, sales deals etc. Assign due dates, tags, checklists and custom fields to capture all relevant information.
2. **Organizing Information**: Records allow you to structure information by using lists as categories. For example, you may have an "Open Leads" and "Closed Deals" list to organize your sales pipeline.

## Key Features

Here are some of the key features of records in Blue:
- [Lists](/docs/records/lists): Sort records into different lists to categorize information. For example, have "To Do", "In Progress" and "Completed" lists.
- [Custom Fields](/docs/custom-fields): Add custom fields like dropdowns, numbers, and dates to capture specialized information. For example, add a "Lead Score" number field.
- [Tags](/docs/records/tags): Use colorful tags like "Urgent" or "Bug" to highlight priority records.
- [Comments](/docs/records/comments): Collaborate with your team by commenting directly on records.
- [Attachments](/docs/projects/file-management): Add files, images, videos directly to records for easy access.
- [Automations](/docs/automations): Trigger automations when records are created, edited or deleted. For example, send an email alert when a high priority record is generated.
- **Colors**: Set colors to your records to identify the urgency of the records or for content planning.

![Introduction](/docs/records/introduction/Introduction.png "Introduction")

Record Colors

## Use Cases

Here are some examples of how you can use records in your organization:

### Task and Project Management

Use records to track the progress of tasks and projects with assignees, due dates, attachments etc. Group them into lists like "Backlog", "In Progress" and "Live" to visualize workflow.

### Sales Pipeline

Manage your sales pipeline by having records for each lead and opportunity. Track deal value, lead score and next steps in custom fields. Organize records into lists like "Open Deads" and "Closed Won" to analyze your funnel.

### Support Tickets

Records can act as support tickets to track customer issues. Agents can be assigned tickets, ask clarifying questions in comments, and update status in a custom field. Group tickets by type using tags like "Bug", "Question" etc.

### Recruitment

In your hiring process, have a record for every candidate application with key information in custom fields. Move them through lists like "Applied", "Phone Screen" and "Final Round" to track progression. Use email automation to send updates to candidate when record is moved to another list.

So in summary, records give you a flexible way to build a custom workflow that matches your business process. With features like lists, custom fields and automations, you can build some powerful systems on top of records in Blue.

::callout
---

icon: i-heroicons-light-bulb
---

Records are specific to a project. In the future, we aim to have the ability to have one record available across projects.
::

