--- 
title: The Problem With Overworking
slug: problem-overworking
tags: ["insights", "modern-work-practices"]
description: There is a disturbing trend lately that is fetishizing long hours to reach success. This is an issue because hard, long hours and results are not correlated.
image: /patterns/chevron3.png
sitemap:
  loc: /resources/problem-overworking
  lastmod: 2024/06/01
  changefreq: monthly
---


Searching for #hustle on Instagram currently gives around 20M results, which tells you a lot. 

However, hard work, hustle, and grind is not, by itself, a recipe for success. The key to making progress as a team is not counting the number of hours and using peer pressure to force Jack Ma’s 996 culture.

**It’s about working smart.**

The truth about modern knowledge work, is that it actually doesn’t scale very well. We’re not cranking widgets in a factory, we’re not painting walls, we _thinking up solutions to problems that may not have a straight forward answer._

I have a story of when I was in charge of running a large mobile application project for a client as an agency, and they wanted us to run development 24/7 by hiring more people and working in shifts. I refused to do so, and we ended up not working together a few months after that. This client then told us that they would launch a v2 of their application in three months. At the time of writing, nine months have passed and there is no sign of the new application.

The reason why knowledge work does not yield to long hours or shift work is because the results created by different teams are not linear, and are not strongly correlated to the amount of time spent.

A great designer or engineer can solve problems an order of magnitude (that’s 10x!) better than a mediocre designer or engineer. And that’s just a direct comparison! If you factor in a _team _of great designers or developers, we can literally say that the difference is 100x.

That’s the difference between shipping a great product that is a commercial successful and that customers love to use, vs not shipping anything at all (which happens a surprisingly large amount of the time!)

In addition, you may also want to think about how people change over time, especially _long_ periods of time. If your team is working a healthy amount of hours per week (let’s say 35 or 40) they then have time to actually live their lives, ups-kill themselves, take up new experiences, and this leads to them become better people, which will directly translate to them being more productive.

Compare this to the poor souls that are grinding 9am to 9pm six days a week, and they hardly have time to shower, let alone to get some perspective on life and the work they are actually doing.

And sometimes, the realization might come that actually what we are doing is not worth doing. Take Google+, for instance, the failed social network by Google.  They rushed building it, threw 500 people at it for a couple of years, and on some estimates spent $600m to $800m on trying to make this work.

So the total final productivity of those 500 people during that time was pretty much zero.

**Don’t let that be you.**

