---
title: Keep It Simple
slug: keep-it-simple
tags: ["agency-success-guide"]
description: In this chapter, we explain why simple processes are often much better than more academic, complex business processes.
image: /resources/madentrance.jpg
date: 12/06/2024
showdate: false
sitemap:
  loc: /resources/keep-it-simple
  lastmod: 2024/06/12
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---

When I was ten years old, I had a maths teacher who taught me the principle of KISS: “Keep It Simple, Stupid!”. I have found that this lesson is broadly applicable, not only when trying to solve quadratic equations, but across almost every part of life.

Simplicity and complexity is something that I find very binary. There is no shade of grey: you either have a simple approach to something, or you can choose to make it complicated. And once you do decide to make it complicated, you’ve jumped off a cliff. You’ll be hurtling down faster than you can cope toward further complexity.

You might think to yourself, *“I’m quite smart, and I like to have nuance with what I am doing, so I can handle complexity.”* 

And you’re right — in any specific area of your work, you probably can handle complexity. 

You can have a sophisticated hiring process with various steps and procedures and flowcharts, and easily stay on top of it without any problems at all. But what about all the other hundreds of processes that make up a modern organization? Can you stay on top of it all, including the various linkages and dependencies?

My test to see if something is too complicated is whether I can explain it to a junior team member. If they understand it, then we are keeping things simple and are on the right track. If it is too complex and they cannot quickly understand it, then we’ve probably over-engineered a solution to a problem. We need to go back to the drawing board and understand how we can simplify, what is superfluous, what can be removed without much consequences.

I often find that I can get 70% to 80% of what I want from any specific process with surprising ease. But, if I insist on getting 100% of what I want, things starts to get significantly more complicated. So, I’ve learned to settle at that comfortable 75% mark and enjoy the fact that things are kept simple.

Another thing that will make embracing simplicity come more naturally is being a small organization. This might sound counterintuitive: after all, you’ve put so much time and effort into starting your firm — why should you not expand? 

Admittedly, most successful businesses are large, with different departments, bigger teams, and fancy corporate offices.

This used to be the way I thought, too. My initial vision for Mäd was to employ hundreds of team members in half a dozen offices across South-East Asia.

So I tried it, twice.

Aggressively scaling, pushing sales, and hiring new team members. I once had an onboarding day with eleven new people on the same day. I even bought another consultancy to add more team members (I’ve found this not to be a good way of keeping things simple, to be honest).

Both times Mäd tried to scale, it almost went bankrupt. Obviously, I now understand why — I didn’t know enough about finances, hiring talented team members, or project management to be effective at that scale. But above all, the company was too big and unwieldy for me to have full visibility and control. I was simply unable to keep track of all its people, processes, and operations.

Large organizations look good on paper with their long lists of clients, impressive resumes of team members, and glossy marketing collateral. They signal success, which can be exhilarating for entrepreneurs and founding teams.

The problem is that size doesn’t necessarily equate to effectiveness. In fact, large organizations often suffer from a number of issues that make them slower, less nimble, and less effective than their smaller counterparts.

To give you an example: both of my organizations, Mäd and [Blue](/), are small companies. They share an office, have small teams, and I know everyone’s name. 

This is not an accident; it is by design.

There were never any major issues when the team count was kept below 25, but both times it went above this, everything seemed to fall apart. But, when we focused on quality instead of quantity, we managed to grow by raising our prices *instead* of our headcount.

The proof of this is that Mäd has had the opportunity to work on hundreds of projects with dozens of clients across a multitude of industries. And I’ve heard so many horror stories of clients spending absurd amounts of money and time working with large consultancies, with very little to show for it. Once they work with Mäd, it is like a breath of fresh air — no bullshit (aka complexity), just a focus on the work at hand (often for a fraction of the price of the large consultancies).

So, I’ve reached the conclusion that there is a strong inverse correlation in consulting firms between the size of the firm and the quality of the services provided. I can tell you confidently that the way to keep things simple is actually by being very deliberate about it.

And you’ll be surprised to find that it might be harder to keep your business simple and straightforward than it is to let it grow and get overly complicated!

This is why trying to build and run a large consultancy is a surefire way to jump off the cliff to complexity:

### Large consultancies are less nimble and agile. 

Because they are so big, it is much harder for them to change direction quickly or pivot when necessary. They are also more bureaucratic, with more layers of management that need to sign off on decisions. This can make them very slow and unresponsive, which can be a huge disadvantage compared to smaller organizations.

### Large consultancies often have a “one-size-fits-all” approach.

They tend to offer the same solutions to all of their clients, regardless of the specific needs of those clients. This “cookie- cutter”approachisnotideal,sinceitdoesn’tallowforcustomization or tailoring of solutions to meet the unique needs of each client.

### Large consultancies can be inflexible and uncompromising.

They often have very rigid processes and procedures in place, which can make them very difficult to work with. This can be a major frustration for clients who need more flexibility and agility from their service providers.

### Large consultancies tend to be more expensive.

Because they have more overhead and higher costs, large consultancies typically charge more for their services than smaller organizations. This can be a major deterrent for clients who are looking to get value for money.

### Large consultancies often have difficulty attracting and retaining top talent. 

The best and brightest employees often prefer to work for smaller, more nimble organizations where they can have a greater impact and make more of a difference. As a result, large consultancies can have difficulty attracting and retaining top talent, which can make it harder for them to provide the best possible service to their clients.

What complicates things even further, on top of all these previous points, is the communication overhead.

Here, we’ll return to the subject of working with humans. In general, how simple or how complex your organization is will depend on factors like the size of the team, individual team members’ working processes, and their output.

There is no 50,000+ employee organization in the world that is made up of a 50,000-person team working harmoniously together. An organization of 50,000 is actually made up of, give or take, 2,000 teams of 25 people each.
And the more teams (and team members) you have, the more communication threads they form.

Paying homage to my maths teacher that I mentioned at the beginning of this chapter, let’s apply that knowledge and run the maths.

We can use this equation to calculate the communication threads within a company.

**n*(n-1)/2 = The Number of Possible Communication Threads**

Where n is the number of people that need to be involved in the project.

Plugging in the numbers, at 50,000 employees, there are 1,249,975,000 possible direct communications threads.

But let’s be generous and assume that it is just the teams that need to coordinate, not each individual. So, we have 2,000 teams, and using our equation, we get 1,999,000 possible direct communications threads.

Note that this is also a simplification because there are multiple available communication channels, so we can easily multiply the above numbers by five.

Ok, but *so what?*

Well, the result is that this incredible overhead in communication means that in large organizations, the left hand doesn’t know what the right hand is doing. And the members of these organizations are aware of this, so they create rules to try and fight this issue — eventually, we end up with highly formalized rituals and regulations to ensure tight-knit communication in projects.

Add in the problem that large organizations tend to have team members spread around the world, and there is the issue of different time zones, so we can see where this is heading...

Lots of bureaucracy and far less work done. Lots of discussions and alignment meetings, but not much deep work by individuals. More management, fewer actual doers.

In short, overwhelming complexity.

### So, how can you counter this?

Make it a priority to cherry-pick and cultivate the best — and smallest — possible team.

When it comes to defining small teams, I like to lean on Jeff Bezos’ Two Pizza Rule: if you cannot feed the entire team with two pizzas, it’s too big. Split some team members off and create a separate team, or rethink how you are doing things.

Funny enough, small teams do better by doing less — which sounds contradictory but is really to your advantage. This is because individual team members in a small team are not only more efficient, but they also have a clear way of defining what they need to work on.

Sure, 17,000 people in a large organization can achieve a lot more than a team of 15, and they also will not suffer as much whether their productivity is reduced by a few people or cut in half. When you think about it, in large organizations, you can do pretty much everything wrong and you can still be carried along by the general organizational momentum.

But, there is actually a significant, yet counterintuitive benefit to doing less. If you are doing less, you are going to be far more selective in what you say “yes” to. Potential projects and ideas have to go through a rigorous analysis before you start working on them because resources are scarce.

This is a massive advantage because the whole way to focus is by saying no to distractions — the endless shiny objects in the distance that take you away from your predetermined path. Having a clearly defined way of evaluating what is and is not a priority is excellent for keeping things simple.

So the key question that each team member should be asking themselves is:

> Am I working on the thing that is most likely to drive our organization towards our stated goals as quickly as possible?

In answering this question when they evaluate work, small teams have to think about the long-term implications of the work. For instance, how much overhead and maintenance will this project require in the future? 

These concerns are important because it is easy to build up inertia over time as there are so many moving pieces that you have to keep ensuring are working — regardless of what industry you’re in. It will help eliminate unnecessary complexities before they have the chance to take over.

Small teams also do less because they are more likely to employ strategies such as outsourcing (especially some types of on- demand work versus long-term contracts) and automation. This is another way of maintaining simplicity because it removes the need for expanding — and, therefore, complicating — the team and its communication lines.

For instance, instead of hiring a full-time graphic designer, a small team may use an online service for task-based design work. A bigger organization would likely hire a full-time designer or put a large contract out to bidding for various agencies and then have to spend time interviewing various agencies, doing the selection process, and so on.

With regards to automation, this is linked to my earlier point that each person in a small team has to take full responsibility for the work that they are doing, and so they will likely want to avoid as much rote work as possible. In larger teams, you may have an abundance of junior team members or interns that you can throw at any problem, so automation is less important than it should be.

With my experience starting and running Blue, having a small team also ensures that product features and improvements are shipped in an incremental manner instead of thinking in terms of large feature releases. This is because there is typically just one person working on a feature, and we don’t want to wait six months for everything to be perfect before we get it into customers’ hands.

So, the best idea is to review the full scope, cut it down significantly, call it a V1, work on that and ship it. Then, we get into the great cycle of having real customers using the new feature, getting feedback, and then iterating based on that.

In fact, we don’t have a single dedicated software tester at Blue. The person who builds the feature is responsible for the testing, and then the entire team tests the feature as well. Then, we ship it to a public Beta version of Blue that is used by hundreds of customers, and they also test it and provide feedback. We’ve managed to outsource our final end-to-end testing to our customers — free of charge.

The idea of having a public Beta that anyone can access just by changing the URL of our web application was a good one because that saved us growing our team by 10-20% to include testers.

All that being said, make no mistake about it — running a small team is not easy and not always simple. If you’re a team of five, you can easily lose 40% of your capacity by having one person on holiday and another off sick. So productivity does matter, and each individual is far more important to the overall direction of the organization.

My key advice here is to embrace simplicity and run as fast as you can from complexity.

Refuse to have overly complicated processes that require extra work just because *“that is the way it is done around here."*

Complexity breeds complexity: before you know it, you have 50 people doing the same amount of work that could be done by 8.
Instead, as a leader, make sure to keep teams small — much smaller than you think possible. 

You should constantly be looking around and feeling surprised at how much your team is achieving. This should be a shock to outsiders as well. You should hear things along the lines of “Oh, you only have x people on your team, that’s amazing!”.

The world is moving too fast for overly complex organizations with hierarchies and politics. The future belongs to those that are nimble, those that embrace change, and those that have the courage to bet on themselves.
So, go out there and take the challenging but rewarding path to simplicity — with a small team!
