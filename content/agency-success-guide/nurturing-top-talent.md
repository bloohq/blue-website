---
title: Nurturing Top Talent
slug: nurturing-top-talent
tags: ["agency-success-guide"]
description: Attracting, developing, and keeping top talent means better results and fewer headaches. 
image: /resources/top-talent.jpg
date: 08/06/2024
showdate: false
sitemap:
  loc: /resources/nurturing-top-talent
  lastmod: 2024/06/08
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
The importance of carefully sourcing and hiring talent for your business is immeasurable.

One key consideration you should reflect on is the type of person you want to hire. For example, at Mäd, I’ve adopted the concept of the ‘M’ shaped person. 

These individuals are typically skilled in more than one area of work and can apply their knowledge to a variety of tasks — versus an ‘I’ shaped person who is an expert in only one specific function or skill. This is of particular importance if you are running a professional service firm and want to keep your team ‘lean’.

## Identifying Top Talent

So the first thing you should determine is what kind of qualities are you looking for in a team member? What sort of personality traits should they have, and which hard and soft skills align best with your company?

You should have a clear idea of the ideal type of person that you want on your team. Much like qualifying potential clients, you want to do the same with candidates and team members.

I’ve hired close to one hundred people, and can highlight common themes that I’ve seen across the top team members that have been successful.

The first thing is that you want people that are both energetic and enthusiastic. Generally speaking, there is often a lot to do in a consultancy, and you don’t want people who are sluggish or do not have enthusiasm for the work at hand. They should be excited about the things that your company does, and a good barometer of this is to ask yourself whether new ideas are constantly bubbling up to you. 

If they aren’t, then perhaps your team is not as enthusiastic about work as you are, and this is something that you need to fix.

However, these two traits of being energetic and enthusiastic can actually be damaging if they are not tempered by both organization and conscientiousness. Being able to delegate effectively is such a vital part of leading an organization that you need people who can handle the work themselves without constant supervision, including managing their own timelines and striving to reach the standards that are set by the organizational culture.

With all that said, you do also want to have team members that are somewhat different from each other. The reason [cross-functional teams](/resources/great-teamwork) work so well at finding creative and innovative solutions is that people who have chosen different disciplines tend to think very differently, and when you bring these different types of thinking together, that’s when the magic happens.

Differences between people are good as long as they are channeled into constructive debate with a solid framework for both decision- making and disagreements. Knowing when to disagree and yet to commit to a decision regardless is an important skill.

## The Hiring Process

The other important thing to remember is that you shouldn’t look for people who are perfect. You should hire for strengths, not weaknesses. 

For instance, it is likely that the very creative designer you hire is not the most organized person, while a skilled and talented software engineer may not have the best communication skills. This doesn’t mean that they should not be considered to be team members, just that you need to work around their weaknesses and highlight their strengths.

I have made a significant amount of hiring mistakes, and I have also learned that my gut feeling and instinctive judgment about people cannot always be trusted.

The key idea behind a good hiring process is:

- Hire the best talent while reducing the risk of false positives and false negatives.
- Hire as quickly as possible while keeping the above point in mind.

Firstly, you want to avoid as much as possible what are called false positives. This is when you hire someone, but they turn out to not be very good. What normally happens with false positives is that their CVs look great and that they interview really well, but their performance is not of the same quality. Then the situation becomes untenable, and you have to let that person go. The expense, both in terms of time and money, is significant and should not be underestimated. You can often also take a reputational hit if client projects are also affected.

Secondly, you want to avoid false negatives, where you miss out on hiring good people that would have really helped your organization. This is a significantly easier problem to avoid because if you have a clear idea of the type of people that you want in your organization, they should be relatively easy to recognize.

So, the strategy to handle false positives and false negatives is to have several rounds of interviews with different team members, and create a test brief that will mirror some part of the work that you expect the candidate to do as part of their job responsibilities.

While I strive to know all job roles within Mäd (and [Blue](/)!), there are undoubtedly gaps in my knowledge for which I can refer to experts within the relevant departments, such as lead UX designers, graphic designers, or even back-end coders.

Your main qualifying tool will be to set a test brief. If a prospect is impressive during their interview, you should put their skills to the test. We always then ask the prospect to return to our office and present their work to a varied selection of team members.

Including team members in the interview process allows you to build up varied opinions and get a broader view of the applicant’s suitability. During the presenting stage, we invite our team members to assess and score the quality of the work in a shared Google Sheet. Evaluating each candidate as a team provides a significant amount of clarity as well as objectivity, as some members may not have met the candidate before.

With that in mind, consider believability-weighted decision-making on your team’s scoring of the candidate, especially if some of the team members are not in the same functional area as the candidate. This may seem obvious, but it’s surprising how many organizations tend to equalize everyone’s opinions, even when there are clear experts in the room.

For instance, if you have a candidate for your marketing department presenting, your Head of Marketing should have more say than an engineer who was also invited to the test brief presentation as an additional pair of eyes. If the candidate was for the engineering department, then perhaps the Head of Marketing’s opinion would not be weighed as much.

Aside from being capable, it’s vital to vet prospects for their ability to fit the culture of your professional service firm. Sometimes incredibly talented individuals can be a hindrance if they don’t believe in your structure or direction. The easiest example of this tends to come from work structure: at Mäd, we have a loose structure that focuses on output rather than the way the work is achieved. We champion remote work and flexibility, but some may thrive in a 9-5 office scenario and have difficulty readjusting to a structure with extreme accountability and self-management.

The other key thing is to dig deep into the details of their work experience. Have frank discussions about challenges, previous issues at other workplaces, and how they resolved them. CVs are only conversation starters; the candidate should be excited to discuss their suitability, and you should be inquisitive with a want to understand if this person is right for your organization. It is often a red flag if a candidate does not go into lots of detail about previous projects and work experience.

As a final point, references aren’t always as important as some employers will argue. The candidate gets to choose their referees, so you should expect that they’ll be sensible enough to select peers or line managers that will give them glowing reviews regardless of their actual performance. In some countries, it’s even illegal to give a bad review.

But, I’m not saying references are useless. A great tip is to check the job titles of their chosen references — and to double-check that they’re giving you references from their most relevant (and recent) positions.

I once had a candidate apply for a fairly junior role, but they had C-Level references prepared. In contrast, a senior-level candidate might provide odd reference choices by including a parallel member of management as their referee rather than a direct supervisor. This would be a red flag and something to press on.

I’d like to end this section by saying that hiring new team members should be an exciting process.

Adding talent to your professional service firm is a sign of progress and growth. As Mäd is human-centric, we’re always enthusiastic about bringing in great people to help contribute to our vision and mission. 

So yes, proceed with caution, as making mistakes can be a time-consuming and fund-draining process, but keep your focus on the bigger picture — what new talent can do for your professional service firm. And remember, even the best hiring process will not give you perfect results. Sometimes you’ll hire people, and it just won’t work out — and that’s what the probationary period is for, as a last line of defense.

## Team Culture

I couldn’t begin to guess how many organizations would claim that their team is a *‘family’*.

I’m sure you’ve seen that cliche before, and perhaps it’s something you aspire for with your own organization. However, at Mäd, we firmly state that our team is not akin to family; it’s more like a competitive sports team.

Now, you might be wondering why we would ever compare our team to a sports team. The reason is simple — because it works. Just like any good sports team, everyone on our team has a specific role to play. And, just like any good sports team, we are constantly striving to improve and win.

We’ve found that treating our team more like a sports team than a family has led to substantially better results.
You’re in a business environment with goals and responsibility. If a soccer player were terrible in their role, they’d be put on the bench or let go from the club. 

The team wouldn’t simply say: *“They’re not performing well, but they’re part of this family, and we stick by family.”*

That’d be crazy. It’d turn a competitive club into an amateur or hobbyist group.

Likewise, I advocate for ditching the family cliche and reimagining your team like sporting professionals. You’re on a team with collective goals, and you collaborate well — but everyone has to perform and earn their place. You may find that some strong team members have dips in performance occasionally, but that can be normal, and it’s up to you to figure out if they need help to recover their form.

So with that being said, how do you approach issues with your talent?

## Managing Performance

Well, firstly, if you think it’s an issue — then it’s an issue.

This might seem contradictory to how I pointed out I don’t always trust my gut during the hiring process, but when it comes to issues, you should definitely have faith in yourself as a leader to spot worrying signs. If something feels off, or if you’re considering raising an issue, then it’s absolutely worth broaching the topic.

But — *and I can’t stress this enough* — don’t use a sledgehammer.

The concept of PIP and DIP states you should Praise In Public but Discipline In Private, and this is a good starting point for dealing with talent issues. Arrange a time to meet with whoever you believe is causing the issue(s) and give them a fair and honest opportunity to talk through whatever might be causing the situation.

Your goal is to fix the issue for the better, so keep an open mind rather than arriving at the meeting with a fixed outcome already chiseled in stone.

Depending on the severity of the issue, it may be helpful to have at least one other senior team member present at the meeting, purely as a witness. You’ll also want to summarize the outcome of your discussion within an email so that you have a written copy of any key points or decisions for all parties to refer to.

As a transparent and honest leader, you should have laid out clearly your expectations for the company, team, and each team member. If the team member is likely unaware that there is an issue, you should take the time to openly communicate where the lines are and the direction they should be following.

Remember, if you haven’t been communicating clearly enough, then it may be yourself to blame for any issues that arise—which is also how I view project work. If you’re not in sync with what really needs to be done, client expectations, and what research suggests... then you’ll head off in the wrong direction.

Many issues can be resolved simply by highlighting them under a magnifying glass. Let the team member(s) know there’s an issue and discuss how it can be solved in a positive manner. Remind them of the assets and support available to them, and encourage them to speak freely about any factors that might be blocking them from success.

Solving blocking problems is one of the key areas of leadership. You jump in where you’re needed, unblock the problem, hand it back over, and then focus on the next blocker.

I like the concept of ‘healthy pressure’, which usually manifests in tighter deadlines or adding extra (but fair) tasks to team members. Often, just bringing up an issue causes this feeling of pressure for the individual to raise their game and not let standards slip again.

Some problems might feel more severe and may require immediate strict action.

Again, you need to communicate what is and isn’t acceptable. This should ideally be done during the onboarding process, perhaps by clear language in employee contracts that set firm rules.

For professional service firms, a common problem can be conflicts of interest. This is something I include in contracts to set my expectations from the get-go. At Mäd, we have strong ambitions and hire talent to drive forward our goals with their full attention; as such, we demand that our employment is their sole employment, and therefore freelance or consulting work is strictly prohibited.

There are multiple reasons for addressing conflicts of interest. The most obvious one is that you want team members to be just that — part of the team. Creative projects can be time-consuming, and the last thing you need is some of your key talent being distracted by external work.

Here’s a hint: if a team member is still freelancing and making money off the same skill set that you’ve hired them for — you’ll eventually face an issue over it. This could be a deal-breaker for your talent, so it can raise a tough decision for you to make. There are always consultant opportunities, but your team members should know the difference between being full-time or being a consultant and freelancer available for hire.

If you reach a point where you need to dismiss a team member, ensure you handle the situation ethically. Be aware of the correct process for doing this in whichever country you are based. Typically, you should give the employee a fair warning of any issues, with clear steps of what needs to change.

At Mäd, the journey has felt like a roller-coaster at times, with our team expanding at speed... but also falling at times.
Getting rid of team members is never easy, yet sometimes it’s a necessary part of the business.

My final advice here is to remember team morale. Losing team members can promote job insecurity or sadden those who see their workplace team as a friends. Addressing dismissals honestly and respectively can let your team know they’re important to the organization while reiterating that the organization is not a social club, but a professional team.

