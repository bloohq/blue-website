---
title: What Comes Next?
slug: what-comes-next
tags: ["agency-success-guide"]
description: Now that you have the information, it is time to take action! 
image: /resources/angelique.jpg
date: 15/06/2024
showdate: false
sitemap:
  loc: /resources/what-comes-next
  lastmod: 2024/06/15
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
**Give yourself time — start to delegate. Otherwise you don’t have a business, you are a freelancer with assistants.**

One of the most important points in this book was about delegation.

At the beginning of your journey, you (like me) had probably started off as a freelancer working on a project basis with clients — very often as a one-man team. 

But, as your business grows, this will no longer be sustainable. At some point, you will hire people whose job will be to help you tackle the work that you do.

This also means that you are no longer a “solopreneur” or a freelancer: *you are now a leader.*

So, give yourself ample time to build a solid team, train your new team members, and figure out precisely what responsibilities everyone should have.

But most importantly, learn how to delegate your own work — otherwise you don’t have a business, you are a freelancer with assistants!

Delegation *is* entrepreneurship.

**Work on the business instead of in the business. How can you improve the business model?**

It’s fruitless to try to get any work done if you don’t have a longer- term vision for your business.
When you begin running your company, and you have great people in your team who can handle most of its day-to-day operations, you don’t need to work in the business as much as on it.

Obviously, the work at hand is important. But as a leader, you need to consider how you can improve your organization as a whole. 

This means thinking thoroughly about your company’s business model; the way essential things like finances, admin, and HR are structured; how you can keep all of this running smoothly while you focus on other important things.

Remember: your priority is the *point of constraint.*

**Make your own way — come back and use this book as a reference, but try your own experiments and see what works for you.**

Finally, I’d like to refer back to the very beginning of the book and remind you that [these are not rules.](/resources/no-rules-here)

Everything in this book is based on my own experience and I encourage you to venture beyond that. Don’t view this book as an instruction manual — rather, see it as a general guide as you embark on your own business journey.

Don’t be afraid to try!

All of us have plenty of ideas, but often no resources or motivation to act on them. Having an underlying process of experimentation to support our efforts can provide the “safety net” we need to make things happen.

Remember how it works in science: you have to test a hypothesis first before knowing whether it’ll work or not. When you get results, you’ll be able to make conclusions and update your approach based on that. 

And then try again.

This brings me to a final but much-needed discussion on failure.

Looking back, I can say that to fail well and to fail often are two of the most valuable things I’ve learned in building my companies. Without the ability to fail well — that is, treating failure as an opportunity to learn and grow, — I wouldn’t be here writing this book. I would have been too scared and too intimidated to carry on, and who knows where I may have ended up?

The only time you should be afraid to fail is when you fail to learn.

But what does it mean to *“fail well”?*

This paradoxical phrase points to a willingness (and tendency) to investigate the lessons that often exist when we fail. In Black Box Thinking, Matthew Syed emphasized that the biggest obstacle to progress is failing to learn from mistakes — here’s where you can be afraid of failure!

On the other hand, a positive attitude to failure is fundamental for success. So instead of feeling threatened by failure, learn to redefine it as an opportunity for creativity. Failure can act as a generative force, so failing could also often lead to innovation.

On a final note, never stop learning. 

Any experience is a learning experience. You can learn from just about anything — in the long run, even bad clients will have taught you something valuable.

**Learning = Experience + Reflection**