---
title: The Ideal Scenario
slug: ideal-scenario
tags: ["agency-success-guide"]
description: What does an ideal agency look like? This is the question we answer in this chapter.
image: /resources/madoffice4.jpg
date: 04/06/2024
showdate: false
sitemap:
  loc: /resources/ideal-scenario
  lastmod: 2024/06/04
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
Let’s start at the end.

I have reflected on this extensively, and it has come down to one core concept:

**Independence.**

After all, you started your own business because you did not want to work for someone else. You wanted to set your own rules. So, I am sure that you value your independence as much as I do.

Independence in work means having the ability to say no. Never having to take work you do not want. To work on your terms as much as possible.

There can be many reasons for rejecting work:

- You will not enjoy working with the client.
- The specifics of the project or industry are not attractive
to you.
- There is simply not enough budget available to make the work
feasible.

Independence means that running a professional service firm is more joyful.

You are working with the right caliber of clients. You surround yourself with colleagues you enjoy working with. Your work has meaning, and you leave the office with more energy than when you arrive. 

This creates better results for your clients, more enjoyable work, and reduces turnover in your team. It also has significant spill-over effects on your personal life as well.

We can break down independence into its fundamental components:

1. Being Already Busy with Work. 
2. Not Having Key Client Risk. 
3. High Recurring Revenue.
4. Plenty of Cash In the Bank.
5. Lots of Enquiries.

## Being Already Busy with Work

You likely won’t have financial pressure to find new clients if you are already busy. Ensure that you and your team are at capacity for the next thirty to sixty days. Doing so gives you plenty of time to review new potential clients and decide who you want to work with. You should be tracking this in your [Sales CRM](/solutions/sales-crm).

Ensuring you are always busy is a great way to ensure a steady income — these are two sides of the same coin.

You must communicate with your clients that you want to create long-term relationships. This way, they know you are not looking to make one-off project fees. You show that you care about the long- term outcomes — aligning your incentives with theirs.

When you are busy with existing clients, it gives you the freedom to be selective with your future clients and focus. You can pick and choose the ones that you want to work with instead of having to take on whoever comes your way. You do not have to worry about the distractions and bullshit of dealing with difficult and highly demanding clients.

## Not Having Key Client Risk

This is counter-intuitive, as one would imagine that the bigger the clients, the better. But, this is not true.
Let’s set up a hypothetical scenario where we have two businesses in which everything else is equal, except the distribution of revenue between their client base.

Company A has two clients, each accounting for 50% of the total revenue.

Company B has fifteen clients, five of which have 50% of the total revenue, and the ten share the remaining 50% of revenue.

Which company is more resilient and has fewer risks in its business model?

Company A cannot afford to lose a client because that would mean a revenue reduction of 50% and likely significant cutbacks to team members and general business expenditures. Company B can afford to lose multiple clients, which would not have significant impact.

And these are real scenarios. Mäd has previously been Company A, where we had one customer representing 70% of our billings, and now we are Company B, where we have a much more equal customer base. No single customer can take our business down.

And this is really what defines key client risk:

> It is when no individual client is too big or important. If they canceled their contract, would they bring your business down? If the answer is yes, you’ve got key client risk.

There are also other advantages to not having key client risk. It makes you more comfortable having strong opinions on your work with clients. This is because you’re not worrying about the relationship with the client. It’s fine to have heated debates with clients about the work, as long as the relationship is on a solid foundation.
This is a subtle but important point.

You need a solid foundation with clients before having heated debates about the work because it allows for open communication and a better working relationship. If there is a strong foundation in place, then both parties are more likely to be respectful of one another and have productive discussions about the work. This can lead to better results for everyone involved.

A final point on key client risk. 

If you have key client risk, your client will often be *acutely* aware of this. They know that they are your biggest customer, and they can use this against you to extract better pricing and terms during contractual negotiations or just generally have unreasonable expectations in the project, putting pressure and stress on your team.

## High Recurring Revenue

Another component of independence is to have a high degree of recurring revenue. This means long-term retainer-based agreements, where clients pay the same monthly amount. This smoothens cash flow, encourages long-term thinking, and naturally develops long-term client relationships. It also makes planning much easier compared to only having individual project revenue.

Revenue from long-term contracts can help to offset the fluctuations that are common in businesses that only have project-based revenue. This type of stability is especially important for small businesses and startups.

The ideal scenario is that you have plenty of recurring revenue, so you never really need to consider cash flow as more than a passing problem, not something you battle with every month. This means not having to chase every invoice and worry if a slippage in a project deliverable will result in late payment.

I’ll discuss some tricks in [Chapter 7](/resources/cash-is-king) about structuring the payment terms in your contracts, even for fixed-price projects, so that all your income “feels” like recurring monthly revenue. This is perhaps one of the most significant yet simple changes you can make to your professional service firm.

## Plenty of Cash in the Bank

Having a minimum of three full months of operating cash flow available in the bank is *essential*. This shields you from short-term variations in cash flow based on payment delays.

The ideal scenario is to build a full six months of operating cash flow in the bank and keep it there. This creates a huge safety net and gives you a psychological edge — which you should take advantage of and communicate to your team and your clients.

When you have extra cash flow on top of this reserve, you can reinvest it into the business, take profits, pay down debt, or negotiate upfront payment with vendors you trust to receive a discount. This is a great position to be in and gives you flexibility.

You need to take immediate action if you don’t have enough cash flow. This may include reducing expenses, renegotiating payment terms with vendors, or decreasing prices to attract more customers quickly. You can also have lines of credit already set up with your bank, so you can easily access them in times of need.

The other key point about having plenty of cash in the bank is that you do not need to take projects you would rather not take just because you need the money. In the early years at Mäd, I had to constantly accept projects that I knew were likely to fail, with clients I didn’t want to work with, purely because I had this constant pressure to make ends meet every month.

This is about ensuring you can sleep well at night and focus on running your business — not chasing invoices.

## Lots of Enquiries

You should be getting lots of inquiries per month about new work. You can then spend time evaluating these inquiries and pick the best potential clients to work with. If you only have a low number of inquiries, this then means that you cannot be as selective with who you work with. This is because you cannot be sure that new potential clients will be knocking on your door any time soon.

You need to aim for at least one proposal per week or fifty-two requests for proposals per year! However, getting this number of requests for proposals per year does not come by accident.

*You need to focus on it.*

If you ask business leaders what they want to spend more time on: sales and marketing would often top the list. After all, this is the engine that drives the business forward.

Without consistent sales, a business cannot function. So, you have a choice. You can be in a position where money is essential versus where money is everything. I would rather that money be “just” essential. In other words, a position where money is important, but not so urgent that it overpowers everything else.

It should feel like you have the wind in your favor regarding sales. This means that new business comes naturally and regularly. **Sales cannot be an activity done in-between projects when you have spare time.** 

This approach would spell disaster.

Think about it, when you’ve got time (i.e., when you don’t have as much work!) is precisely when you need to have new sales coming in. Consider that often, many sales take two to three months to close. You may end up with a lengthy quiet period while chasing leads. And trust me, potential customers know when you are hunting the sale.

So instead of selling from a position of power, you’re now selling from a place of weakness. You should be selling when you’re very busy. This demands that you find time to slot your new clients in, instead of chasing clients to fill up all your free time.

You’re in trouble if you’ve got time pressure because you must ensure that you meet your numbers and stay afloat. This means you won’t negotiate and deal as hard as you might otherwise. One terrible thing that can happen here is that you rush the scoping of a particular project. You quote a fixed price to close the deal and get some cash in. A few months later, you find yourself working for free to complete a project.

Remember, there is never a moment in your life when you’re making more money than when you’re negotiating. If your client is under time pressure and you’re busy, this can play to your advantage — ever heard of rush fees?

Some businesses charge a “rush fee” for expedited service. This additional fee is charged on top of the regular price for the service. The purpose of the rush fee is to compensate the business for the added cost of a rushed production, which may include overtime pay for team members and just the general headache of working in a rush. You can, and should, charge rush fees if a client has time pressure because working under tight project deadlines does cause extra stress, and it is not sustainable if every client has rush requests all the time.

So, there you have it: the key components to achieving independence.

If you are able to position your firm as being in high demand, you can significantly grow your customer base, while continuing to be busy all the while.

Then, once you get into a flow that gives you freedom and predictabiblity (especially in regards to cash flow), you can focus on running and eventually expanding your business. Remember that stability gives you the power to choose projects and be confident in your team and its abilities.

But to be able to act on these steps, you will need to rely on other factors: to ensure that you have a steady stream of clients and therefore profits, you need to actively work on sales and marketing.

## Think About Your Customer Journey

So, what does it mean *“to make Sales and Marketing a priority”*? For starters, you need a plan and a team to execute that strategy.

Each industry is different, so it’s difficult to give a precise plan that will work for everyone. That said, I can provide you with a framework that you can use to create your plan. Spend time and think of the buying journey of your customers. Map it out, put yourself in their shoes, and imagine the type of questions they might have. Think of the problems they might see ahead, what they worry about, and what they care about.

Don’t just do this yourself, but ask your existing or potential customers to find out! Over time, you’ll have a more accurate picture that maps well onto reality. You will have in-depth knowledge of your customer’s journey to buy your services.

Then, think about how you can make this journey easier. How can you be relevant to your potential customers even before the project?

At a high level, most customer journeys will go somewhere along these lines:

1. **Discovery:** How are your clients looking for solutions to their problems? How can they then find you?
2. **Education:** How can clients learn more about you and your product and services?
3. **Decision:** What decision-making framework will clients use to choose a firm? Why should they choose you over many other firms? This introspection will lead to a list of activities to help with this customer journey.

One key thing to remember is that you should not think in a short- term manner here but look a few years further down the line. 

For instance, having a large number of industry resources could be excellent for prospects. They could spend their time getting lost in your archives and discovering new ideas and, naturally, want to work with you. If that sounds interesting, you’ve then stumbled on a multi-year project.

The idea is to get the right balance of short-term execution and long-term planning. You need to be able to leap forward two to three years from now. Understand what the key things are that will make a difference.

Then, start working on these differences *right now.*

Break significant actions into smaller, simpler steps that you can complete *and* ship out. This makes long-term projects manageable, because you’ll see immediate benefits while working towards a long-term goal. It will also keep motivation up, as you can observe and track weekly progress.

Projects that take six months to see if they are impactful are wasteful in a small organization — the risk is too great.