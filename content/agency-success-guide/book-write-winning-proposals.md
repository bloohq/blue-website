---
title: Write Winning Proposals
slug: write-winning-proposals
tags: ["agency-success-guide"]
description: Writing winning proposals is the most important part of running an agency. Learn how to do it right. 
image: /resources/madlaptopwork.jpg
date: 06/06/2024
showdate: false
sitemap:
  loc: /resources/write-winning-proposals
  lastmod: 2024/06/06
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
**Writing proposals is the most critical activity in any professional service firm.** 

After all, without any new work, the firm will stop existing. And without any of the right type of new work, the firm will stagnate.

So, ensure that you take writing proposals seriously. You’ll never make more money per hour than writing proposals or negotiating with clients — remember that!

The purpose of writing a proposal is two-fold:

1. **Provide clarity to the client about the proposed project.** You might find this strange if the client contacted you. After all, why should you explain the project back to them? Well, remember that we want to position ourselves as experts. You’ve done more projects than they have, so you should be the one providing clarity.
2. **Educate the client on why your organization has the best people and experience for them.**  Why should they pick you instead of anyone else? Why is your firm specifically suited to their industry, their problem, and their organization?

This means that you need to be razor-sharp in your thinking. You need to clearly showcase what makes you different from everyone else. You need to explain your methods and why they matter. You need to show the actual value of your work — and what it can do for them.

It is best to start collaborating with the client right away, even before the project has started and you are in the proposal phase. Work in a collaborative manner with the client to narrow down the precise scope of work of the project, providing advice along the way. This gives them a taste of what it is like working with you (and vice versa).
This is important because if you see that they are already challenging to work with at this stage, you know what to expect during the project. You can then use this information to price your proposal more accurately, or even decide not to take the project at all.

And...you need an [easy-to-use and performance CRM(/solutions/sales-crm) (Customer Relationship Management) tool to keep track of all the proposals that you are working on! 

Once you’ve agreed on their needs, you can move on to the next step. This is where you add pricing to your document and formalize the proposal with all the terms and details. Some clients will often want to skip this process and jump right to the proposal. Refuse to do this; the win rate for this type of work is low — it’s not worth your time.

The reason for working on the scope with the client before the proposal is that you don’t want to attempt to hit a moving target. If the scope constantly changes, you’ll need to adjust your pricing for each proposal version. This starts to focus the discussion on the cost per line item instead of overall value. The client will ask for precise justifications on why the price changed between each version. You’ll be fighting a losing battle.

Also, it’s a lot more work on your side!

If you only give the pricing once the scope of work has been confirmed, you avoid this problem. You also encourage the client to review the scope of work in detail.

So, let’s dissect what makes a good proposal. A good format that I have found to work is to use the following sections:

- Introduction
- Scope of Work
- Out of Scope
- Timelines
- Investment
- Suggested Contractual Terms
- Team CVs
- Case Studies & References
- Professional Service Firm Profile

I’ll go into the details of each section below, but you should also know when to break this format. 

If you are responding to RFPs from large organizations, they often specify the exact structure for your proposal. 

I’ve written RFPs for large banks and international organizations, and I absolutely hate it when vendors send their proposals back and fail to follow the suggested format because it creates a significantly larger amount of work to compare the different proposals. It also showcases a lack of care and attention to detail from the vendor — something which I’ll often score them down for.

So if that’s the case, and there is a required structure, make sure you follow the instructions. If you don’t, you might be disqualified.

Whether you do slides or long-form, this is up to you. Personally I prefer long-form. For larger projects with significant details, a long-form document is almost always preferable.

Learning from each proposal and adding this information and sections into a master document is also helpful. This means that for every project that you write a proposal for, if there are any improvements in the proposal, you feed those back to the master version so you can reuse them again in the future.

This master document is then the starting point for each new proposal. This can save you a significant amount of time and effort than writing each proposal from scratch. But, you need to be careful that you don’t fall into the trap of sending out templated proposals.

I can promise that many clients can see through a template proposal, and you’ll lose work.

The reality is that you will not win every single project proposal you send. But you can build a great business even by winning only a minority of the proposals you send! At Mäd, we reject a third of the RFPs that we receive, we lose another third, and finally, we win a third. So with just a 30% win rate for every prospect that requests a proposal, we can build a healthy and long-lasting business.

But, because we don’t know which ones are going to be the winning ones, we need to put our best effort into each proposal.

This is especially true if you think of the lifetime value of a potential customer: not necessarily the investment for this particular project, but also the future projects further down the line. This could result in more work, great case studies, references, and testimonials. Also, don’t forget that people will move jobs in the future. 

If you keep the relationship going, you’ll also have the possibility of having a new client once they join a new organization. Over time, this can become a significant source of new business, although it does take several years to build up.

So, you can see how vital proposal writing is, not only for the immediate project win but for the long-term health of your firm.

Let's now dissect each section of a proposal.

## Introduction

This is often called the *“Executive Summary.”* This is the section that the key decision-makers will read, apart from the pricing.

So, this is not the place to be long-winded or shy. State your case, and explain why you’re the best organization for the project. Think carefully about your key messages:

> What do you want the person reading to take away from this introduction?

This section will also be the first impression that stakeholders will have of you. Make it a good impression! Show that you understand their pain points and what they are trying to achieve.

If you reach a scale where you do not have to write every proposal yourself, try to write the introduction at least. 

This will give it a personal touch that’s hard to replicate.

With this in mind, ensure that you have a signature block at the end of the introduction with your personal phone number and email address — this showcases to the client that the leadership of the firm is easy to reach and accessible, which provides them with peace of mind.

One extremely strong buying signal is if you have the CEO or any member of top management on direct message via WhatsApp/ iMessage/Telegram. 

I don’t believe I have ever lost a proposal where this has happened: these people are super-busy, and if they are spending their time on direct messages with you, it means that you are almost certainly an important part of their upcoming work.

## Project Objectives

Let’s remember the first of the two objectives of any proposal: *providing clarity.*

Stating the project objectives and the critical success factors goes a long way to clarifying why the client should invest in this project. This means that you showcase how the project can have a significant impact on the client’s business.

This can be a very concise part of the proposal, but it can really put the client at ease — they will understand that you are a good listener and that you have understood what they care about.

A great trick here is to ask about key success factors during the initial meeting with the client. Take notes of the precise wording that they use. If possible, use these very same words, phrase by phrase, in this section — this will strongly resonate with the client, and you’ll be surprised by how much weight and importance this relatively small section of the proposal can have!

When possible, have quantitative goals.

## Scope of Work

At the *heart of the proposal*, this is where you discuss, in detail, what you’ll be doing for the project. The best idea here is to get into depth about your processes and explain why you will be doing what you are doing.

It’s important to ensure that this section does not feel generic.

For sure, many scopes of work across different proposals may be similar, but you need to take the time to fully understand what is unique about this client and project.

If you are doing anything related to digital products or IT, you might need to split this section into two. The first section discusses the features and functions of the product that you will build, and the second section describes the actual stages of the work required, such as design, scoping, development, testing, and so on.

A common problem within the scope of work section is that if there is a lack of specificity, it opens the possibility for clients to disagree on deliverables. You should therefore aim to make your scope as clear as possible to avoid any ambiguity or misinterpretations. For example, if you include writing reports in your scope of work, you should indicate what these reports will consist of and how often they will occur.

A good way to test that your scope of work section is clear enough is to run a thought experiment. Can this section be used by the project manager to easily go through and tick off each and every item at the end of the project to ensure that the project has been completed? 

If yes, then you have the right amount of clarity and granular detail in your scope of work section. 

If not, then you need to go one level deeper in terms of detail. This can sometimes feel like a lot of work, but it is worth it to have peace of mind that you will not have trouble signing off on the project at the end.

## Out of Scope

This is a really important section that is often overlooked by many professional service firms, but it is actually a good thing to clarify exactly what you will not be doing as part of the project.

This is because it forces the client to truly think carefully about the scope of work. They will see a list of exclusions and be aware that they will need to change the scope (and thus pay more) if they need additional services or deliverables.

It’s important that when you list things out in this section, you take the point of view of the client. For instance, if you are doing web development, it might be obvious to you that the website hosting is not included in your project price and is a separate cost that the client needs to consider and pay for. 

However, for a client that is redoing their website for the first time in 15 years, this may not be obvious at all. In fact, they might not have even considered the fact that they need to pay for the hosting!

The same goes for additional features in the creative industries like production, printing, fonts, licenses, and so on.
Personally, I like to bundle in local travel costs and make it clear that we will then refund travel expenses outside of a certain geographic area, normally the city that you’re based in.

You may also want to use catch-all phrases, such as stating that all third-party costs are out of the scope of the project and will be billed separately, and even that work over a certain amount of hours or after a certain date has passed is automatically out of scope.

This last point is quite interesting because it means that the client also has some degree of time pressure to ensure that they do all that is required on their side to complete the project within the allotted time if they want to stay within the initial budget.

## Key Deliverables

To expand on your scope of work, it is useful to map out what the client should expect to receive in the form of deliverables. This section will summarize the expected goals and targets that must be achieved during the project and will include supporting information to help the client clearly understand what is being agreed upon.

This section varies from project objectives as it focuses on tangible or intangible accomplishments to be completed within a specific timeframe.

To be considered a key deliverable, certain criteria should be met:

1. The items should help accomplish your objective(s),
2. They should be agreed upon by the client and your team at the
beginning of the project,
3. They should be achieved through meaningful work, and
4. They should, of course, be within the scope of work outlined from
the beginning of the project.

It is helpful to use your key deliverables as a reference point for the project manager and team to refer to throughout their work to help them visualize a distinct finish line. Without clear deliverables, your team may work in circles and potentially begin deviating from their goals.

Remember, well-defined projects ensure that everyone is on the same page. Clients know exactly what to expect, and your team is aligned with what they’re working towards.

## Timelines

Defining your timelines is another crucial part of the proposal. Not only are you estimating how long the project will last, but you are also setting expectations for the duration of each of its parts.

While timelines don’t necessarily have to be incredibly detailed in your proposal, they should at least give the reader a broad overview of the project. Timelines will change — that is just the nature of the business — but try to be as realistic as possible with your estimations.

If desired start and end dates aren’t known, you can simply organize your timeline by the length of time you estimate tasks will take. 

You don’t need to include every single task in the project with an assigned beginning and duration length, as this will make your timeline difficult to read and give your team little flexibility — instead, you should focus on key milestones.

It is worth noting that some projects may include ‘inactive’ time periods, whereby work cannot be progressed until another event takes place. These periods of inactivity are useful to map out from the beginning to solidify when your team will be active and what will be necessary at given times to continue the project: for example, a seasonal campaign or a period where the client will need to curate their own content.

With regards to feedback time or sign-offs, it is even worth asking the client what their typical process looks like and how much time they have historically needed for sign-offs on other projects — you can then use this information to build a more accurate model for your overall project timeline.

From experience, I’ve learned that even the most educated estimations can hit road bumps. When you’re calculating your timeline, you’re attempting to predict potential risk and reduce surprises, but this is never entirely possible. There will always be unknowns, and you have to factor in a degree of flexibility to solve unforeseen problems within a realistic timeframe.

I’ve found that there is a balancing act between your planning time and your ‘doing’ or working time. 

Simply, the bigger the task, the bigger the planning efforts for that task. Aim to dissect how complex each task might be and consult with your relevant team members for practical advice on how long certain activities may take them. 

Getting your estimations wrong could turn a six-month project into a one-year project, which would result in team members being double-booked in the future or simply unavailable, and worse — risking the reputation of your company and the profitability of the project.

With smaller project tasks, you can afford to make quicker estimates, as any small mistakes will be cheaper and speedier to fix. We’ve found that running a quick team meeting can help you speak through potential issues and map out a realistic project plan that everyone is on board with.

As a side note, no matter how organized you and your team may be, clients can derail timelines massively, so be sure to highlight when you may need their assistance, resources, or approvals. As mentioned before, putting some healthy time pressure on clients can ensure they do their part to keep the project on track.

## Investment

By the way, *never* say “price.”

You should always be framing any money that is spent directly or indirectly on your services as an investment from the client that can earn them a return.

There are also a few key things to mention in this section, including the payment schedule (more on that in Chapter 7) and also making sure to clearly state if your fees are inclusive or exclusive of tax. If you don’t specify this well, sometimes you will encounter some crafty procurement people in large companies that will use this as an excuse to negotiate a significant discount — they will always claim that they assumed your fees included tax, even if you usually quote NET, but didn’t state it clearly.

Where possible, try not to provide granular line-by-line item breakdowns of your pricing. If you break it down by major area, this is fine, but if you’re getting into price-per-deliverable or anything along those lines, then this is a red flag. It means that the client views you just as a vendor to outsource some items to, not a true partner that can help them in the long term.

## Suggested Contractual Terms

Sometimes, large clients will have their own default terms and conditions that you need to agree with. However, you should always include your suggested contractual terms in your proposal, along with an area to sign off on the entire proposal. That way, you make it as easy as possible for the client to accept and move forwards.

While this is only a small example of reducing friction in your sales process, if you have this attitude towards everything, you make it significantly easier for clients to buy from you — and they will appreciate it by buying more!
When I first started out, I paid a large law firm a fair bit of money to write a lengthy 25-page contract that covered almost all possible eventualities and things that could go wrong in the project. 

However, what often happened is that I ended up spending a lot of time with the clients, or their legal teams, having to discuss each and every point and ensure that we agreed on everything. This often caused significant delays in starting projects, and I felt it was truly a waste of everyone’s time.

If the client is a large enterprise, they will likely have their own standard contracts that you will need to review and agree to.

Two things that you want to be aware of:

### Penalty Fees 

Some clients will want to place penalty fees for late projects, but this is something that you should negotiate out of the contract. This is because, often, timelines are out of your control , regardless of the level of effort or goodwill that you put into the contract. 

This can be as simple as things being more complex than initially expected, or that there is a feedback delay, or that one of your key staff becomes ill. You don’t want to have the stress of a late project and having to pay late penalty fees. 

Additionally, you should also think about how you generally want to structure the relationships with your clients. Starting a contract with each party holding a gun to their head is not a good long-term strategy.


### Non-Competition Clauses

Sometimes, clients will put in their contracts that you must only work with them and no other companies in their industry. You must not agree to this because it will significantly reduce your potential future sales, and if they want this level of exclusivity, then they should be paying separate fees that are above the project fees to compensate for the potential lost work in the future.

Nowadays, when clients ask us to provide a contract, we have a simple one-page contract that clients can review in a few minutes. I’d say that 95% of clients simply accept it without any changes. Feel free to review it and change it for your specific needs:

- This agreement is between [professional service firm Legal Name] ("[professional service firm Name]") and [Client Legal Name] ("Client") on __________.
- The representatives are [professional service firm Rep Name] for [professional service firm Name] and [Client Rep Name] for the Client.
- This contract is the only agreement. There are no other agreements, oral or written.
- The Client wishes to purchase the services from [professional service firm Name] described in "Scope of Work" and "Key Deliverables".
- This contract can only be modified or amended by mutual agreement of [professional service firm Name] and the Client, in writing.
- This contract is governed by the laws of [Your Country].
- All official project communication will be via [Insert official project management system name or email]
- The client will provide feedback on any mid-project deliverables within x working days. If feedback takes longer, [professional service firm Name] may requote the timeline and price of the project.
- Invoices are to be paid within x days of being received by the Client.
- [professional service firm Name] will stop working if the invoices under this contract are not paid within x days of being received by the Client, and may terminate the agreement.
- [professional service firm Name] has full permission to use any non-confidential deliverables in their portfolio and marketing materials, including but not limited to a case study on the [professional service firm Name] website once the project is completed.
- The liability of [professional service firm Name] to the Client under this contract will be limited to the payments actually received by [professional service firm Name] from the Client under this contract.
- All intellectual property created by [professional service firm Name] under this contract will be the property of the Client, after all payments have been received by [professional service firm Name].
- [professional service firm Name] will treat all Client information as fully confidential. [professional service firm Name] will only disclose confidential Client information to pre-approved employees and subcontractors.

## Team CVs

Remember what we said about being a people-based business? By adding the details of your team, their roles in the project, and their CVs, you’re positioning yourself not just as service providers but as trusted experts. It shows that you’re confident enough to show off your team.

It’s common advice to tailor your CV to the job you’re applying for, and this advice carries over to project proposals, too. It can be useful to have prewritten team bios/CVs, but you should never simply copy and paste them into new proposals. Take time to show that you understand the clients’ problems and display the team members’ direct suitability to address their needs.

When you’re displaying your team, you’re focused on conveying value — both on their credentials and on the way they work. This latter point is of particular interest: in a crowded professional service firm market, it can be advantageous to leverage your team culture. 

Think of it this way, multiple agencies could possess talented individuals with the same qualifications, but if the way you work and think is aligned with the client, then it’ll lead to a harmonious arrangement and meaningful work.