---
title: Be Wildly Different
slug: wildly-different
tags: ["agency-success-guide"]
description: Differentiation is key in the agency space. Learn how to make small changes that pay off in the long-term.
image: /resources/madcomputercode.jpg
date: 05/06/2024
showdate: false
sitemap:
  loc: /resources/wildly-different
  lastmod: 2024/06/05
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
## The Power of Differentiation
We don’t live or work in a bubble. Other people are trying to do the same thing and want your clients. With that in mind, you need a strategy not only on how you will survive but how you will thrive.

The fundamental idea behind the strategy for B2B professional service firms is differentiation and building strong, clear value propositions. 

The key question to ask yourself is:

> Why should someone pick *you?*

Because you are different from everyone else, in a better way!

The critical thing with marketing is to have a clear set of value propositions and to be able to communicate these effectively to your target audience. Ideally, these value propositions need to strongly differentiate you from everyone else.

Make an enemy of *“business as usual”* in your industry — without naming specific competitors.

And the best way to showcase differentiation? Actually be different!

I know this sounds obvious, but you’ll be amazed at how many companies talk about being different. Yet, if you just glance at the website or step into their office, it’s the same as everyone else.

I’ve seen examples of professional service firm owners talking about User Experience Design, and I visit their website only to see a carelessly put-together template with blurry photos, confusing navigation, and significant misleading information.

**Don’t do that.**

Actually make an effort to discover how you can be different from others. This is not something that will happen overnight: you’ll have to do a significant amount of research and experimentation with your approach and processes, but eventually, you’ll stumble upon some degree of success. 

You must then analyze and understand why you succeeded in that particular instance. Then, work out how to make this success repeatable and predictable.

Stepping back, there are really only two ways to do business.

The first way, is that you can extract economic rent. This means that you’re providing useful services that the market needs and expects, but that’s it. The maximum you can expect to grow is at the pace of inflation or the growth rate of your specific industry.

The second method is to focus on standing out from the crowd.

Personally, I think the second method is key for B2B professional service firms, because it is a highly competitive field.

For instance, you are likely to have more competitors in your local market than [Blue](/) has across the entire world!

**Let that sink in.**

This is because starting a professional service firm is not significantly capital intensive, and you can often fund startup costs and growth purely on the back of large project wins. This means that although it is an attractive startup business for many people, most firms fail within their first couple of years because they do not discover how to operate — the precise problem that this book is trying to solve.

However, the good news is that, unlike in the technology industry, professional services are not a winner-takes-it-all market. No firm can possibly service even a fraction of the entire market, so it is about fighting for the right clients with the right budget. 

In reality, your main competitor is not other firms in your industry, but your own ability to run the business well and showcase differentiation.

## Competing Across Time Frames

You need a clear answer to that right now, and you need an answer that will evolve as well.

A discussion on a strategy and differentiation without a discussion about time is irrelevant. Everyone competes in different time frames. 

In the short term, you might be trying to win a particular bid against another competitor. You need to have a strategy to try and win this in the timeframe of days or weeks.

You can compete in different ways on the higher time frame side of things. What I love about building a long-term strategy is that if one manages to execute it well, it is essentially the equivalent of a checkmate in the game of chess.

For instance, if you have a five-year strategy about your company’s professional brand that you will have thought leadership developed over the years with lots of white papers, educational content, and so on, then this is not easily replicated. Once your competition notices this is working, they cannot wind back the clock and do five years of work. It’s too late — by the time they catch up, you will have already moved on.

That said, executing on a five or ten-year strategy is something that almost no small or medium business does. This is because it is a risky endeavor. If you mess it up, it will be an enormous waste of time and energy. This is why I believe a strategy should not be something that is unveiled at a particular time or place but is an evolving and ongoing process of change.

The other key thing to take away from the above is that many clients have seen many firms come and go over the years. Once you reach the 5+ year mark, things get significantly easier because you have a lot of goodwill from previous projects and clients, but also, new clients are reassured that you’re not another one of those operations that will disappear overnight and leave them in a mess.

If you’re a new professional service firm, you have to fight against this perceived risk that clients see, and the best way to do that is to play one of your most decisive advantages: **your lower cost base.**

Because you will likely have significantly lower overheads than established companies, you can afford to underprice them enough to win work you normally wouldn’t. However, you need to understand that clients often see pricing as a barometer of quality, and there is such a thing as pricing too low.

So how do we move from where we are to where we want to be? How do we build a bridge to that five or ten-year vision of success?

## The 1% Improvement Strategy

Well, instead of one huge plan or action, my advice is to look for small 1% improvements.

Everyone is fascinated by that apparent and sudden leap forward: Steve Jobs announcing the iPhone. The team at SpaceX crying as their multi-stage reusable boosters land back to earth in one piece.

And so, most people assume that this is how progress is made. 

In reality, this couldn’t be further from the truth.

Large leaps forwards are years in the making. You can see Neil Armstrong land on the moon and discount the work of 500,000 people that collaborated at NASA (and their suppliers!) to make that happen in the previous ten years.

Progress looks like fixing a minor software bug, an accidental discovery on how to make something better, and insight gleaned from years of thinking about a specific problem and trying hundreds or thousands of possible solutions.
Progress is made in iterations: taking two steps forward and then one step back, accepting and embracing failure, and seeking to improve every day. This is why I recommend that business leaders ask themselves the following question every morning:

> How can I make things 1% better?

While 1% does not sound like a significant performance improvement, this is where the magic of compound interest comes into play. Einstein called this the *“eighth wonder of the world”* — those who understand it earn it, and those who don’t, pay it.

A 1% improvement each day for a year leads to a 37-fold improvement. It doesn’t matter if you are improving 1% a day or 1% a week; what matters is that you are always looking for opportunities to make small wins that will add up in the long term.
Conversely, if you get 1% worse each week, you’ll get significantly worse over just a year. And what’s scary is that a 1% reduction in performance or quality is hardly noticeable.

Forget overnight success — most entrepreneurs and accomplished individuals will tell you that their “overnight” success was ten years in the making. Eight of those years are spent working in obscurity; then, everyone notices the leap forward once all the hard work has been mostly done.

The key to compounding 1% gains in organizations is to focus on things that are both urgent and have a long-term impact because it is impossible to compound if you focus on tasks that are only of short-term importance.

Something as menial as documenting the way a particular process is made can be counted as a 1% improvement if you can be confident that this documentation, or a version of it, will be used and valuable in five years.

It’s also worth regularly looking back on how the organization was a few years back to notice the real tangible improvements in how things work and the results that have been achieved.

We took this approach with our own office, for instance, taking the time to invest in small but meaningful changes that compounded over time. After two years, it was barely recognizable, and visitors would be excused if they thought we had moved office — forgetting that our business address had not changed.

The key thing to realize here is that complex things are actually on the same scale as easy things, but the actions required to accomplish the difficult things take a lot more time and repetition.

Indeed, complex things are more challenging in the interim — but they also result in significantly better long-term returns. On the other hand, easy things are simple enough to tackle immediately, but then their outcomes are usually short-lived as well.

So then this forces you as a leader to define what is truly important to your organization, and then obsessively focus. For instance, if you are a fast-moving startup, then having late-night working sessions and prioritizing the speed of shipping a feature may be more important than the stability and reliability of your product. Bringing more people on board may be more important than a coherent culture.

This is fine, as long as this is a *conscious choice.*

## Balancing Short-Term and Long-Term Focus

However, you cannot build a reliable product over time without focusing on these small 1% improvements that have long-term payoffs vs. the short-term hacks that are not optimized for the long term.

So if you build a product and believe that your customers value reliability, you need to slow down and bake that into your culture, 1% at a time. This means sometimes missing shipping dates to ensure that things are done properly. When this happens, you should secretly celebrate inside, because it is a solid way to show that you care about your organization’s values.

**A leader that allows a shoddy product to ship to the world just to hit a deadline is only paying lip service to reliability.**

We have noticed that, at times, focusing on 1% improvements feels really inconsequential. Going through our corporate website to ensure that images are well-optimized, reviewing older projects to see if they make good case studies, changing our design software to something that is more collaborative, or watering the plants in our office.

By themselves, none of these actions change anything in the slightest. A slightly more optimized ‘about’ page will not win us any more customers or make our team happier. Yet, if each page is given small improvements regularly, by the end of the year, our website would be remarkably better.

We use this approach because the difference begins to show when you consider tens of thousands of these actions chained together over the years. There is never a particular moment that can be pinned down where you say, *“that is when it happened.”*

In the end, there is a choice to be made about what you believe. You either believe that things can always be done better and make it happen, or you believe in the status quo, stagnate, and watch while the world improves.

**This is why we water the plants in our office.**
