---
title: Start Building Momentum
slug: start-building-momentum
tags: ["agency-success-guide"]
description: Understand the concept of momentum and why it is crucial to build momentum in your agency. 
image: /resources/madoffice3.jpeg
date: 03/06/2024
showdate: false
sitemap:
  loc: /resources/start-building-momentum
  lastmod: 2024/06/03
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, to [great teamwork](/resources/great-teamwork) and client relationship building to writing winning proposals — it's all here.  

---
You might be asking yourself: why is this first part of the book called *Start Building Momentum?*

The reason is that the key aim of all your efforts as a professional service firm leader is to drive momentum to your organization. You may find yourself spending a significant amount of your time on sales and marketing.

This means attention from prospective clients and an increase in requests for proposals (RFPs). This assures you that there is always more work coming soon. You can use these RFPs to test new formats, services, and pricing models. It also increases your confidence, as not every single proposal is critical to your success because you are sure that there will be more RFPs coming in the near future.

You are in the driving seat when it is not critical that you win or lose any particular proposal. You have a strong negotiating position. You don’t need to rush to close a deal. You can take the time to push back on requests, review the scope of work, and so on.

This is the first part of the book because, without momentum, you have nothing.

Momentum is the foundation of your firm, and everything else builds from this. This is a crucial point to remember and something that you should keep track of.

When you have momentum, many mistakes don’t matter or can be forgiven because you always have the next deal. You’ll feel excited waking up in the morning and pushing forward; this energy will be passed on to your team.

But,if you don’t have momentum, it is very difficult to get started or to keep going when things get tough. Every deal counts, and the pressure and stress are high.

This is not how we want to run our businesses — or our lives.
