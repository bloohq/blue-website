---
title: Introduction
slug: introduction
tags: ["agency-success-guide"]
description: Learn why we wrote the definitive guide on how to run an agency effectively, and how we got started.
image: /resources/madoffice1.jpeg
date: 01/06/2024
showdate: false
sitemap:
  loc: /resources/introduction
  lastmod: 2024/06/01
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
## Hello

**This is the book that I wish I had when I first started my business.**

Starting a business has been one of the most challenging things that I have accomplished. In hindsight, this could have been much easier if I had made better choices.

The only way to make better choices is to have the proper framework for decision-making. This should have foundations in both experiences (often known as mistakes!) and [insights](/resources/insights).

If you can learn from other people’s mistakes, that’s often better than making your own — hence this book.

This book analyzes the last seven years of experiences that I have had running [Mäd](https://mad.co), a strategic consultancy. I’ll be teasing out fundamental principles that helped me overcome my initial lack of knowledge.

I’ll share some of the ideas and processes I use to ensure I make the right decisions more often than not. The key idea is to run a calm business that has solid fundamentals.

Perfection, after all, is not required to be successful!

If you’re a [solopreneur](/solutions/solopreneurs) starting out, the advice here will save you significant headaches. It will also lead you to create a successful business with *sustainable* growth.

By *sustainable* growth, I mean building a firm that:

1. Turns a reliable profit each year.
2. Provides everyone with a sensible balance between work and
free time.
3. Creates a culture and working environment that you can be proud
of.

If you already have a business, you will gain value by discovering new ways of doing things.

This book is structured in three parts:

1. **Start Building Momentum** — How to drum up business and set a strategy to ensure that you are consistently approached by new potential clients.
2. **Working With Humans** — How to deal with both team members and clients. How to treat people like human beings with [great teamwork](/resources/great-teamwork), not cogs in a machine.
3. **Run Things Effectively** — Once you have clients and team members, how to keep everything humming along nicely and ensure that the business is stable.


If you can do these three things — build momentum, learn to handle people, and get organized, — you’ll be well on your way to having a successful firm.

So, learn from my mistakes, and then get out there and make your own!

Enjoy reading,

Manny.

## How It Started

I was hesitant to include my own story in case I ran the risk of inflating my ego. That said, understanding some context always helps build a fuller picture.

My decision to write this came around my 30th birthday. It is a strange age because it means you’re now “an adult”, whatever that means.

I was born in Venice (Italy) and spent my formative years growing up in London.

At 22, I moved to Phnom Penh (Cambodia) to see my brother, who had been living there for nearly three years. Fast forward eight years, and I have built a couple of businesses I’m proud of that have a global client base. These turn a profit and have allowed me financial and location independence.

How this happened is what this book is about.

I had no previous experience in running any type of business. This lack of experience meant that I made many mistakes along the way.

In my first few years, I freelanced as a brand/web designer, working for very small amounts or even for free. In hindsight, this was the right strategy because it allowed me to build a solid foundation. I worked long hours and understood the fundamental issues that I would encounter again in the future.

I decided I wanted to build my own professional service firm, and asked one of my clients if they would invest. 

They agreed to do so — for reasons beyond me — and cut me a cheque for $75,000. This was more money than I had ever seen — and Mäd was born.

I started to build a team. I had this somewhat idealistic notion that I could make a company with no rules and lots of freedom — a place where everyone would work well together and do incredible work.

Within a year, I had to fire almost all my twenty-five team members.

Everyone pretty much hated me, and I had little money left in the bank. The Monday morning after I had fired most of my team, a delivery (and invoice!) arrived at my office for several dozen expensive office chairs. I used the last funds available to cover these chairs, which we still have today! All these chairs sat unused and wrapped in plastic on the far side of the office. This served as a lovely daily reminder of my complete failure as an entrepreneur.

Nowadays, I like to joke that this was the year I earned my MBA: this was by losing enough money to pay for an entire MBA course in a year.

Yet, not everyone and everything was gone, and we still had a handful of clients and a few core team members. After about a month of indecision on whether to just stop, I started working again. We started building momentum again.

Within six months, we moved to a new office, and things seemed to be going well. We managed to grow the team to over 30 people and had many clients and retainers, which helped keep us afloat. Then, I got complacent concerning the quality, and again things went sideways. We had many cancellations in a short period, and most of the team had to leave again.

At the time, I blamed various individuals for not doing their job. In reality, it was my fault for not building a resilient organization with high-quality standards.

And so, after this second large failure, I decided that I would try to do everything. I took on each functional role to understand what needed to be done — and how to do it. I wanted to understand everything from finance to [project management](/resources/project-management-basics-guide) to sales and marketing. My idea was that as soon as I could understand a functional area well, I could start to delegate it or automate it.

While somewhat stressful, this was educational. I started to question why certain things are done in specific ways. I asked myself, how can I radically simplify and even [automate critical processes](/resources/applying-critical-thinking-to-process-design)?

At the same time, I also decided to build my own project management system, called [Blue](/). The core idea was to create a system that was easy to use, affordable, and powerful. Something that could run complex and nuanced projects.

As Blue started to [gain features](/platform/changelog), these were battle-tested by Mäd. This kickstarted a virtuous circle where more usage meant more feedback. This, in turn, made us more thoughtful and organized. Blue now has thousands of organizations using the platform across the world. I’ll speak more about Blue when we discuss [effective project management](/resources/effective-project-management).

I do not consider Mäd a startup any longer. We’ve had a significant amount of time to work out how to do things right.

We’ve learned processes to deliver high-quality work on a repeatable and scalable basis. We have an excellent reputation, and almost all our work comes via word of mouth. The choices of dozens of leading brands and innovative organizations that work with us testify to this.

Throughout all these experiences, I took a lot of notes. Some days, I wrote thousands of words about what I was thinking. This includes notes on critical decisions as I made them, and then an analysis of what went wrong vs. what went right. This is a habit that I keep to this day, and something I encourage you to start doing.

This book leverages and condenses all these reflections.

It gives you access to a full seven years of experiments, failures, and successes. As mentioned in the introduction, this is the book I wish someone had given me when I first started. I could have avoided a lot of the problems I encountered.

That said, I am also, in some ways, happy that I experienced everything that I did because it has helped me be who I am today.

If you have any questions while reading this book, feel free to email me at [manny@blue.cc](mailto:manny@blue.cc), and good luck!