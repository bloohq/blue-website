---
title: No Rules Here
slug: no-rules-here
tags: ["agency-success-guide"]
description: There are no hard rules in this guide, only reflections and guidance that you can adapt to your situation.  
image: /resources/madoffice2.jpg
date: 02/06/2024
showdate: false
sitemap:
  loc: /resources/no-rules-here
  lastmod: 2024/06/02
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
Everything you read in this book is not a hard rule or the only way to do things. I am not trying to claim ultimate authority on anything. Lots of the advice I give will be entirely wrong for you and your organization.
You must be smart enough to figure out what’s right for you. Use these ideas as a springboard to create your own structures, as I did.

This book is what has worked for me in the last seven years, and what is working right now. Based on the facts I observe, I will continue to experiment, try new things, and replace what stops working in the future.

The best way to think about this book is as a starting point to try new experiments in your organization. Much like a scientist, follow this simple process:

1. Build a hypothesis and try something new.
2. Review the results. Update your working theories with a new
hypothesis. 
3. Try again.

There will undoubtedly be considerable differences between what works for you and what works for others — this should not be surprising.

A professional service firm often mirrors the personalities and quirks of the founder. This is because these types of businesses, more than any other, thrive on people and culture. This is because the people, culture, and processes *are* the products you sell to clients.

This comes down to the fact that your clients are hiring you because of **you** and your team’s expertise. They want to achieve something that they cannot achieve by themselves.

**This is a crucial point to remember.**

While you should take important things seriously, ensuring that you and your colleagues are having fun is essential — that's a [core component of great teamwork](/resources/great-teamwork). We’re all going to spend quite a bit of time at work, so it’s worth making that time both meaningful and enjoyable.

Remember, we’re not running a hospital or operating in a war zone, where decisions are life or death. The worst possible thing that can happen is that you waste some money or a project is late. It’s not the end of the world — even if it might appear that way in the heat of the moment.

This doesn’t mean everyone should be overly happy or falsely joyous at work. That will depend on the culture you develop. What it does mean is that, on balance, most people are enjoying most of their work, most of the time. 

You don’t need to aim for maximal results to be the absolute best place to work for everyone. Make a place on the better side of things, and don’t hesitate to invest in your environment and people.

Often, you can create a better workplace by doing things that don’t need money — only effort. Run engaging workshops, invite speakers, ask team members to share experiences, and so on. These things, over time, add to a better work environment.

No one is claiming that life in a professional service firm is easy, and by no means should it be. Being challenged regularly is excellent for personal development. You gain experience 3x to 5x faster in a service firm than on the client side.

So as you navigate through the rest of this book, remember that having fun is essential. Loving what you do helps to make any difficult times worth it, and it stops you from giving up.

**Let's start!**