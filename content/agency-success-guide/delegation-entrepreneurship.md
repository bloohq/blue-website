---
title: Delegation is Entrepreneurship
slug: delegation-entrepreneurship
tags: ["agency-success-guide"]
description: Learn how to delegate effectively so you can scale your agency to new heights.
image: /resources/delegation.jpg
date: 09/06/2024
showdate: false
sitemap:
  loc: /resources/delegation-entrepreneurship
  lastmod: 2024/06/09
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
Have you ever stopped to consider what a professional service firm *actually* is?

A common definition is:

> A business or organization providing a particular service on behalf of another business, person, or group.

Yet most firms start out with one person and their laptop. A freelancer with aspirations. A person going beyond themselves to start a brand with a higher purpose.

So let’s think about that further.

Even if you have strong skills that reflect your professional service firm offerings, you can only build a business and be an entrepreneur if you are able to delegate actions and achieve results consistently. 

Time is finite and valuable; as a leader, you need to protect your time for the most important business functions related to your role. You need to be working at the point of constraint, which is whatever is stopping the organization from getting to the next level.

For a professional service firm, the point of constraint is usually generating new leads and selling to those leads. The business will not grow if no new clients are being brought in. As the leader, it is your job to make sure that the sales pipeline is full and that consistent sales are being made.

The other common point of constraint is the effective management of multiple projects and the hiring of new talent.
To do this, you need to delegate other tasks to employees or outsource them altogether. This can be a difficult thing to do, as you may feel like you need to be involved in every aspect of the business. 

However, if you want to grow your business, you need to let go of some control and trust that others can handle certain tasks just as well as you can.

There’s often the temptation to do things yourself because you can trust yourself more than others. You know you’ll do it exactly as you want it to be done, and also, you might even do it much faster. It can be time-consuming to brief others on the problem, fill them in on your thinking, discuss solutions at length, then await their work and possibly feedback for further changes.

Yet, this process is crucial. It is the *only* way to build capacity in your team.

If you’ve surrounded yourself with capable talent, you simply need to ensure you can give clear instructions. Delegation will empower team members with responsibilities and build trust. Delegation is not, and should never be, micromanaging someone to do your work.

You need to define quality with clarity. Make the outcomes you expect explicitly clear. Once your team is aligned with your expectations, they’ll eventually begin to predict what you want.

This also has the added benefit of allowing others to upskill themselves and take responsibility. I’d go as far as to say that professional service firms offer some of the finest capacity-building for individuals out of all potential organizations they could work for. The fast-paced life and varied clients require them to think on their feet and readjust, readapt, and recalibrate often.

It’s good practice to hold appraisals for team members to keep them on track, and perhaps, as a leader, you’ll eventually be answering to a board, investors, or silent partners. But even so, I still recommend scheduling self-appraisals for your role. The benefit of this thought exercise is that you will analyze what you are doing on a day-to-day, week-to-week basis and compare it with your expectations of what a great leader should be doing for their professional service firm.

You may find that you’re spending too much time on client pitches, legal admin, or slow onboarding processes. Perhaps business development or brand positioning is suffering as you simply don’t have the time to focus on these tasks. You might even realize that you’re unnecessarily trampling over your own social life and health by taking on an excess of tasks that could be simply delegated.

**Delegation is one of your best weapons.**

Anything that you have to do repeatedly should be examined under a microscope. Is there a better way to accomplish this? Can it be delegated? What happens if we just stop doing this completely?

However, there are things that you should not delegate, and it is worth discussing the framework to use to decide what to delegate and what *not* to delegate.

The art of entrepreneurship is, in part, knowing *when* to delegate.

Delegation is a tricky thing. Generally speaking, if you’re a leader, you’ll be able to do most, if not all, of the responsibility of your subordinates at a higher quality than they can.

But, you cannot do ALL the work of your subordinates. There is simply too much to do.

And, if you try to do all the work of your subordinates, you will quickly become bogged down, and your business will suffer.

You need others to be able to do work required to a specific standard — it doesn’t have to be your standard; it just has to be good enough to be acceptable and move things forwards. And sometimes, you may get even higher quality work than you could achieve, especially if you hire the right people.

It is important to remember that delegation is not abdication. You are still responsible for the work being done — you are just delegating the actual work. Before we get into the mechanics of delegation, it is worth pausing to discuss the role of deep work. 

Suppose anyone on a team has technical responsibility, and I mean anyone who is supposed to generally do one thing (i.e., an accountant, a software developer, a designer, a data analyst, and so on). In that case, the critical thing is that they spend as much time as possible working on the right thing without disruptions and changes to the direction of their work. This ensures that they can regularly get into deep work, which is where the highest quality work can be achieved.

To make this happen, it is the leader’s responsibility to protect their time and work. This may mean saying no to requests for help or delegating tasks to others on the team. But, whatever the case may be, the leader needs to ensure that each individual on the team can get into deep work as much as possible.

Managers (not leaders) often fail to understand this, because the role of a manager is almost diametrically opposed to the role of a technical worker. The manager is there to align, coordinate, unblock, and report. This, if done badly, can mean a significant number of meetings. After a long enough career, the number of meetings themselves is seen as progress, and this thinking is extended to technical workers themselves, who should actually be protected from meetings as much as possible.

So, back to delegation.

The key thing a leader needs to identify is what work they should be delegating and which tasks they should handle themselves. This is often not as straightforward as it may first appear, especially if one does not have a model or framework in place to evaluate work.

The key point to remember is this:

> Leaders should spend the majority of their time working on the point of constraint; everything else should be delegated to someone else.

And if we extend delegation not just to handling work but to making decisions, then another way to see this is that anything that is highly consequential and irreversible should never be delegated, and anything under a specific amount of consequentialism (with an arbitrary cap of, say, 50%) should always be delegated.

So, what is left? A sweet spot of sorts, in which the leader needs to use their judgment to decide if they should delegate a task or not.

A leader needs to remember when delegating that they are delegating work, not responsibility. The leader is still responsible for the work being done; they are just not doing the work themselves. This means that the leader needs to be clear about what is expected from whomever they are delegating to, and they need to be clear about what the standards are.
The leader also needs to be available to provide support and answer questions, but they should not be expected to do the work themselves.

Delegation can be difficult for many leaders, especially those who have come up through the ranks by being “go-getters” and taking on everything themselves. But, it is a necessary skill for any leader and a skill that can be learned.
The critical thing is to start small, with tasks that are not too important and not too consequential. And, as one gets more comfortable with delegation, to delegate more and more.

So that is all about when to delegate; the next question becomes how to delegate. There is a continuum between giving clear instructions and getting precisely what you want out of people, and being a suffocating micro-manager that does not let individuals use their brains and initiative to tackle work and decisions the best way they see fit based on the unique circumstances at the time.

I generally prefer to try and let individuals I trust to figure out as much as possible by themselves, but that requires trust, which is not something that is gained immediately but is built up over time.

So, I tend to be closer to the micro-manager with individuals I have not had the chance to work much with, and far looser with individuals I have worked with for an extended period of time.

When it’s the latter arrangement, I like to focus my brief of the work on some of the key risks, the key expectations of results, and specific hard timelines. I always let the individual know that I am a resource they can access at any time if they have questions or get stuck, and I trust them not to abuse that level of accessibility. 

If the specific task or decision is something I am an expert in, I may also give general pointers towards the “how” of the execution. Still, I think it is generally best to let people figure things out themselves and build their skills in this manner.

On the other hand, when it’s the former arrangement, and I am working with someone new or on something highly consequential, I tend to be much more specific in my delegation. I will go over not just what I expect as an outcome but how I want that outcome to be achieved. This level of detail can lead to micromanagement if one is not careful, but it can also help avoid major disasters.

The key here is always to keep communication open so that the leader knows what is going on and can provide guidance without stifling initiative and creativity. This is good because it can enable discussions to center around outcomes, not just inputs. Delegation becomes far more about providing clarity about the long-term outcomes than the short-term inputs.

I’ve found that the primary problem for businesses (and their leaders) is that there will always be too much to do. Startups often lack the resources to achieve many things in a short time, but so do large organizations — unfortunately, work will always catch up with how much resources you have.

This is also precisely why it is important to learn how and when to delegate work.

If you are just starting out as a leader, you will be working on things you are not good at and doing unfamiliar tasks for the very first time. 

Within your business, you might have to take on all the roles of CEO/CFO/CTO/Head of Marketing/Human Resources — and more! And although startup founders often have a background in some disciplines — CEOs might have marketing experience, and if they are in tech, will more often than not have coding skills, — they will still be dealing with uncertainty in other areas, making tasks take much longer than they should.

Not only will you have an overwhelming number of responsibilities, but you will also have many different stakeholders whom you need to dedicate your time to. 

These could be:

- Investors
- Board of Directors
- Advisors
- Clients
- Partners
- Suppliers
- Staff
- Press
- Family & Friends!

So how can you divide your time among all these people and tasks?

Well, let’s start with the fact that there are 168 hours in a week. Everyone who has ever existed has had this same amount of time each week. A week will always last a week regardless of what you do.

As a leader, you can utilize these 168 hours to follow a relaxed schedule, or you can take on a more aggressive approach. You’ll allot some time for work, sleep, and personal life. Within your working timeframe, you will also have to dedicate time to various stakeholders.

There’s a simple equation you can use to calculate how much time you can make for each person or activity:

1. Count up your working time
2. Divide by all your stakeholders
3. Simple: You are now giving everyone the time they need!

But, this is not a very good way of thinking, because it ignores a few fundamental factors.

Unfortunately, many leaders fall into the trap of uniform time allocation, where they try to please all stakeholder groups with attention. They will make time for everyone.

The problem here is that by diluting their attention across so many groups, they are not able to spend much time on the point of constraint.

Also, humans will inevitably have different working habits and varying levels of patience and tolerance for how long they can wait. It would not be a very good idea to give each stakeholder or task the same amount of time: after all, some people are more independent others need more of your attention, and some topics of discussion with these stakeholders will have higher stakes than others.

So, my key point here is that you need to be mindful of diverse stakeholders and their characteristics. In the same way, you must also evaluate the tasks on your list.

Not all the things on your to-do list are equally important or equally urgent. The outcomes of completing different tasks will have varying degrees of impact. As a leader, it is up to you to set your priorities straight — that is also where you should find the point of constraint. Ultimately, your main responsibility should be to only focus on the point of constraint.

All this is to say that there will eventually be a pressing need to delegate work. Now the question is: how do you know which work you should handle yourself, and what could be done by someone else?

Generally, there is a pretty obvious and simple approach to tackling issues that are points of constraint:

• Find what is a point of constraint.
• Fix it!
• Give it to someone else to manage, and move on.

But before that last step of handing over the responsibility to someone else — delegation — a leader’s task is to find and fix the point of constraint first.

In the context of running your business, as a leader, you can (and will have to) take on different roles, just as I mentioned earlier. This could be anything from keeping track of finances as CFO, recruiting and hiring team members as Head of HR, or coming up with a strategy for Sales and Marketing.

Similarly, when dealing with issues that are standing in the way of progress, a useful skill to have is the ability to switch between different hats (figuratively, of course).

When I say that someone should wear different hats, what I actually mean is having different mental models in your toolbox to deal with different situations.

There are five mental archetypes that can be used whenever you find yourself working at the point of constraint:

1. The Learner 
2. The Architect 
3. The Engineer 
4. The Coach
5. The Player

Let’s go through each of them in detail.

## The Learner Hat

I consider this a fundamental role, and I think that everyone, not just leaders, should never take this hat off. 

Having a learning mindset as a leader is perhaps one of the most important things that you can do, for multiple reasons.

Firstly, it sets a great example for the rest of the organization, encouraging your team members to always seek knowledge both from you and from other sources. This can be invaluable when you start delegating tasks to others.

Secondly, the world is constantly and rapidly changing, so the expiry date of knowledge is becoming shorter and shorter. 
So, whatever you know and learn right now will likely become outdated quickly and may well be useless in a few years’ time. This also applies to business knowledge and will be more prominent in some industries more than others.

Tip: You can (and should try to!) also learn a lot from listening to other people and their experiences by asking the right questions.

## The Architect Hat

With the Architect hat, your goal is to identify things that you should and should not be focusing on with regard to the point of constraint. It’s about thinking of the long-term vision and connecting what you are doing right now to the results that you want to see a long way down the road — just like an architect makes decisions for the design of a structure based on the purpose of the final building.

The key point here is that you can waste tremendous effort on a bad strategy and get nowhere, and conversely, you can have a moderate effort on a great strategy and get superb results without too much sweating. After all, if you’re going the wrong way, it doesn’t matter how fast you’re going: lots of effort will not overcome a bad strategy that has already been executed!

In terms of business, when you wear this hat, you should be thinking about:

- **How to be different?** It’s unlikely that you’ll have no competitors, so think about how you can create and clearly communicate what makes your business different from everyone else.
- **What won’t change in the long term?** It is tempting to always focus on new stuff, but ensuring that you understand what the stable factors in your industry are and continuing to invest in them is something that is often overlooked. After all, nobody is going to wake up in ten years time and wish that your product was harder to use, took longer to deliver, or was more expensive.
- **Developing an irresistible offer.** Essentially, building offers for your various stakeholder groups that are difficult to refuse — not because of extortion, but because the offer is so good.

## The Engineer Hat

If you are wearing the Architect hat, you are asking, *“What?”*. But when you are wearing the Engineer hat, you are asking, *“How?”* — specifically, how can you make things better?

My main advice here is to set up systems and constantly find ways to improve workflows. At Mäd, we have an entire system dedicated to building SOPs (Standard Operating Procedures) that has helped the team create consistently high-level work.

SOPs are high-level instructions on how processes should look like and how to execute them, so they are especially helpful when delegating work to other people to guide them through tasks step- by-step.

Basically, anything that has been done more than once, and will likely be repeated again, should be written down.
An SOP system sounds complex, but it can really just be a simple Word doc. I strongly encourage you to make your own set, and your business will reap the benefits right away.

## The Coach Hat

[In Chapter 4](/resources/nurturing-top-talent), I discussed the analogy of a business as a professional sports team (versus a family). Here, the leader is the head coach, with the key responsibility of finding and developing the best talent while keeping track of the non-performers.

In this kind of culture, the most important thing is not always what you are doing, but what the team working on your business is doing. There will never be a totally perfect team member, and when you wear the Coach hat, your job is to build up their strengths and cover their weaknesses.

This also applies when delegating tasks to other people. 

Don’t expect everyone to immediately know what to do — guide and support them through the process until you see that they’re capable of doing things the way they should be done.

Also, always have a human approach, but don’t be afraid to call out issues as they happen. The reality is that as a business evolves, certain people end up being bad fits, and would probably do much better elsewhere.

## The Player Hat

Often, a leader is a great individual performer, which is what led them to become leaders in the first place. So it’s only natural that they should occasionally jump in, get their hands dirty, and actually do the core work that their business requires — that is, wearing the Player hat.

This is a fantastic way to gain additional insight, as well as develop empathy with the people who actually do the work each day, and find how both their work performance and overall lives can be improved. It can also be great in an emergency if there is suddenly a large workload, or someone is absent.

However, leaders should be careful not to overuse their Player hat, especially if it is to cover underperforming team members. The priority, in this case, would be to fix the performance issue, not cover for the team member.

The best situation for a leader to be a Player is at the point of constraint. This is when you should really focus on doing things yourself.

If you don’t know where to start in terms of finding and fixing points of constraint, then this might mean that the point of constraint right now is you — and so the most important thing for you to do is to improve and learn as much as possible.

If you suspect that you have business model problems, then the Architect hat is the most appropriate. The following points are worth exploring:

- **Lack of momentum.** If you’re not growing above inflation year on year, and growth is difficult, then you’re probably suffering from a lack of momentum.
- **Capital-intensive.** If you’re buying revenue (i.e., spending $1.50 to get $1 from customers) or if you need to go to the bank each time you get a new large order, then this is definitely a point of constraint.
- **Low gross margins or profits.** If your gross profit is less than 50%, or if your profits are less than 20%, then this is holding you back.
- **Low recurring revenue.** If 50% of your revenue is not recurring, this may be a serious point of constraint.

If you believe that team issues are what is holding you back, then put your Coach hat on and consider the following:

- Would you rehire a current employee right now? If not, then they should go.
- Would you fight to keep a current employee if they told you that they were going to leave? If not, then they should go.
- If, as an organization, there is little commitment to learning, then this is worth investigating.
- If individuals are putting themselves or their departments above the company, then often this means that there is little alignment.
- If ideas and energy don’t bubble up to you all the time, and if you ask for feedback and get little in return, then this means that your team doesn’t care that much, and that’s something to also
address.

If you think you might have process problems, then wear the Engineer Hat, and start by asking:

- Are we delivering our products or services at the quality our customers expect?
- Are our processes streamlined enough? 
- How can we improve them? 

If you have a recurring need to hire superstars for every position, this might also be a cause for concern. Great process allows even moderate performers to do good work. Think of McDonald’s, who have managed to build a global multi-billion dollar brand by mostly employing rather unmotivated, paid-by- the-hour teenagers.

So, as a business founder, you should expect to work on different functional areas of your organization throughout the day and also be able to change mental hats whenever needed.

Flexibility and adaptability are invaluable skills for a leader because you will have to do much more than just boss people around. You will often have to learn how to do most of their work, too — sometimes even before they do.
That said, even though it’s crucial to find a productive flow that will produce high-quality results, I also strongly believe that work should be fun.

We inevitably spend quite a lot of time at our jobs, and having a dynamic, fun environment where truly great work is accomplished should not be overlooked. This is what will help you keep a talented team of high achievers who share your goal of driving the business and seeing it succeed.