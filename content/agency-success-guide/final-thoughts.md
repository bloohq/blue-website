---
title: Final Thoughts
slug: final-thoughts
tags: ["agency-success-guide"]
description: Wrapping up everything that we have covered in the Agency Success Guide.
image: /resources/blueceo.jpg
date: 16/06/2024
showdate: false
sitemap:
  loc: /resources/final-thoughts
  lastmod: 2024/06/16
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
Okay, so that’s it. Ten years of reflections on how to do business, distilled into what I hope was an efficient and easy-to-understand manner.

I set out writing this book with the intention of sharing my experiences in building and running a strategic consultancy from scratch. While I wish someone could have given me this knowledge before I had time to fail and start over a couple of times, it was an invaluable journey nonetheless, and I hope I’ve now succeeded in guiding you on your path to becoming a professional service firm leader.

The principles I’ve laid out are a consolidation of all the things that have helped me overcome the many roadblocks along the way and learn how to make better decisions. My goal — and I guess yours, if you’re reading this! — is to set up a business that is both profitable and sustainable to run. And, not to mention, enjoyable!

You’ll have learned some actionable steps and useful frameworks from this book that you can refer to when you are conflicted in your decisions. Just a piece of advice: make it a habit to use frameworks as a qualifying tool for various stages of your work.

If you’re the CEO, you need to always focus on the most important thing — this is quite straightforward to write, but not quite so easy in practice. If you’re not 100% sure, remember these three general points:

1. Don’t run out of cash.
2. Give your team a clear vision.
3. Provide the resources your team needs to achieve this vision.

