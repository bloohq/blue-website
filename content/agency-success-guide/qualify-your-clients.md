---
title: Qualify Your Clients
slug: qualify-your-clients
tags: ["agency-success-guide"]
description: Learn how to find the best type of clients — the ones that are a pleasure to work with and profitable.
image: /resources/qualify-clients.jpeg
date: 10/06/2024
showdate: false
sitemap:
  loc: /resources/qualify-your-clients
  lastmod: 2024/06/10
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
**Your ability to work well with clients will be one of the deciding factors of whether you are successful in the long run.**

Ideally, clients need to be comfortable placing a significant amount of trust in you, and so they need to view you as an expert that they can rely on.

That said, you’re not their employee, and you’ll need to be professional enough to be able to push back on requests and opinions that you don’t agree with. If you learn to do this properly, you’ll gain even more trust and respect from them.
When you’re first starting out, the fact that someone may actually trust you enough to open their wallet and pay you to do anything might seem quite incredible.

I still vividly remember my first-ever project, which was worth a grand total $1,200. I remember going home, telling my brother, and being completely overjoyed... I never really managed to recreate that peak of excitement, even for projects which were orders of magnitude larger than this original freelancing job.

However, as I grew and scaled my business, I realized that not all clients are the same. Sure, they all pay us with the same thing — money — but everything else is different.

Some clients are from interesting backgrounds and industries and are a pleasure to work with. Others are demanding, take up a lot of time, and change their mind every single week. The difference between good clients and bad clients is huge. If you have good clients, going to work is going to feel great, and your team will be happy and do great work, and the opposite will happen if you have rude, micro-managing, and hard-headed clients.

So, it starts to make sense to ask ourselves, what’s the ideal client, and why? What framework do we use to decide whether we should take on a particular project or not?

At a very basic level, you need to take on new work because the business needs to bring in cash to pay for the expenses that are incurred. 

However, there are also other motivations for taking on new work:

### It gives you a new challenge. 

This is often overlooked. It’s easier to become stagnant and be the big fish in a small pool by taking on projects that you’re entirely competent in, but this can lead to complacency, and slow growth and development.

Stepping out of your comfort zone to tackle new industries and new problems allows you to increase your knowledge base and opportunities. As long as your toolbox of methodologies is agile enough, you’ll be able to navigate through unknown areas. The key thing with these types of projects and clients is that while they may stretch your skills, they are still within a level of being achievable.
### The client is a global or well-known brand. 

This can be a game- changer. Having a leading brand on your client list can turn heads and attract a steady flow of work. It is also a great experience to discover, firsthand, the inner workings of such organizations — you may gain some great new working practices from them, too, making the project mutually beneficial.

To this day, we still have clients intrigued (and convinced) by some of the very first projects we took on purely because of the brand names of those original clients.

### The client is an organization or cause that you strongly believe in. 

Earning money is great, but making a difference can feel just as good, if not better. CSR isn’t just a check box on a marketing plan, but a chance for you to be the change you want to see.

### And finally, you just genuinely like the client team members and want to work with them. 

Will it be good for business? Maybe. 

Your happiness is certainly likely to improve, and feeling good at work is always advantageous. When you click with a client, you’re likely to produce some of your best work and be fueled with positivity.
The business landscape can be cut-throat and disingenuous, so it’s always refreshing to create meaningful relationships and output for people — not just for money. We’re human-centric at Mäd; after all, relationships matter more than transactions.

For now, let’s put aside the discussion of money, as we will discuss that at length in Chapter 7. If a client is willing to pay enough to make it worthwhile, it is sometimes fine to take on difficult clients and ensure that you can hit your financial objectives, but this should be a conscious choice, not just a mistake.

So, let’s answer the two points we asked ourselves above about what makes the client ‘ideal’, and how we can decide whether to take on the project.

The *ideal* client is one that allows you and your team to do their best work, and moves your organization forward in some way.

Ideally, they should pay well and on time, understand where their expertise ends and yours begins, give directional, not executional, feedback, and not micromanage.

Aside from this, there are some other key considerations. 

Ideally, your client will be easy to service; by this, I mean that they won’t drain your time unnecessarily. For example, creating content for a client can be incredibly straightforward, whereby your team will research, draft, and present the usable content. But it could equally become a time drain if the client wishes to work reactively and nitpick on every word used. The best clients commit to structured project management agreements—such as a weekly project meeting, or specific feedback loop geared towards efficiency.

Next, and this may seem obvious, they should pay well. Higher paying quality clients allow your business to be pickier with other clients, and a healthy cash flow opens up opportunities to say no to well-paid jobs that aren’t an ideal client fit.

Speaking of higher quality, great clients open up ideal referral opportunities. Word of mouth is still the best way to gain new clients. By letting your work and your clients do the heavy lifting for your communication strategies, your team can focus their effort more on continuing high-quality client work.

We also find that some of our favorite clients come back time and time again for future projects. It is much easier to sell to someone already engaged and invested in your brand and services or products. As the COVID-19 pandemic showed, businesses need to be prepared for the unexpected. Having long-term clients gives an extra layer of security. Also, the ‘pitch’ stage becomes much easier as you don’t need to stress about first impressions or cold sales. 

If the client knows you, likes working with you, and is happy with your previous work, you’re the baseline, and it will be difficult to beat.

Many professional firms talk proudly about *‘firing clients’*, which also sounds a little odd. Yet business relationships are akin to everyday relationships and friendships too. 

Some people simply clash. 

A perfect client will be aligned with your values and be open- minded about your processes. After all, they’re coming to you for your expertise, so surely they should respect how you work, right? Establishing harmony early on in a project can have a huge impact on outcomes. It’s a breath of fresh air when you understand the client’s goals, and they trust in that understanding to give you the freedom to tackle their problems your way.

And finally, continuing on from that harmony, the ideal client knows their role. The role of a client is to give directional feedback, not executional. Professional service firms are teams of experts that can be leveraged to solve complex problems in short periods, replacing the need for clients to hire additional team members or go through lengthy training and upskilling. A client that knows their limits and understands the rationale behind trusting the firm is a foundational pillar for good projects.

When a new brief comes in, or you are in discussions with a client about a new project, you need a methodology for clearly evaluating the proposed project to see if it is a good fit and something that you and your team should spend time working on.

The first thing you should ask yourself is whether this project is something that you and your team will enjoy working on. This may seem like a strange initial question to ask, but the reality of things is that we are going to spend a lot of time working, and it is important that we wake up each morning and look forward to our working day. 

Granted, it is idealistic and utopian to think that every single day is going to be like this, but if it happens more often than not, we are already winning!

So, one of the biggest components of enjoying your work life is being careful about who you work with and what you work on. So, you need to be careful about the projects and clients you decide to take on, because it is likely that they will significantly impact your overall life for the duration of the working relationship.

A bad project or client will cause you to overwork yourself, to spend the time in bed when you are trying to sleep thinking about stupid details, and generally not live a balanced life.

So ask yourself: *will you enjoy working on the project?*

Next up, the question to ask is whether this project will advance your organization forwards in some way.

This question is purposefully vague because this can mean so many different things. Perhaps the client is extremely well connected, and this will lead to lots of word-of-mouth leads in the future. Or, there are some interesting things as part of the project, which means that you will learn something new that you can apply to your future work, or there is a way to templatize the work to speed up future work.

It could even be as simple as the fact that the project is for a well- known brand and that you can add their logo to your client list, which will look great and make closing more deals in the future even easier.

Finally, there is also a well-known framework that you can use, which will help you not waste too much time on clients or projects which are not a good fit. 

It’s called the BANT framework, and the acronym is as follows:

- Budget
- Authority
- Need
- Timelines

## Budget
Obviously, the Budget is an essential part of the conversation. If your client can’t afford to work with you, there’s no moving forward. So much time can be wasted if you haven’t qualified the budget early on. It can be easy to spend lots of time and energy preparing detailed proposals, researching the client, and nurturing the relationship and project—but all this work is ultimately misplaced if your client’s expectations on price don’t align with reality.

## Authority
When considering Authority, we need to analyze who the decision- makers are. Are you speaking to them? If not, the chances of you winning the contract will be dramatically lower. By determining relevant stakeholders and decision-makers, you can shape your work to suit their needs and expectations. If your client contact doesn’t have much decision-making power of their own, it could be a sign that the project is less likely to progress.

## Need
The Need should be analyzed. If the client and their company face a challenge that is blocking them from desirable impact, they will be more motivated to fix this. Sometimes you might find that your products or services don’t adequately address their needs or even that their problem is more of an inconvenience than a pressing need. The key here is honesty. The greater the need and the more accurate a fit that your solutions have, the higher the chance the project will be able to flow at a steady pace. Remember, if the challenge isn’t a particularly impactful concern, it’s less likely to get stakeholder attention, which could lead to delays.

## Timelines

Finally, you need to consider the buying Timeline for the client. Are you solving a painful and urgent pain point? If yes, then they will want to start right away. If not, maybe this project is just in the exploratory stages, and it may be more than six months before it starts. Ideally, you want the shortest possible sales cycle; thus, you want to enquire early on during the sales process with regard to the potential project start date.

Ultimately, the client should have clear answers already prepared for their BANT requirements. The four areas interweave to make them equally important.

Unless the client has all four, it’s unlikely that the brief or discussions will lead to anything more than frustration for you. With that in mind, there are cases where a client may not have an idea of what a sensible budget is for the project at hand because they have never purchased this type of service before. If that’s the case, then it is your responsibility to educate the client on the value of your services and how a successful project will benefit them.

## Give Clients Work

To ensure that you do not waste too much time when pitching for new projects, it’s important to give the potential client enough work so that they can prove that they are serious about engaging you. Exactly what you ask is not as important as how they act and respond to your request. You can request for extra meetings, for a write-up on certain ideas, for data, examples from their industries, etc.

This can achieve two key things:

### 1. It gives you a preview of what working with the client is like.

If they take a while to send you what you need, and all their documents are a complete disorganized mess, then you know what you are dealing with, and you can price your proposal to build in the extra work that will be required for you or your team to manage the disorganization.

### 2. It can also show you if the client is serious or not. 

If they give you typical excuses (like they don’t have time or they don’t think your request is valid or required), then:

1.  They are either not serious about the project and are just casually shopping around with regards to a vague idea that they have, or
2. They know already who they want to work with (hint: *it’s not you!*) and they are just getting a few additional quotes to fulfill their buying process.

Generally speaking, we like to have at least one meeting with a potential client in our office if they are based locally, because this allows them to see our operations, spend some time and commitment to get to know us, and be impressed with our facilities. If you have a decent-looking office, this should be a key part of your selling process and one of the things you ask your potential clients.

So, don’t let all the effort and work during the sales process fall on your shoulders — give the client something to do and make sure that they’re serious.