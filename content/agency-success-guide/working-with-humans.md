---
title: Working With Humans
slug: working-with-humans
tags: ["agency-success-guide"]
description: Working with clients and team members is key to growing a successful agency.
image: /resources/workwithhumans.jpeg
date: 07/06/2024
showdate: false
sitemap:
  loc: /resources/working-with-humans
  lastmod: 2024/06/07
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
If you are running a professional service firm, your level of success will be directly linked to your ability to deal with people. The more effectively you can communicate with your team members and convince clients, the easier it will be to run your firm.

One mistake that I have often seen, especially when people are starting out, is that they want to try and appear like a large organization, even when they are a two or three-person team. They place multiple email addresses on their website as if they are different departments, and they attempt to keep clients at arm’s length.

This is a shame because they are neglecting one of their key advantages — *the fact that they are a small firm and can provide highly personalized services!*

So if you are small or just starting out, use your size to do things that large organizations simply cannot do. Be much more informal with regard to your communication and be much closer to your clients.

**Use this advantage!**

Don’t try to sound like a big company. 

Instead, be... human! 

Use your unique personality to sound like a real, honest person. This is refreshing, and clients love it. Instead of feeling that they are emailing a random suit in a five-thousand-person organization, they are actually speaking with a real person.

There is a long-term trend now that smart managers and leaders are often turning to smaller boutique firms. This occurs for several reasons:

1. **Attention.** If you pick a 20-person professional service firm and you’re a large company, you’re likely to be one of, if not the largest, customers. This means that you are going to get a significant amount of attention — far more than if you go for a multinational consultancy or professional service firm where your account is just a number.

2. **Quality.** It’s a known fact that smaller teams outperform larger ones, and you don’t need huge teams to achieve big results, especially with regards to knowledge work and technology.

3. **Less Bullshit.** The owner of the firm is reachable and responsible, things move quickly and with less paperwork, and more actual work gets done!

With that in mind, if you are running a smaller professional service firm or even just starting, try to communicate these value propositions to your current and potential clients — it will work!

There is also a fantastic bit of advice that I read in a book called Better by Atul Gawande, which deals with various problems and opportunities in modern health. 

The author advises doctors to ask at least one question during a patient visit that has nothing to do with their health problems. It could be something as simple as asking if they saw the game last night or anything else that’s trivial.

This advice works just as well when working and dealing with clients. During meetings, especially at those moments before the meeting officially starts or at the end when everyone is packing their stuff away, use that time to strike up a conversation or ask a question that’s not related to the work. 

But don’t think of this as a strategy; just be genuinely interested in connecting with your clients as real human beings. 

Marvin Bower, one of the historical leaders of the global consulting firm McKinsey, was a fan of tight client relationships, and he often used lunches and dinners as a way to stay close to clients and top of mind.

Treat your clients as human beings: *you might just find out that they are!*