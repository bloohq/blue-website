---
title: Cash Is King
slug: cash-is-king
tags: ["agency-success-guide"]
description: Cash flow is the most critical component of running an agency, learn how to manage it so you always have a good operating buffer.
image: /resources/madcouchbar.jpg
date: 13/06/2024
showdate: false
sitemap:
  loc: /resources/cash-is-king
  lastmod: 2024/06/13
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---

## Choosing Your Engagement Model
Depending on the precise nature of the work that you do with your clients, you will have several different engagement models available to you. These are various ways of measuring your work and deliverables, and of getting paid. Choosing the right engagement model is crucial in ensuring that your business is successful. It is also likely that you will have multiple engagement models in your business at the same time on different projects with different clients.
Two of the most common engagement models are fixed project price and retainers.

The fixed project price is quite clear: you give a price, often broken down by line items or deliverables, for the work that you will do on behalf of your client. The main advantage from the client’s perspective is that they have security that the budget won’t change. One key advantage for the professional service firm is that if you are able to be efficient, your effective hourly rate can shoot through the roof because you are no longer tying your earnings to the amount of time you are spending on a project.

I personally really like the fixed-price approach, as long as the scope of work is extremely clear. You can run into major issues with this methodology if you have a vague scope, and then there is a difference in expectations and deliverables between you and your client. This can be the cause of many headaches and back- and-forth negotiations on price changes. If the client changes their mind on any part of the scope of work during the project (which often is not an unreasonable thing to do as more and more work is done and the available data or circumstances change), then you have to renegotiate each time this happens. This can be quite time- consuming for both parties.

One method to keep working on a fixed-price basis is to always split projects into steps. For instance, if you are building a mobile application or website on behalf of a client, you might engage initially only for the design and specifications phase, where the key deliverable of that phase is to define the exact scope of work of the development phase, after which you can quote a fixed price for that work accurately.

This is the exact approach that many construction companies use to quote for the construction of a building. There is an interesting message that you can give to clients when you are pitching a project in this manner. You can essentially tell them that anyone else that is pitching the project in its entirety is typically just guessing, and they are either overpricing to account for a large degree of risk, or underpricing – and then they want to make it up with costly and time-consuming change requests once they have won the project.

The other popular engagement model is to simply keep track of each team member’s hours and then charge a fixed price per hour for the work done. I personally do not like this approach for anything except some types of maintenance contracts where you simply need to keep things running or smaller projects that will only last up to 20 or 30 hours.

That said, I am aware that there are firms that run exclusively on this model, and that works well for them. There are a few significant issues I see with this model, the main one being that it misaligns the professional service firm and the client, which is never a good thing. For instance, if you hire better people who can do the work more efficiently, they will spend less time to do the same amount of work — thus reducing the billing. This is obviously absurd, because you end up doing better work, faster, and yet charging less!

Additionally, this method also encourages the client to start to micromanage you and your team, and it may even encourage you to begin micromanaging your own team. Clients will scrutinize invoices and reports of hourly work, and if you go down the route of “proving’’ that you are doing the work by tracking team members, you’re probably going to mess up your culture completely. I don’t know of a single talented individual who wants to have his computer tracked and each of his or her actions logged and have to give detailed reports of their entire work day.

However, one practice that I’ve found to work that involves a degree of tracking is logging self-reported hours. As a service firm, you will be working with more than one client at a time, and there needs to be some sort of separation between the amount of time your team spends on each client. So I recommend letting your team experiment with loosely keeping track of their hours, just so both you and your client are aware of how much time is needed to complete tasks for specific projects.

## The Power of Recurring Revenue

The best type of revenue is always recurring – this is the gold standard that all businesses want to have. If you run a business that has over 50% of the revenue recurring month to month, you have a great business on your hands.
This is why over the last few decades many things have become “as a service”. You don’t buy movies or music anymore; you simply subscribe to a service. If you have built a strong enough relationship with a client after a few projects, either on a fixed price or hourly contract, you can then approach them for a retainer contract.

Ideally, this is not tied to a specific set of deliverables or hours, but purely based on a specific outcome that you want to achieve. This is not an easy sell, but if you can pull it off, you can have some serious high-margin recurring revenue. What we’ve done at Mäd is that our recurring revenue covers all of our staffing and office costs, and then all the fixed-price projects are essentially pure profit. This makes financial planning significantly easier in the long run.

So, the question is: *how do we get to this level?*

## Crafting the Perfect Retainer Pitch

As mentioned, you cannot pitch this immediately when a new prospective client walks in through the door. There isn’t enough trust for you to do so, and they have not yet experienced the tangible results that you can provide.
I think the easiest way for me to showcase the type of messaging is to simply copy a sample introduction letter that I attached to a recent retainer proposal. 

See below:

--- 
Dear Stakeholder,

I am pleased to present to you our proposal for a retainer agreement between [Client name] and Mäd. The purpose of this is to ensure that [Client name] can respond quickly to changing business conditions, while simultaneously receiving great value for money for the services rendered.

Instead of having to give out each block of work in an ad-hoc manner, with long lead times for proposals, contract negotiations, and bidding, our team will become your team for the duration of the contract. This means that we not only deepen our relationship and understanding of your business, but that we are immediately available to execute on your various projects required to move your objectives forwards.

This has the following advantages for you:

- **Cost Reduction.** By having one integrated team, you save a significant amount of money (50%+) compared to hiring multiple firms for the same work. In fact, when you consider all the overheads and time required to build a high-quality in-house team, a retainer is significantly more cost-effective as you do not have to consider additional office space, equipment, and benefits. Additionally, because a retainer agreement allows us to plan our long-term cash flow with more confidence, we are happy to reduce the typical project margin to have a long-term relationship with your organization.
- **Speed.** We can work with your various departments to plan work one to three months ahead, and be immediately available to execute, without you having to constantly do individual biddings and briefs for each new small amount of work required. This saves your team time, and gives you better results, faster.
- **Higher Quality Work.** This happens not only because we learn to work better together over time and that we understand your business better, but also because there are strong benefits to a fully-integrated approach where there is one cross-functional team that is executing across various deliverables and needs. This ensures that there is a strong consistency across all the work, and that insights from one type of work directly influence the final output of different types of work.
- **Reliability.** We can completely abstract away the issues with building in-house teams like the lead times to find individuals, sick and planned leave, and staff turnover, as we have multiple team members for each role that can cover others’ responsibilities with ease.

This messaging needs to be tweaked on a client-by-client basis, as different clients will have different priorities. One client may love the idea of the cost reductions, while another is far keener on the reliability and speed that’s available. Don’t discount the importance, from a client perspective, of having a team of consultants that know their specific business very deeply over a long period of time.

**Understanding your client cannot be understated.** 

You may predetermine the ideal client values and mindset for your professional service firm, but undoubtedly you’ll meet a vast array of thinking and approaches. Having your introduction pointed directly at the values of your client will help you stand out amongst potential competitors and help show that you understand them and their business needs.

This reflects our thinking on premade solutions. 

Yes, it is advantageous to have prewritten copy-banks so that you can craft an ideal introduction letter at speed without omitting valuable points – but you need to ensure that you are tweaking the content to speak directly to the client. 

Copy-pasted content, by nature, is impersonal; there’s simply no single ‘perfect’ answer for what to say and how to say it. While we do have a master proposal template detailing all the typical requirements that are common among our projects, we always expand on this to ensure we’re directly addressing the individual needs of each client.

So to summarize, seek to build trust by being genuine and cutting to the heart of what the client wants and values. Spend time figuring out what is most important to the client and follow our suggested introduction letter as a general guideline.

## Financial Fundamentals

For many leaders, finance is often something that’s annoying and never quite in check. It is seen as a distraction in the way of getting the real work done, and this is quite understandable. It’s just dry and boring, and not why we started a business in the first place.

However, money is the oxygen of business, and without money, a business stops breathing.

If you’re constantly chasing your tail and worrying about paying bills, then this will affect the way you and your team work.

So, you’ve got two choices, really:

1. Worry about getting good financial discipline, so you have solid books and a great business model.
2. Or, worry about the precise due date of every invoice so you can pay your staff and rent each month because you don’t have the necessary cash reserves and financial discipline.

Obviously, the first option is preferable, so let’s discuss some of the key things here.

## The Magic of Margins

**Firstly, you need a minimum of 50% gross margin on your work.**

This means that the cost of the wages that you pay your staff is equal to or less than half of what the client is paying you.

This is because you still need to pay for all your other operating expenses, such as an office, utilities, and hundreds of small things that you never think about, especially when you’re starting a business for the very first time. You also want to be able to build cash flows as a safety net, as well as to make long-term investments in your business.

**Secondly, you need to have a net profit margin of at least 25%, and ideally 30%.** 

This means that for every $100 you make, you need to have $30 left over once you’ve paid everything — and I mean absolutely everything, including your own wage as a business owner. This, generally speaking, means that you’re actually tripling the price of your team members as the price to the clients — which is an ideal way to work out how much you should aim to charge clients if you’re ever in doubt.

This is important, so let me repeat this. In an ideal world, you’re tripling your staff cost as a method to work out billings to clients.

So if you’re paying someone $4,000 per month, and they spend around 130 hours on client work (let’s say 160 of standard working time per week minus holidays and internal meetings), that means that your cost per client billable hour is $4,000/130 which is approximately $30/hour, and you need to charge clients $90/hour to ensure you have the correct margins. 

If you do this calculation and it feels slightly on the high side based on your current typical billings, this is good — it means you’ve got room to grow your billables!

To ensure that you keep the correct margins, you have a few approaches available. 

This can either be done by tracking the precise hours your staff work and bill to clients, or more intuitively just by charging a premium and keeping your eye on the books. While in the earlier days of our business we favored a more intuitive approach, we’ve found it much more effective to track the billable hours of our team to ensure our project pricing is as accurate as possible.

I think either approach can work, but then ensure that tracking hours does not just become a way to micromanage the work of the team.

## Cash Is King: Building Your Safety Net

Finally, you need at least six months of cash in the bank. This might seem like quite a lot, but it should be there for two key reasons:

1. **You stop worrying about short-term cash flow.** If an otherwise good client is late paying a large invoice, you’re not stressing that you won’t be able to make it to the end of the month. There’s nothing worse than not being able to pay your own team and still having to go into the office to work each day. Trust me, I’ve been there.
2. **You can make long-term investments.** For instance, paying for software upfront for a year often saves you 20%, and if you’ve got the cash, you can even pre-pay your rent and negotiate with your landlord a decent discount. Also, you may want to make sizeable investments that don’t have a clear or immediate pay-back period. For instance, we spent $20,000 purchasing our domain mad.co, and there is simply no way to know what the payback period of this investment is.

Pay really close attention to this next section, because it is one of the most useful parts of this entire book. Running any business is about ensuring that you manage your cash flow in a sensible manner.

**In fact, I would say the primary responsibility of a startup CEO is to ensure that there is always enough cash in the bank.**

If you run out of cash, the game is over.

So, your priority as a professional service firm leader is to ensure that you have plenty of cash flow to pay staff on time, cover your fixed operating costs, make sound investments, and sleep well at night!

(Don’t underestimate how important that last point is — being able to sleep well at night can make all the difference when you’re running your own business!)

Having a minimum of 3 months of expenses banked away ensures that your business is resilient, and if you can expand that to six months, that’s even better. Remember, growing your business can also cost money, so you need to factor that in.
Surprisingly, the main thing that affects your cash flow is not how many projects you have, or even how much you bill. It’s actually all about when you get paid.

## Revolutionizing Your Billing Strategy

Traditionally, many professional service firms take a deposit, and then have milestone payments attached to key deliverables.

In my opinion, this is a terrible idea, especially for work that is highly collaborative in nature.
The issue is that you are now relying on people who do not work for you (i.e., client team members or third-party vendors) to do their side of work in a timely manner so you can get paid on time. Because you cannot control these people directly, it means you have no direct control over when you get paid, which affects the most important aspect of running a successful business — cash flow.

While this may work out well some of the time, why take the risk?

Additionally, it also puts a lot of pressure on the professional service firm to get work approved so you can get paid, which adds double pressure to everyone involved in the project on your side.

What happens with this traditional milestone-based way of billing is that your cash flow becomes lumpy and unpredictable, and it is difficult to forecast how much cash your business will have at any given time, and if you grow and have more than ten clients, managing all these approvals linked to payment milestones becomes a full- time job almost in itself, and quite a boring one, too.

We struggled with this method for years, and eventually created a significantly easier way of doing client billing that is fair for everyone and puts the focus on where it should be: working well together as a team with the client and producing high-quality work.

**This may be the most important insight I have had.** 

For your next project, try this:

Take an initial payment, ideally ask for 50% upfront. If the client has issues with this, you can go down as low as 35%, but ideally, no lower. If there are further complaints, you can cite that booking your team for the period of time requires enough commitment from the client so that you can focus on their project.

By the way, never refer to the initial payment as a deposit, as this can give the idea that it can be refundable. Payments to a professional service firm should never be refundable.

For the rest of the payments, instead of linking them to deliverables, simply split up the remaining amount as monthly payments across the entire project timeline, plus one additional month.

So that means that if you estimate that the project will take 3 months, the payment schedule will be spread out across 4 months.

This means that even after the project is over, you still have one more bill to send the client, and this works well and puts them at ease in case there is a delay in the project.

The great thing about this method is that it enables you to easily forecast all your billings month-to-month, and it makes each client feel like a retainer client. It also adds positive pressure to everyone involved on the client team, as it is more obvious that they have you booked for a certain period of time, and they need to get things on their end moving fast — otherwise, they are paying for nothing!

Another advantage here is that once you have a client that is used to paying these monthly billings to you, it is much easier at the end of an initial engagement to then pitch a retainer agreement, as it will almost feel like a seamless continuation of the working relationship that you have already created.

That’s pretty cool, right?

To further simplify things, we also suggest that regardless of when your project starts, you align sending your invoices on the first of each month, with the exception of the initial payment. This makes it significantly easier for you or your finance person to manage the invoicing, as it only needs to be done once per month, and it will generally also suit the client better as well, so you can help them stay organized and ensure that your invoices are processed in a timely manner.

This sometimes means waiting a few weeks more before sending that second invoice across, but this small delay is worth the time you save by batching all your invoicing at the start of each month, excluding those initial payments that can be invoiced at any time.

What is really counterintuitive about this overall billing strategy is that it actually gives you less cash flow in the short term compared to the standard milestone payments (especially if projects move faster than expected!), but you exchange the velocity of cash flow for the predictability. In my experience, this is a great trade-off to make. Remember — sleep well at night!

This is important because running a professional service firm is tough, you have a lot to think about, and the key things you need to be working on should not be related to back-office administration. You’ll save yourself countless hours with this strategy, and the investments that you can make with this extra time are probably worth a lot of money if you stop and think about it.

Sometimes, you’ll need to deal with some pushback from clients that want a more traditional payment method. In our experience, this happens less than 5% of the time. However, when it does happen, you need to be able to use logic to justify and explain why you want to bill in this unconventional manner.

The key narrative in your explanation should be centered on wanting to simplify your billing structure so you can spend significantly more time working on real client projects (like their project!), instead of spending time chasing bills and arguing over the precise definition of “done” for a milestone payment.

If you are speaking to the management team, you can also explain that this billing method can give positive pressure to their own team (as long as they are aware of what our professional service firm is billing) to ensure that everyone is working with the correct shared objectives in mind.

## Forecasting for Success

Every so often, you’ll need to make financial decisions, such as whether to buy more equipment, take on a new project, hire someone new, and so on.

The only way to ensure that you make better decisions in this regard is to use the data at hand to build out a financial projection of how things are going and should go in the next few months. Personally, I like to project 4-6 months out and use that to guide my decisions.

The way to set this up on a spreadsheet is actually quite simple and straightforward. The example below should give you a clear idea of what you can create, and the important thing is that this should all be done on a cash basis because, unfortunately, you cannot pay staff members, contractors, and other bills with the unpaid invoices that your clients owe you!

With this spreadsheet, you can then add additional costs that you’re considering and see how it affects both your Net Cash Flow (how much money is left at the end of each month) and also your Cumulative Cash Flow (how much money in total will be in your bank account).

As long as you’re keeping Cash Flow Positive and the Cash in Bank is within a range that you are comfortable with – this should be at least three months of cash and ideally six months – you can most likely go ahead with the expenses that you are considering.

If you’re in business, you likely have ambitions to grow over time. Growth is good. It enables career opportunities for your team members, and it gives you new challenges, which staves off career boredom. After all, nobody wants to do precisely the same thing for thirty or forty years!

## Growing Sustainably

If you are doing things right, it should feel that you have the wind in your sales, that there is momentum. You should feel that you are “winning”.

All this being said, it is important that growth is sustainable. The type of businesses that we are discussing in this book are generally not funded by investors, with the exception of some initial startup costs, and so you need to grow and still ensure that you manage to keep the financial fundamentals in place.

In addition, because firms are very much based on the quality of the people they can hire, this can also put restraints on your growth.

Your ability to hire great people at scale will be a limiting factor, and it is a huge mistake to lower your hiring bar just to ensure that you have enough bodies in the room to keep up with your billing. This will work in the short term, but it will cause you to lose the very thing that made you special in the first place and helped you to grow.

This is precisely what happened to me in the first year of business, where I did not manage to handle the growth. It became unsustainable, and I ended up having to fire almost all of my team members.

So let me go through my definition of sustainable growth:

> It is the maximum rate of growth where you can keep the finances in a healthy place and where work quality does not go below your brand promise to your clients.

With regards to keeping your financials in place, this means that for every additional commitment that you make on a monthly basis, you need to ensure that you add enough money to your bank account to keep your capital ratio in place. For instance, if you decide that you want six months of cash in the bank, then this means that for every $1 in additional monthly expenses, you need to have an additional $6 put away. 

This puts a whole new perspective into things when you’re hiring a new team member or signing up for a new service.