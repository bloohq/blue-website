---
title: Effective Project Management
slug: effective-project-management
tags: ["agency-success-guide"]
description: Learn how to manage projects so that you get great results on time, and within budget.
image: /resources/madbrainstorming.jpg
date: 14/06/2024
showdate: false
sitemap:
  loc: /resources/effective-project-management
  lastmod: 2024/06/14
  changefreq: monthly
---
This is part of the [Agency Success Guide](/resources/agency-success-guide), a book written by the CEO of Blue for all agency and professional service firm owners, based on his 10-year experience as an agency founder. 

In this book, you'll learn everything you need to know on how to run a radically successful agency. From cash-flow to project management, and client relationship building to writing winning proposals — it's all here.  

---
As an aisde, one of main [reasons I started Blue](/resources/why-i-started-blue) was because I had a strong desire to get better at managing my projects. I tried using various other tools, but they just didn't work for me, and that's why I started [Blue](/). 

Anyway, let's start this chapter! 

As a professional service firm, project work will comprise the bulk of your business functions. Your ability to efficiently manage projects could therefore be a decisive factor in whether clients choose to work with you – that is, how well you can handle the initiation, planning, and control of all the processes required to deliver on the contract.

Project management implies a reliance on tangible and intangible deliverables with finite timespans, so although projects eventually end, the discipline of project management will require consistent tracking. This highlights the importance of clear and effective communication.

It is vital to have a technique for managing projects that is consistent across all projects. This means that you can jump into any project, and instantly see how things are going. It should be crystal clear if a project is managed well or badly. This doesn’t mean that there isn’t a difference between projects, but that the building blocks of project management are in place for every project.

While there are many different ways of managing work, I’ve found one method, via trial and error, that consistently provides the best results and is clear and easy to understand for team members and clients alike.

This method is called [Kanban](/platform/kanban-boards), which is a Japanese manufacturing system originally created by Toyota, where the supply of the various components and parts required to build an automobile is monitored via having an instruction card that is sent along the production line.

The idea behind Kanban is very simple: you need to create a clear process to enable you to complete whatever the work is at hand.

At a basic level, this might look something like:

- **Work that needs to be done.** Often called a “Backlog”, this is a list of all the work that needs to be done in order for the project to be complete.
- **In progress.** This is what is currently being worked on.
- **Under review.** This is what is being checked by someone else to
ensure that the work has been done to the correct quality.
- **Done.** This is a historical list of all the work that has been
completed.

**What this hints at is total transparency with regards to all the required work to complete a project. This means the breakdown of what needs to be done, who needs to do it, and by when it needs to be done.** You can then modify the basic process above to match it closely to your actual process.

If you can build a system like this that your clients view in their own time, it will massively reduce (or completely eliminate!) the requirements for reporting around many projects. This is because the client can login and see the status of the work. This also implies that all communication is made via one platform and is transparent to everyone else on the project. The issue with not doing this is that it starts to build information silos, and nobody knows where key information is or where the records of various decisions are.

Of course, your processes will most likely be somewhat more complex than the simplified abstraction above, but that is a good start. It’s best to think of processes as something that is constantly changing and responding to the reality on the ground, not a rigid, unchangeable, and unforgiving methodology that gets in the way of work.

Process is a positive thing, bureaucracy is process run amok. The way I view process is that you need to least amount of process required to get the results that you are looking for.

Finally, a few other things to note about project processes.

Have a built-in mechanism to capture questions and ideas. If you don’t, this will become informal and time-consuming. What we like to do is to have these as lists before the main process, so our typical project process looks something like this:

- Ideas
- Questions
- Backlog (long term)
- Backlog (short term)
- In Progress
- Internal Review
- Client Review & Action
- Completed

We add separate lists for ideas and questions so they are easy to capture, and we also split our backlog into long-term and short- term to easily see what’s coming up. This is especially useful if you have a significant amount of work in the project.

We also have two rounds of reviews, one is internal, and the second is for the client. That client list also contains all the items that the client needs to do, such as providing information, files, access to systems, and so on. This is great to have because often, clients won’t have their own system to manage what they need to get done on the project, and this provides an easy way for them to stay organized.

Having a review meeting with your team members to go through all your projects is a great strategy to stay aligned. You don’t need to cover every single detail or have in-depth discussions, it’s just about aligning everyone and ensuring that you’re all on the same page.

It really helps to avoid those *holy shit!* moments when someone realizes that they have let something slip in a really drastic manner.

Something that really helps if you’re leading these meetings is to prepare the agenda beforehand, and then write key points below each project in a bullet point format. For the next meeting, you can then clone the document and ensure that the things from the previous week were done while also starting discussions on new things.

The format is rather simple:

Project 1.
• The client didn’t like option 1 or 2, so we need to come up with something new. Jack is going to do this by Wednesday.
• We also need to start planning for the review meeting on Thursday. John will draft an agenda and send it to the client.

And so on.

The next key recommendation is meeting minutes. While tedious, taking accurate meeting minutes and ensuring that they are sent to everyone who attended the meeting, followed by a clear list of next steps, is crucial.
Although meetings often get a bad rep, there are good uses for them and professional service firm life revolves around meetings. You’ll meet your team to review work, you’ll meet with the client to discuss the work, you’ll meet prospects to sell work, and you’ll have plenty of other meetings as well.

If a meeting has no outcome, then it can be argued that it might actually have been pointless to have had that meeting in the first place. Having a clear set of meeting minutes is important; however, this is a bit tiresome sometimes to take, tidy up, and then send across to everyone.

One trick that we have developed is to use our project management system as a live system to take notes that turn into future things that need to be action, either as individual todos in a backlog or as checklist items within a todo. This means that as the meeting is happening, key actions and feedback are being recorded live into the PM system and then there is no need to then send the meeting minutes separately, which is a great time saver.

As a final point, it is also worth considering the (hopefully superior!) client experience that we want to create in each project, and then work backwards towards the project management practices required to enable this superior experience. 

Are there simple things that your project managers can do that will be greatly appreciated by a client? What’s the picture of perfect here? How can we move closer to that ideal?
