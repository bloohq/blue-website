---
title: How does Blue offer unlimited AI Access?
slug: unlimited-ai-faq
tags: ["frequently-asked-questions"]
description: How does Blue offer unlimited file storage for project management? 
image: /patterns/waves1.png
date: 01/08/2024
showdate: false
sitemap:
  loc: /resources/unlimited-ai-faq
  lastmod: 2024/08/01
  changefreq: monthly
---

At Blue, we're one of the few [project management platforms](/solutions/project-management) that does not bill separately for AI usage. This is in line with our mission:

> To organize the world's work by building the best project management platform on the planet—simple, powerful, flexible, and affordable for all.

We want enteprise-level functionality to be available to everyone, not just the large corporations with deep pockets. This is why we have [one plan that includes all features without any additional paid add-ons or upsells.](/pricing). [When we reviewed Trello](/alternatives/trello-alternative), we noted how it was annoying to work on a platform that is now designed essentially to upsell you to working with Jira. 

A lot of companies are currently charging significant amounts for AI features. But, we know that the prices of inference — the "processing" of language that happens in the background when you send a request via a platform such as ChatGPT — have been dropping exponentialy. 

 We believe AI will become a standard feature, not a separate category, much like the internet itself.

However, we understand that customers can be skeptical of "unlimited" claims, and this is why we are writing this article — so show the economics behind giving all of our customers complimentary AI usage. We did a [similar write up for our policy of unlimited file storage.](/resources/unlimited-file-storage-faq)

Blue provides a range of AI tools, including free AI chat on our website trained on our documents, various public AI tools such as project risk analysis and email creators, AI categorization for records inside projects, and AI summaries for comments within records—with many more features planned for the future.

And we will have a lot more in the future!

The economics of our unlimited AI access are based on a simple principle: **the reduction in AI processing costs is happening faster than our usage is increasing.**
 
So, we can expect our AI costs to decrease even as usage goes up. 

This is due to the rapid advancements in AI technology and the commoditization of Large Language Models (LLMs). Blue leverages this by running our own open-source LLMs for semantic search indexing while using OpenAI's GPT-4-turbo for other AI features.

To illustrate the dramatic price drops in AI processing, consider the difference between GPT-4 and GPT-4-turbo. GPT-4 costs $30.00 per 1 million tokens, while GPT-4-turbo costs just $0.15 per 1 million tokens—a 99.5% reduction in price over just 15 months. This translates to a monthly price reduction of approximately 32.7%, meaning each month, the price reduces to about 67.3% of the previous month's price.

![](/resources/openaihistoricalprices.png)


Extrapolating this trend to 2030, we can predict that AI processing costs will continue to plummet. This rapid decrease in costs allows us to confidently offer unlimited AI access to our customers without compromising our business model or the quality of our service. As AI technology becomes more efficient and widespread, we anticipate being able to offer even more advanced AI features to our users at no additional cost.
