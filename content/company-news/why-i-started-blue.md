---
title: Why I started Blue
slug: why-i-started-blue
tags: ["news", "insights"]
description: The CEO of Blue provides an in-depth first-hand account of why he started the company, the core principles of simplicity, affordability and flexibility, and his vision for the future of Blue
image: /patterns/bluewhitecircle.png
date: 1/04/2018
showdate: false
sitemap:
  loc: /resources/why-i-started-blue
  lastmod: 2018/04/01
  changefreq: monthly
---

**Editor's Note:** The following is a first-hand account from Emanuele Faja, CEO of Blue, on the origins and mission behind Blue, the modern project management platform.

---

The idea for Blue was born out of my own frustrating experiences trying to keep my design consultancy Mäd organized and operating efficiently. As our client base expanded, I found myself juggling multiple complex projects with a team spread across different countries. The traditional project management tools we relied on were either hopelessly clunky and overengineered or far too costly for a bootstrapped startup like us.

Something had to give. Why was organizing work so painfully hard? The software was supposed to make us more productive, not waste precious time wrestling with arduous setups, bloated feature sets, and pricing that didn't make sense for smaller teams.

I became determined to build a nimble, common-sense alternative tailored for the modern workforce - a simple but powerful command center to truly *"organize the world's work."*  That kernel of an idea ultimately sparked the creation of Blue in 2018.

 ## Embracing simplicity as a core principle

 From the very beginning, I resolved that Blue would be different - both in its philosophy and approach to product development. We would be relentlessly focused on delighting users through elegant simplicity rather than overwhelming them with complexity.

As a lean team, we knew we couldn't afford bloated processes or unnecessarily complicated functionality. Every new feature would have to clear an extremely high bar of solving a real problem faced by teams working in fast-paced environments. If it didn't make work tangibly easier, it wouldn't make the cut.

This commitment to simplicity became enshrined in our company values and cultural DNA. We strived to create a platform that would be "as simple as it should be, but no simpler," favoring clarity over unnecessary frills.

At the same time, I recognized the equal importance of long-term thinking, customer obsession, and investing in deep work to build something remarkable. These principles formed the bedrock for Blue as we started putting the first versions into the hands of Mäd's clients and getting brutally honest feedback that shaped our roadmap.

## On value propositions.

As Blue took form, three main value pillars emerged at its core:

1. **Simplicity:** Our pared-back approach translates into faster onboarding, less training overhead, and higher adoption rates. By having a small, focused team, we avoid the "feature bloat" that plagues so many other PM tools.
2. **Affordability:** By operating nimbly and eschewing the premium pricing of most competitors, we're able to undercut their costs by 80% while still providing robust capabilities. This allows cost-conscious teams to reinvest in other growth priorities.
3. **Flexibility:**  We realized early on that no two teams work exactly alike. Rather than dictating standardized workflows, Blue flexes to adapt to the needs and processes unique to each customer organization, no matter their industry.

Woven through each of these pillars is our broader "customer obsession" - a deep commitment to understanding and resolving the real pain points teams face in managing their work today.

This trifecta value proposition has resonated powerfully. What started as an internal tool for my consultancy has rapidly grown into a trusted platform used by over 7,000 organizations across 120 countries to drive over 350,000 projects spanning countless functions like sales, marketing, operations, and more.

## The future

Building a successful technology company from scratch is an immense challenge. There were countless hurdles and near-death experiences that made the early days feel like a fight for survival at times. For instannce, in September 2022 we went down for almost 24 hours after growing from 1,000 companies to over 4,000 that year, and with multiple critical components breaking simultaniously. This taught me the importance of proactive monitoring and contigency planning.

But our clarity of purpose - that fundamental mission of creating the best project management platform for the modern workforce - kept us going and courses-correcting when we veered off track.

As I look at where we're headed, I see Blue evolving into the preeminent command center for managing any type of project or process, no matter how simple or complex. We'll double down on making the product even more intuitive and delightful to use while layering in enhanced intelligence, deeper integrations, and increased flexibility.

At the same time, I'll fiercely protect the core simplicity that has been our calling card and differentiator since those early days sketching ideas on office whiteboards. It's this simplicity that has made it possible for teams to adopt Blue effortlessly and *"just get work done"* without wrestling complexity.

The opportunity ahead is tremendously exciting as more teams seek nimble, cost-effective ways to collaborate and drive results in our increasingly project-based economy. I'm proud that Blue is well-positioned to capitalize on this massive global transition in how work gets organized and accomplished.

Most of all, I'm grateful to the customers and tireless team who took a chance on an ambitious vision to make organizing work simpler. Their partnership and passion continue fueling us in our mission of building the best productivity platform for teams everywhere.