---
title: 2022 Annual Letter
slug: 2022-annual-letter
tags: ["news"]
description: We look back on 2023 and set out our plans for 2024, reflecting on our progress, highlighting milestones, and our strategic goals for the upcoming year. 
image: /patterns/letter.png
date: 31/12/2022
showdate: true
sitemap:
  loc: /resources/2022-annual-letter
  lastmod: 2022/12/31
  changefreq: monthly
---

Hey there,

This is Manny, the CEO of Blue.

As the end of 2022 draws near, it's a time to reflect on all that we've accomplished at Blue over the past year. And what a year it's been! From rebranding from Bloo to Blue, to experiencing tremendous growth and dealing with the challenges that come with it, it's been a rollercoaster ride. But as the saying goes, the best problems to have are the ones that come from success.

And speaking of success, let's take a look at some of the impressive statistics from 2021 to 2022. In just one year, we've seen a significant increase in registered companies, going from 4,585 to 6,245. This is a testament to the value and effectiveness of our project management tool. And that value has translated into revenue growth, with a 3x increase year-on-year. Our Monthly Recurring Revenue (MRR) has also seen a staggering 420% growth.

But it's not just about the numbers - we're also committed to providing fantastic customer service. In 2022, we replied to over 7,000 support requests, with an average response time of less than 4 hours and a handling time of 67 seconds. That dedication to our customers has paid off, with 90% of support requests being rated as "Great" and 53% of all requests being resolved with just one reply. In addition to answering support questions, we also had over 300 one-to-one calls with customers to learn about their use cases and help them get the most out of Blue.

In addition to all of this, we've also released dozens of webinars to showcase the many ways in which our tool can be used. And all of this hard work has paid off - in 2022, we were awarded "Best Project Management Tool" by Cybernews and "Best Project Management Tool Provider" by Corporate Vision.

Product

This year, we faced numerous challenges as we worked to scale up our product. With an increase in traffic of an order of magnitude, certain things that had been working smoothly for years suddenly stopped functioning as intended. I’ve read that this is a good sign, because it meant that we did not over-engineer or over-design our work initially, but it can be frustrating to see performance degradation over time.

Despite these challenges, we were still able to ship a number of improvements to the platform while also doing a significant amount of engineering work to keep things running smoothly. We deployed a new version of Blue 77 times in 2022, which is slightly more than once per week.

Some of the notable features that we released in the past year include:

* **File Folders and File Sharing**: This feature allows users to easily organize their files within a project and share their files to people outside of Blue.
* **Project Wiki and Blue Docs**: These tools provide a real-time collaborative platform for teams to document and share information about their projects.
* **Add time to start date/due date**: This feature allows users to specify a specific time for their to-dos to start or be due, rather than just a date.
* **Bulk edits todo, Right-click to edit, and Edit comments**: These features make it easier for users to manage and edit their to-dos and comments, saving time and streamlining their workflow.
* **More automation features including checklists and cross-project automations**: These features allow users to automate certain tasks and processes, saving time and increasing efficiency.
* **Ability to hide emails**: This feature allows users to hide the email addresses between users across the platform.
* **Todo Dependencies**: This feature allows users to specify dependencies between to-dos, ensuring that tasks are completed in the proper order.
* **@@ mention todos**: This feature allows users to easily assign to-dos to specific team members by mentioning them in the task description.
* **Forms redirect**: This feature allows users to redirect users to a specific URL after they submit a form.
* **Due Date Custom Field**: The ability to create a custom field specifically for date, so you can have multiple dates within one todo.
* **Turn project ON/OFF**: This feature allows users to easily turn a project on or off, helping them to focus on their most important tasks.
* **Copy Forms and Tags on Forms**: These features allow users to easily copy and reuse forms and tags, saving time and streamlining their workflow.
* **Adding todos from the bottom**: This feature allows users to add to-dos to the bottom of their list, rather than having to scroll to the top to add a new task.
* **Todo Reminders**: This feature allows users to set reminder for their to-dos, ensuring that they don't forget important tasks.
* **Change Company URL and colors**: These features allow users to customize their company URL and colors to match their brand.
* **Preview audio files**: This feature allows users to preview audio files before they download or share them.
* **Embed forms**: This feature allows users to easily embed forms on their websites or other platforms.
* **Youtube videos embed**: This feature allows users to easily embed YouTube videos in their projects.

In addition to the many features that we released this year, we also implemented support for custom domains, allowing users to use their own domain name with their Blue account. This adds an extra layer of professionalism and branding to their projects.

We also completed a massive database upgrade, switching to a new technology that significantly increases the speed and performance of Blue. This upgrade will make it even easier for users to manage their projects and collaborate with their teams, without any slowdown or frustration.

Overall, it's been a busy and successful year for Blue and we have some exciting developments in store for the upcoming year.

Our primary focus is to enhance the reliability and performance of the platform. To achieve this, we are transferring our long-running processes, such as bulk edits, large imports/exports, and copying large projects, to dedicated background servers. This will help maintain the speed and dependability of Blue, even during peak periods.

We are thrilled to introduce what we believe is the finest web hooks implementation in the industry. Our web hooks are highly granular and flexible, enabling users to automate and customize their workflow in creative ways. Did anyone mention Zapier?

We are also developing a new custom field for formulas. This feature will allow users to calculate custom values based on other custom fields, providing more flexibility and customization.

In addition, we are creating a reporting engine to generate bar charts, pie charts, and stats cards from data across multiple projects. This will make it easier to track and analyze essential metrics and trends. No need to export data and manually build your own reports in PowerBI.

We are also working on granular custom permissions. This will allow users to adjust and refine the permissions and access levels of team members within a project, providing more control and security. This feature will be especially useful for larger organizations or teams with complex hierarchies and roles.

Furthermore, we are developing the ability to cross-reference data. This will permit users to reference data from other projects or sources within their current project, giving more context and insight into their work. This feature will be especially helpful for teams working on interdependent projects or for those who need to track and analyze data from multiple sources.

Automations will also receive a substantial amount of upgrades, including actions to send custom emails and SMS using the data that is already in your projects. We will also introduce conditional triggers, where you can specify that multiple conditions must be in place before an automation is triggered. This will bring an entire new level of accuracy to automations.

In addition to the major features and improvements we have planned for the coming year, we're also committed to working on the small things that can make a big difference in the long run. To that end, we've hired a dedicated engineer who will focus exclusively on small usability improvements.

We know that it's the little things that can often make the biggest impact in terms of user experience. Whether it's streamlining a workflow, simplifying a process, or just making a tool feel more intuitive and natural to use, these small improvements can add up over time and make a significant difference in how people use our product.

That's why we're excited to have a dedicated engineer who will be able to focus on these types of improvements. By taking the time to identify and address these small pain points, we hope to create a smoother and more enjoyable experience for our users.

Overall, we're committed to continuous improvement at Blue, and we believe that this focus on both big and small improvements will help us to create the best possible product for our users.

As always, thank you for using our platform, and I am excited to see how everyone uses Blue in 2023 to hit their organizational goals and get things done.

Feel free to reach out to me if you have questions, ideas, or feedback.

Stay productive,

Manny.
