---
title: 2023 Annual Letter
slug: 2023-annual-letter
tags: ["news"]
description: We look back on 2022 and set out our plans for 2023, reflecting on our progress, highlighting milestones, and our strategic goals for the upcoming year. 
image: /patterns/letter.png
date: 31/12/2023
showdate: true
sitemap:
  loc: /resources/2023-annual-letter
  lastmod: 2023/12/31
  changefreq: monthly
---

Hey there,

This is Manny, the CEO of Blue. 

Every year, I send an email on my birthday (today!) to reflect on Blue’s achievements. 

And what a year it has been! Our MRR (Monthly Recurring Revenue) grew 4x from last year (and 19x from 2021), and we shipped numerous features to help customers thrive .

Before I get into the details, I’d like to reiterate our core purpose. 

Our mission is to organise the world’s work. This means building a powerful yet easy-to-use platform to handle any process or project and enables [great teamwork](/resources/great-teamwork). A product that we would proudly recommend to our friends, family, and colleagues. 

To be candid, this year marks a significant milestone for me personally — it's the first time I can wholeheartedly recommend Blue without any reservations. 

My focus remains on sustainable growth. We don't have investors —this means our only customers are our customers. I made this decision consciously to ensure that our focus is always on building the best possible platform. 


## A few announcements 

Major feature release. Next week, you will see half a dozen major new features. You'll see dashboards, custom user roles, company-wide maps, formula custom fields, email automations, automations based on custom field changes, and also an incredibly powerful cross-project referencing system so you can pull data from one project into another. I can’t wait to see how customers leverage and innovate using all these new features. 

Blue is getting a new look. Last year, we rebranded from Bloo to Blue, and this year, we’re announcing a brand new visual identity that matches our progress over the last six years. You'll see this across all of our applications in the next few days. 

![](/logo.svg)

## 2023 Product Overview

I started 2023 with two key focus areas:

1. Improving platform stability
2. Turning Blue into a full work platform

Both goals have been met. 

Despite significant growth in volume — we had 600M+ API calls in 2023! — this is the best year on record for stability, with 99.91% uptime. In other words, Blue was unavailable for only ~8 hours in 2023. We’ve become obsessed with our monitoring metrics, and this has paid off. We measure every single interaction in Blue and constantly keep an eye on things:

With regards to turning Blue into a full platform, we shipped 358,149 lines of code to customers this year. In terms of features, the major ones that we released are:

* **Custom Permissions**: Create granular permissions for custom fields, invites, lists, assignees, and features.
* **Dashboards**: Turn data into actionable insights with cross-project queries and visualize using stat cards, pie charts, and bar charts.
* **Timeline / Gantt Charts**: Visualize project dependencies and timelines, improving project planning and tracking.
* **Cross Project References**: Share data between projects, improving data accessibility and project integration.
* **Conditional Automations**: Create advanced, condition-based automations to cater to complex scenarios.
* **Cross-Project Automations**: Move and copy records between projects using automations.
* **Time Length Custom Field**: Track time between events in records.
* **Chat**: Enable multi-topic discussions within each project, facilitating better communication.
* **Formula Custom Field**: Run calculations on outputs of other fields.
* **iPad App**: A dedicated app for iPad users, leveraging larger screen space for enhanced usability.
* **Record Colors**: Change the color of a record on the board.

...And hundreds of minor improvement!

I often get asked — how do we decide what to build next?

While I have a high-level product direction and strategy, the core methodology is simple: we speak to customers!

This may sound obvious, but you'll be surprised how often products are built without customer input.

In 2023, we had 4,168 email exchanges with customers, with an average response time of two hours and twenty-five minutes. We also had hundreds of one-to-one calls to learn about customer use cases, gather product ideas and receive feedback.

## 2024 Focus

To be honest...there is no new 2024 focus! We keep our sights locked on building a platform that is easy to use, yet powerful. 

We’re doubling the size of our engineering team in 2024 and doing major behind-the-scenes upgrades to enable the next 10,000 organisations to sign up and use Blue. 

For the features that you use most, later this year you will start to see sub-100ms response time — which is twice as fast as a blink of an eye. 

As always, I encourage you to reply to this email with your feedback and ideas, or join our community forums. 

Thank you being a customer of Blue, we're incredibly grateful.

Happy New Year — and stay productive! 

Manny. 

