---
title: Behind the scenes on changing price
slug: pricing-change
tags: ["news", "insights"]
description: This is our thinking behind changing the price from per-company to per-user — it aligns our business model and our customer's growth in the same direction.
image: /patterns/dollar.png
date: 1/04/2024
showdate: true
sitemap:
  loc: /resources/pricing-change
  lastmod: 2024/04/01
  changefreq: monthly
---

In Q1 2024, Blue changed its pricing from a flat-fee ($50 and $200/month) per company for unlimited users to $7/user/month. 

This wasn’t an easy decision — pricing changes never are. To start, we grandfathered all of our existing customers to their previous price if it’s advantageous, and they have the option to switch to the new pricing if that is cheaper for them. 

There are two main reasons why we changed the price:

1. To unlock sustainable growth for Blue
2. To enable smaller companies and solopreneurs to use Blue.

Let’s cover each reason in depth.

## Unlocking sustainable growth

When Blue first launched, we adopted a per-company pricing model to make our platform accessible to as many organizations as possible. However, after nearly six years of operation, we realized that this approach posed challenges to our long-term sustainability.

The issue is that every subscription business has to deal with churn. 

Churn rate refers to the percentage of customers or subscribers who stop using a service or cancel their subscription within a given period. In the context of SaaS (Software as a Service) businesses like Blue, churn is a critical metric that reflects the company’s ability to retain customers over time.

To illustrate the impact of churn, let’s consider a simple example. 

Imagine a company that starts the year with 100 customers. If the company has a monthly churn rate of 5%, it means that, on average, 5 customers will cancel their subscription each month. While a 5% churn rate might seem relatively low, its cumulative effect can be substantial.

After one year, the company would have lost approximately 46 customers, even if it managed to acquire new customers at the same rate. To maintain its initial customer base of 100, the company would need to acquire 46 new customers just to break even. This demonstrates how even seemingly small churn rates can significantly impact a company’s growth and sustainability over time.

In the world of software, customer churn is an inevitable fact of life. Even the best product in the world will always have customers that stop using it. Some business may cease to operate, others might pivot to a different business model that doesn't require the software, and others might simply find a better solution elsewhere. This reality means that even if we build the perfect product, we still need to focus on attracting new customers to replace those that leave. 

In Blue’s case, our churn rate was lower than 5%, but that still meant 30-40% growth per year just to stay at the same MRR (Monthly Recurring Revenue). our per-company pricing model meant that the only way to counter the effects of churn was to continuously acquire new organizations as customers. However, this approach becomes increasingly challenging and expensive as the market becomes more competitive and saturated. The only options would be to raise the fixed monthly price to pay for more advertising and growth expenditures, or  transitioning to a per-user pricing model.

By transitioning to a per-user pricing model, we can now benefit from the organic growth of our existing customers. As companies expand and add more team members, they will naturally require additional paid seats, contributing to our revenue growth. This approach allows us to maintain a healthy growth rate without solely relying on new customer acquisition.

Under the per-user pricing model, Blue's revenue can grow in two ways:

1. **Acquiring new customers:** As before, Blue can still grow by acquiring new companies as customers. However, the per-user pricing makes it more affordable for smaller companies to start using Blue, expanding our potential customer base.
2. **Existing customers adding more paid seats:** As our customers' businesses grow and they add more team members, they will need to purchase additional paid seats in Blue. This organic growth within our existing customer base contributes to our revenue growth without the need to acquire new customers.

The beauty of this model is that even if some customers churn, the growth in paid seats from other customers can offset those losses. In fact, some companies may even have a negative net churn rate, meaning they add more revenue through additional paid seats than they lose through downgrades or cancellations.

For example, let's say a company starts with 10 paid seats in Blue. Over the course of a year, they add 5 more paid seats as their team grows. Even if another customer with 3 paid seats churns during that same period, Blue still experiences a net positive growth in revenue from the existing customer base.

This ability to grow revenue within our existing customer base is crucial for Blue's sustainable growth. It reduces our dependence on constantly acquiring new customers and allows us to focus on providing value to our current users, helping them scale their businesses and teams.

Another benefit is that the per-user pricing model aligns Blue's success with that of our customers. As our customers grow and succeed, they will naturally need more paid seats, which in turn contributes to our own growth and success. This creates a symbiotic relationship where we are invested in our customers' long-term growth and success, rather than just focusing on acquiring new customers.

By aligning our success with that of our customers and reducing our dependence on new customer acquisition, we can focus on providing value to our users and helping them scale their businesses and teams.

## Making Blue accessible for smaller organizations

In addition to unlocking sustainable growth for Blue, the transition to a per-user pricing model also helps us better serve smaller organizations and solopreneurs.
Many of our customers switch to Blue after becoming frustrated with the limitations and restrictions of freemium models offered by other project management tools. These freemium models often lack key features or have limited collaboration capabilities, making it difficult for teams to effectively manage their projects.

However, our previous price points of $50/month or $200/month, while providing a comprehensive set of features, were often too expensive for smaller teams who only needed a few seats. This was especially true for solopreneurs who might only need a single seat to manage their projects effectively.

By introducing the $7/user/month pricing, we have made Blue accessible to a wider range of customers, including smaller organizations and solopreneurs. This new pricing ensures that teams of all sizes can benefit from Blue's powerful features and collaboration tools without breaking the bank.

We thought carefully about the per-user pricing — we have priced our per-user license fee to be one of the most competitive in the market, considering our extensive feature set. This means that smaller teams and solopreneurs can access a comprehensive project management solution at a price point that is often 2x or 3x lower than comparable solutions, without sacrificing functionality or quality.

For example, a small team of 5 people can now use Blue for just $35/month, gaining access to all of the features that larger enterprises enjoy. This includes unlimited projects, custom fields, automations, and integrations with over 5,000 other tools. Solopreneurs can also take advantage of Blue's comprehensive feature set for just $7/month, allowing them to efficiently manage their projects and collaborate with clients or contractors.

By making Blue more accessible to smaller organizations, we are empowering them to compete effectively in their respective markets. With access to the same powerful tools used by larger enterprises, these smaller teams can streamline their workflows, boost productivity, and deliver high-quality results to their clients.

And as these smaller organizations grow and expand, they can easily add more paid seats to their Blue account, ensuring that the platform scales with their business. This flexibility allows smaller teams to adopt Blue early on and continue using it as they grow, without the need to switch to a more expensive or complex project management solution.

In conclusion, the transition to a per-user pricing model not only supports Blue's sustainable growth but also makes our platform more accessible to smaller organizations and solopreneurs. By providing a comprehensive set of features at an affordable price point, we are empowering teams of all sizes to effectively manage their projects, collaborate with stakeholders, and achieve their business goals.

This aligns perfectly with our mission to organize the world's work by building the best project management platform on the planet—one that is simple, powerful, flexible, and **affordable for all.**

We believe that every organization, regardless of size or industry, deserves access to the tools and resources they need to succeed.

By making Blue more accessible to smaller teams and solopreneurs, we are taking a significant step towards realizing this mission. We are breaking down barriers and democratizing access to high-quality project management solutions, ensuring that everyone has the opportunity to optimize their workflows, boost productivity, and deliver exceptional results.