---
title: 2021 Annual Letter
slug: 2021-annual-letter
tags: ["news"]
description: We look back on 2021 and set out our plans for 2022, reflecting on our progress, highlighting milestones, and our strategic goals for the upcoming year. 
image: /patterns/letter.png
date: 31/12/2021
showdate: true
sitemap:
  loc: /resources/2021-annual-letter
  lastmod: 2021/12/31
  changefreq: monthly
---

Hey everyone,

Hope you’re doing well, this is Manny the CEO of Blue.

It is our tradition at Blue to send out an overly lengthy end-of-the-year review on December 31st. It also happens to be my birthday! :)

See below for some interesting stats on 2021 and what we accomplished, and look forward to 2022 as well.

Interesting 2021 Stats & Year on Year Growth

* Registered Companies: 503 → 4,585
* Countries where Blue is used: 63 → 122
* Available Languages: 3 → 15
* Projects created: 1,357 → 16,874
* Todos created: 29,676 → 329,543
* Files stored in Blue: 10,297 → 145,145
* Storage in Blue: 265GB → 3,502GB
* Feature Requests submitted: 400+
* Revenue Growth:  5.6x (and hit breakeven — phew!)
* We didn’t grow our team, there’s still only 5 of us.
* As a comparison, other companies have much larger teams:
	+ Asana → 900 employees
	+ ClickUp → 800 employees
	+ Monday → 800 employees
	+ Wrike → 950 employees
	+ Atlassian → 5,700 employees
* Customer Support
	+ Support emails: ~6,000
	+ Our happiness score is 89/100, so 89% of these support requests were rated as “Great” by our customers!
	+ 51% of support requests were resolved with just one reply.
	+ Our average response time is 4.5 hours, which is significantly improved from 2020 where it was at around 11.5 hours.
	+ We had over 300 one-to-one calls with customers to learn about their use cases, answer support questions, and help them get the most out of Blue.
* We deployed a new version of Blue 41 times, which is almost one new version each week!
* We also made 1,452 code commits and we added 112,755 lines of code to Blue.
* Blue was down for a total of 5.5 hours during 2021, which gives us an uptime of 99.93%.
* The number of open bugs reached an all-time high of 112. While this may sound bad, it is actually a function of having so many people using the platform…there’s more eyeballs catching issues for us to fix!

## Initiatives that we launched
* Partner Programme, that gives 50% revenue share.
* Blue4Good initiative, that is giving $2.5m in licenses to organizations that need them most.
* Blue4Students, which provides students worldwide with a completely free small version of Blue.
* Community Forum
* Community Translation
* Free onboarding calls for all customers
* YouTube Channel with how-to videos

## Key features that we shipped

* You can check our [Release Notes](https://blue.cc/changelog) to see everything that we released each month since we started.
* We launched the following new features:
  * Map View
  * Database View
  * Forms
  * Project Templates
  * Duplicate Projects
  * API
  * Custom Fields for Files
  * Project Folders
  * Custom Interface Cooler
  * Calendar Sync
  * Custom Field filters
  * Repeating Todos

## 31 Things We Want To Achieve.

As I am 31 today, here’s 31 things that we want to focus on building next to enable [great teamwork](/resources/great-teamwork). This is going to be a lot of work, and it is unlikely that we will complete everything within 2022. That said, I hope this helps you understand our vision for the platform.

* File Folders
* Make Blue 300% faster: I’ve been testing an internal version with some core changes that makes Blue ridiculously snappy. When things happen instantly, it really changes the entire experience.
* Single Sign On — Choose how you want to login, email, password, Phone Number with SMS, Facebook/Google etc.
* International Infrastructure for scaling
* Gantt Charts
* Custom User Permissions
* Public Boards
* Zapier Integration
* File Sharing
* File Trash
* Advanced Custom Fields that can be set across the entire organization, or even for a specific todo only
* Keyboard Shortcuts
* Ability to have an individual todo in more than one project at the same time
* Ability to view board with different lists (i.e. see Tags as lists!)
* Automations between projects
* Ability to lock todos from being edited
* Blue Docs with real-time editing / Project Wiki
* Time Machine – the ability to go back to any specific point of a project in the past (and then deploy a copy to restore it)
* Add a color to a todo
* Custom Domains
* Bulk Edit Todos
* Emoji Picker for todo lists
* Ability to hide emails between users either company-wide or on a per-project basis
* An overall of project settings
* Custom reminder on a per-todo basis
* People V2 — the ability to see what’s on somebody’s plate across the entire org
* Reports — ask a question about the data you have. What's outstanding, what's coming up, etc
* Time Tracking
* Voice Messages in comments
* Todo Dependencies — this means that a todo can only be marked as complete if the todos it is linked with have been marked as complete as well
* Weekly calendar view + Due date times


## Things that won't change.

Ease of use. Generally speaking, things get more complex and harder to use as they evolve over time. We aim to do the opposite and actually make things easier to use. We will always focus on a great user experience. We will always make  intelligent trade offs between power and flexibility.


A human approach. You’ll always be able to get in touch with us when you need to do so, you’ll never get a noreply@ email from Blue.


Going slow to go fast. We will always ship when it’s ready, never before. Our average feature spends two months in Beta for testing. This allows customers to provide feedback be we release the feature to everyone. This isn’t going to change. It helps us keep the technical debts down to a minimum. This  means we spend our limited time on innovation instead of untangling things.


Focus on small organizations. It’s strange that small businesses use software developed by companies with thousands of employees. . Do these companies understand the needs of small teams and business owners? We will stay small and only focus our efforts on small organizations. If we make a product that tries to work for everyone — it actually ends up not working for anyone! 
 

Finally, I’ll sign off with a simple goal.

We will have 1M organizations using Blue in the next few years. So If you’re getting this email, you’re one of the early adopters — thank you! 

Happy New Year! 

All the best,

Manny 


