---
title: Rename Dashboard
description: How to rename a dashboard using the Blue API.
---

To rename a dashboard, you can use the following mutation:

```graphql
mutation EditDashboard {
  editDashboard(
    input: { 
      id: "dashboard-id", 
      title: "New Dashboard Name" 
      }
  ) {
    id
  }
}
```


