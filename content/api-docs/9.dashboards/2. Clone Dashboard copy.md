---
title: Clone Dashboard
description: How to clone a dashboard using the Blue API.
---

To clone a dashboard, you can use the following mutation:

```graphql
mutation CopyDashboard {
  copyDashboard(
    input: {
      title: "New Dashboard Name"
      dashboardId: "dashboard-id"
    }
  ) {
    id
  }
}
```


