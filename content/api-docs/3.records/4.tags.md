---
title: Tags
description: You can use tags to categorize records using the Blue API.
---


## Get list of tags

To get a list of tags, you can use the following query:

```graphql
query ListOfTagsWithinProjects {
  tagList(filter: { projectIds: ["ENTER PROJECT ID"], excludeArchivedProjects: false }) {
    items {
      id
      uid
      title
      color
      project {
        id
        name
      }
      createdAt
      updatedAt
    }
    pageInfo {
      totalPages
      totalItems
      page
      perPage
      hasNextPage
      hasPreviousPage
    }
  }
}
```

Here's a table explaining the fields returned in the `ListOfTagsWithinProjects` query:

| Field | Description |
|-------|-------------|
| id | The unique identifier for the tag |
| uid | A user-friendly unique identifier for the tag |
| title | The name or title of the tag |
| color | The color associated with the tag |
| project.id | The unique identifier of the project the tag belongs to |
| project.name | The name of the project the tag belongs to |
| createdAt | The timestamp when the tag was created |
| updatedAt | The timestamp when the tag was last updated |

The `pageInfo` object provides pagination details:

| Field | Description |
|-------|-------------|
| totalPages | The total number of pages of results |
| totalItems | The total number of items across all pages |
| page | The current page number |
| perPage | The number of items per page |
| hasNextPage | Boolean indicating if there's a next page of results |
| hasPreviousPage | Boolean indicating if there's a previous page of results |




## Tag a record

To tag a record with an existing tag, you can use:

```graphql
mutation UpdateExistingRecordAddTag {
  setTodoTags(
    input: {
      todoId: "RECORDID"
      tagIds: ["TAGID1", "TAGID2"]
    }
  )
}
```



