---
title: Right Click on Records
slug: right-click-records
tags: ["product-updates"]
description: Now you can right click on records to easily edit them at the speed of thought.
image: /resources/square-stack-background.png
date: 15/04/2022
showdate: true
sitemap:
  loc: /resources/right-click-records
  lastmod: 2024/04/15
  changefreq: monthly
---

Today's release brings a small but thoughtful improvement to our [project management platform](/). 

You can now right-click on any record to open a context menu, allowing you to make quick edits without needing to fully open the record. This feature is perfect for making swift changes such as updating tags, assigning tasks, or modifying other record details in a matter of seconds.

![](/resources/right-click-record.png)

## List of Options 

Here are the options available when you right-click on a record:

- **Open in New Tab:** Opens the record in a new browser tab.
- **Rename:** Allows you to change the name of the record directly from the context menu.
- **Mark as Complete:** Quickly mark the record as completed.
- **Move:** Move the record to another list of project
- **Copy Task:** Create a duplicate of the current record.
- **Copy URL:** Copies the URL of the record to your clipboard for easy sharing.
- **Move to Top:** Move the record to the top of the list.
- **Due Date:** Set or change the due date for the record.
- **Assign:** Assign the record to a team member.
- **Tag:** Add or change tags associated with the record.
- **Set Color:** Change the color label of the record for better visual organization.
- **Delete:** Permanently remove the record.


When you want to edit a record, you can now right click to have a menu instead of having to open the record to make edits. This makes simple edits, such as changing a tag or assigning someone, much faster.

## Multi Select

But that's not all! 

This new functionality is fully compatible with our multi-select feature. Simply CMD+Click (on Mac) or CTRL+Click (on Windows) to select multiple records simultaneously. Then, right-click to open the context menu and perform bulk actions. This enhancement significantly speeds up processes like assigning multiple tasks to a team member or tagging several records at once.

We're always looking to improve Blue, both with large releases and sometimes small feautures such as this one! 

Until next time — stay productive! 