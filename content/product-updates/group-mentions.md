---
title: Group @mentions
slug: group-mentions
tags: ["product-updates"]
description: The @mention features get a significant upgrade — now you can @mention user groups for bulk notifications, not only individual users.
image: /resources/mentionsbackground.png
date: 1/04/2024
showdate: true
sitemap:
  loc: /resources/group-mentions
  lastmod: 2024/04/01
  changefreq: monthly
---

 We're excited to announce a new feature on Blue: Group @Mentions. This update makes it easier and faster to notify multiple team members at once. Instead of tagging each person individually, you can now use group tags like `@all`, `@team`, `@clients`.
 
 What's awesome, is that if you create custom user roles, those will also work seamlessly with group mentions, so you can try things like `@helpdesk` or `@interns` to mention everyone in a custom user group instantly.

 See it in action below.

 <video autoplay loop muted playsinline>
  <source src="/resources/groupmentions.mp4" type="video/mp4">
</video>

Group @Mentions are designed to save you time and ensure everyone stays informed. You just type `@` followed by the group name, and our system will instantly suggest the right group thanks to preloaded data. This makes communication smoother and faster. The notification system works the same way as individual mentions, so if you tag a group, all members will be notified just as they would if tagged individually.

This feature is perfect for quickly updating your whole team, notifying specific groups like the helpdesk or design team, and keeping everyone in the loop without any extra hassle. 

We're always looking to make Blue easier and more effective to use. 