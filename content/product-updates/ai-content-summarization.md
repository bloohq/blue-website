---
title: AI Content Summarization
slug: ai-content-summarization
tags: ["product-updates"]
description: Blue now uses AI to create useful and actionable summaries from comments. This  helps you understand key points from long conversations and what to do next. 
image: /resources/bolt-background.png
date: 15/04/2024
showdate: true
sitemap:
  loc: /resources/ai-content-summarization
  lastmod: 2024/04/15
  changefreq: monthly
---

The future of work just got smarter with Blue's latest AI-powered capability - Content Summaries! 

We're excited to announce that all Blue users can now automatically generate concise summaries and action item lists for any record, powered by cutting-edge language AI.

![](/resources/ai-summaries.png)

Staying on top of important details and next steps across all your projects can be a real challenge, especially when conversations and comments get lengthy. Blue's new AI Summaries solve this by intelligently surfacing the key takeaways from even the most thread-heavy records.

## How AI Summaries Work

To generate an AI-powered summary for any record, simply click the new Lightning Bolt ⚡️ icon at the top right. Blue's AI will quickly analyze all the text content - comments, descriptions, attachments, and more. It will then provide you with:

- A concise overall summary of the record's content and context
- A bullet-pointed list of key actionable items and next steps

For example, summarizing a project brief record might give you an overview of the client's needs and main deliverables, plus a checklist of important tasks to kick things off. No more wading through long-winded threads!

This allows you to easily re-focus on the most pertinent information and priorities without losing important context and details. The AI handles the "rough cut" summarization so you can dive back in fully up to speed.

## Just the Beginning

While Content Summaries are an exciting first step, they're just the beginning of how Blue will leverage AI to streamline work for our customers. On the near horizon:

1. AI-Driven Content Categorization: Automatically apply relevant tags/properties and trigger workflows based on the AI's understanding of record content
2. Multi-Language Support: Summarize records and extract insights in dozens of languages
3. Advanced Visualizations: Use AI to generate visual abstracts, recaps, and more from any text data

*"AI is going to fundamentally reshape how teams operate and Blue is committed to making that power accessible to all our users,"* said Emanuele Faja, Founder of Blue. *"With our AI-first roadmap, we're constantly exploring new ways for this technology to simplify work, boost productivity, and surface smarter insights."*

The new AI Summaries feature is available today for all Blue customers and workspaces at no additional cost. 

![](/resources/bolt-record-button.png)

To try it out, simply click the Lightning Bolt ⚡️ icon in any record. As always, we welcome any feedback or ideas for how else to bring AI's capabilities into Blue's workflows.