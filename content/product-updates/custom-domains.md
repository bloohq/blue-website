---
title: Custom domains
slug: custom-domains
tags: ["product-updates"]
description: Blue now has the ability for all customers to run Blue on their own domain. This is now generally available and free for all customers.
image: /patterns/bluerectangle.png
date: 15/03/2024
showdate: true
sitemap:
  loc: /resources/custom-domains
  lastmod: 2024/03/15
  changefreq: monthly
---

Blue just got even more flexible and customizable for teams looking to create a seamless, on-brand experience for their users and clients. We're excited to announce that all Blue customers can now set up a custom domain name for their workspace absolutely free!

With custom domains, you can replace the default "blue.cc" URL with your own company's domain (e.g. projects.yourdomain.com). This allows you to provide a more professional, white-labeled project management environment tailored specifically to your brand and users. Setting up a custom domain is simple - [just follow our instructions in our documentation.](https://documentation.blue.cc/start-guide/custom-domains)

This custom domains launch is just the first step in Blue's vision to offer full white-labeling capabilities. The ability to have a custom domain paves the way for upcoming features like custom email domains and uploading your company's logo. With custom email domains, all notifications and emails from your Blue workspace would come from an @yourdomain.com address instead of @blue.cc. And with logo customization, the Blue brand would be seamlessly replaced by your company's own logo everywhere in the product interface.

White-labeling is a top requested feature, especially from Blue's enterprise customers and agency partners who want to provide a fully cohesive branded experience. With custom domains available now for free, and custom emails and logos on the near-term roadmap, Blue will become an invisible backbone that organizations can mold to their unique brand

**Custom domains are available immediately at no additional cost for all Blue customers, from freelancers to Fortune 500 companies.** This is just the beginning as we continually enhance white-labeling and customization options. 

To get started with custom domains or learn more, [visit our documentation.](https://documentation.blue.cc/start-guide/custom-domains) 

Let us know at help@blue.cc if you have any other questions!