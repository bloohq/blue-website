---
title: How to forward emails into Blue.
slug: forward-emails-into-Blue
tags: ["tips-&-tricks"]
description: Learn how to forward emails into Blue to create new records. 
image: /patterns/circles4.png
date: 07/09/2024
showdate: false
sitemap:
  loc: /resources/blue-basics
  lastmod: 2024/09/07
  changefreq: monthly
---
