---
title: The Blue basics in less than 3 minutes
slug: blue-basics
tags: ["tips-&-tricks"]
description: Learn all the main concepts of Blue in less time it takes to drink a cup of coffee. You'll be set for success after reading this guide.
image: /patterns/circles4.png
sitemap:
  loc: /resources/blue-basics
  lastmod: 2024/06/01
  changefreq: monthly
---

## Introduction

If you're new to Blue or looking to refresh your knowledge of the platform's core concepts, this article is for you. In just three minutes, we'll cover the essential elements of Blue, giving you a solid foundation to start managing your projects and processes effectively. Blue provides foundational building blocks that you can use to manage any type of project or process.

For more in-depth information, we suggest that you [visit our documentation.](https://documentation.blue.cc)

## Organizations

At the top level, Blue uses organizations to contain all your projects, lists, records, and other features. You can be part of multiple organizations and switch between them seamlessly. Notifications work across all your organizations, so you never miss an important update. Note that billing is per-organization, so if you have two organizations, you will be billed for users in both organizations.

## Projects

Projects in Blue are the foundation for organizing users and data. They help you segment different work areas and collaborate with your team. Projects can be goal-oriented with clear start and end dates, like a product launch or a marketing campaign. Alternatively, projects can represent ongoing processes, such as customer support or recruitment. Projects are completely private — only users that have access can see and modify the data inside of the project.

## Lists

Lists in Blue represent the steps in your process. Each list contains records that flow through the process. For example, in a sales project, lists could include "Lead," "Qualified," "Proposal," and "Closed." Records within these lists would represent individual prospects moving through the sales funnel.

## Records

Records are the building blocks of your projects. They can represent any type of data, such as tasks, sales leads, or customer inquiries. Records have default fields like title, assignee, date, and tags, but you can also create custom fields to capture specific information relevant to your project.

## Custom Fields

Blue allows you to create custom fields to store data specific to your projects. Custom fields can be of various types, such as text, number, dropdown, or reference to other records. By adding custom fields, you can tailor your records to perfectly match your unique business requirements. For example, you can add a "Deal Size" field to your sales records or a "Priority" field to your support tickets.

## Views

Blue offers various views to visualize and interact with your records. The default Kanban board view lets you see your process at a glance, with records organized into lists. Other views include Calendar for scheduling, Database for spreadsheet-style editing, Timeline for Gantt charts, Map for location-based data, and List for simplified checklists.

## Automations

Automations in Blue are powerful yet easy to use. They follow a simple pattern: "When something happens, Then do something else." Triggers, such as completing a record or changing a custom field, activate the automation. Actions, like sending notifications or updating fields, are then performed. You can create multiple automations per project, and they can be paused and resumed as needed. Automations are project-specific, but automations can also move or copy records between projects.

## Dashboards

Dashboards in Blue provide a high-level overview of your project data through customizable charts and widgets that update in real-time as your data changes. You can pull data from multiple projects at the same time, leveraging stat cards, pie charts, and bar charts.  Use dashboards to track key metrics, visualize trends, and make data-driven decisions.

## User Roles

Blue provides granular control over user permissions within each project. Pre-defined roles include Project Administrators, Team Members, Clients, Comment-Only, and View-Only. You can also create custom user roles with specific permissions tailored to your organization's needs.

## Conclusion

Understanding these core concepts will help you hit the ground running with Blue. Whether you're managing a one-off project or an ongoing process, Blue's flexible and intuitive features make it easy to organize your work and collaborate with your team. 