---
title: Task Dependencies
slug: task-dependencies
tags: ["project-management-dictionary"]
description: The relationships between tasks that determine the order in which activities need to be performed in a project.
sitemap:
  loc: /project-management-dictionary/task-dependencies
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - project-management-first-principles
  - applying-critical-thinking-to-process-design
  - project-management-basics-guide
---

**Task Dependencies** are like the recipe for your project. 

Just as you can't frost a cake before you bake it, some tasks in a project need to happen before others can start. These relationships between tasks help determine the order of your project activities.

Let's imagine we're opening a coffee shop to understand task dependencies better.

There are two main types of dependencies, which are like two sides of the same coin:

1. A "blocking" task is one that needs to be completed before another can start.
2. A "blocked" task is one that can't begin until another task is finished.
   
For example, "Obtain health permits" is a blocking task for "Start serving customers." You can't legally open your coffee shop until you have the necessary permits. 

This also means that, "Start serving customers" is blocked by "Obtain health permits."

We can also break this down into more granular and advanced task dependency types.

Let's continue with our coffee shop example:

- **Finish-to-Start (FS):** The most common type. Task B can't start until Task A is finished. Example: You can't start "Training baristas" until you've finished "Purchasing coffee equipment."
- **Start-to-Start (SS):** Task B can't start until Task A starts. Example: You can "Begin marketing campaign" as soon as you "Start renovating the shop space."
- **Finish-to-Finish (FF):** Task B can't finish until Task A is finished. Example: You can't finish "Finalizing menu prices" until you've finished "Calculating overhead costs."
- **Start-to-Finish (SF):** Task B can't finish until Task A starts. This is the least common type. Example: You can't finish "Temporary pop-up service" until you "Start operations in the permanent location."

Understanding task dependencies helps you plan your project more effectively. It allows you to see which tasks are crucial to moving your project forward and which ones might cause delays if not completed on time.

Task dependencies play a significant role in determining the [critical path](/project-management-dictionary/critical-path-method) of your project. The critical path is the sequence of dependent tasks that determines the shortest time possible to complete the project. In our coffee shop example, tasks like "Secure location," "Renovate space," "Obtain permits," and "Pass health inspection" might form part of the critical path, as delays in any of these could push back your opening day.

Remember, identifying and managing task dependencies is crucial for smooth project execution. It helps you anticipate bottlenecks, allocate resources efficiently, and keep your project on track. So, before you jump into your next project, take a moment to map out those task dependencies – your future self (and your project team) will thank you!