---
title: Deliverable
slug: deliverable
tags: ["project-management-dictionary"]
description: A quantifiable good or service produced as a result of a project that is intended to be delivered to a customer or end user.
sitemap:
  loc: /project-management-dictionary/deliverable
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - project-scope-management
  - simple-work-breakdown-structure
  - project-planning-essentials
---

A **Deliverable** in project management refers to any unique and verifiable product, result, or capability produced to complete a process, phase, or project. It is a tangible or intangible outcome that contributes to the overall objectives of the project and is typically intended to be handed over to a customer, sponsor, or other [stakeholder](/project-management-dictionary/stakeholder). Deliverables can range from concrete items like software applications, physical products, or reports, to more abstract outcomes such as improved processes or achieved milestones. They serve as crucial checkpoints in a project's lifecycle, enabling teams to measure progress, ensure alignment with project goals, and demonstrate value to stakeholders.

Deliverables are characterized by their specificity, measurability, and relevance to project objectives. They are often defined in the project scope statement and are integral components of the work breakdown structure (WBS). Project managers typically classify deliverables into two main categories: internal deliverables, which are interim products or results necessary for project progression but not directly handed over to the client, and external deliverables, which are the final products or services provided to the customer or end-user. The process of identifying, defining, and managing deliverables is crucial for effective project planning, execution, and control. It involves clearly articulating the expected outcomes, establishing acceptance criteria, and determining the resources and activities required to produce each deliverable.

The management of deliverables is a critical aspect of project success, involving several key practices. Project managers must ensure that each deliverable is clearly defined, with specific quality standards and acceptance criteria agreed upon by stakeholders. This clarity helps prevent scope creep and misaligned expectations. Throughout the project lifecycle, deliverables should be regularly reviewed and validated to ensure they meet the specified requirements and contribute effectively to project objectives. The tracking of deliverables often integrates with project scheduling and resource allocation, as the completion of one deliverable may be a prerequisite for beginning work on another. Effective deliverable management also involves managing stakeholder expectations, facilitating smooth handovers, and incorporating feedback loops to continually improve the quality and relevance of project outputs. By focusing on clear, well-defined deliverables, project teams can maintain focus, demonstrate tangible progress, and ultimately drive project success.