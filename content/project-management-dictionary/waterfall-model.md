---
title: Waterfall Model
slug: waterfall-model
tags: ["project-management-dictionary"]
description: A linear, sequential approach to project management that divides projects into distinct, cascading phases, emphasizing upfront planning and structured progression.
sitemap:
  loc: /project-management-dictionary/waterfall-model
  lastmod: 2024/08/12
  changefreq: monthly
relatedResources:
  - project-management-first-principles
  - blue-to-build-blue
  - simple-work-breakdown-structure
---

**The Waterfall Model** is a linear, sequential approach to project management that originated in the manufacturing and construction industries. It divides a project into distinct, cascading phases, where each phase must be completed before the next one begins. This methodology emphasizes upfront planning, clear documentation, and a structured progression through stages such as requirements gathering, design, implementation, testing, and maintenance.

Imagine building a house using the Waterfall Model: you'd start by creating detailed blueprints, then lay the foundation, construct the walls, install the roof, and finally complete the interior finishes. Each step depends on the previous one being fully completed.

The Waterfall Model is characterized by its sequential progression, with phases flowing in a linear, downward fashion, like a waterfall. It typically includes distinct phases such as requirements, design, implementation, verification, and maintenance. This rigid structure offers limited flexibility for changes once a phase is completed, emphasizing comprehensive documentation and clear milestones at each stage.

One of the main advantages of the Waterfall Model is its clarity and structure, providing a clear roadmap for the entire project. Its simplicity makes it easy to understand and manage, especially for smaller projects. Additionally, it sets clear expectations for deliverables at each stage.

However, the Waterfall Model also has significant disadvantages. Its inflexibility makes it difficult to accommodate changes or new requirements mid-project. Testing often occurs late in the development process, which can lead to the late discovery of major issues. Moreover, there are limited opportunities for client input during development.

The Waterfall Model is well-suited for projects with well-defined, stable requirements and a clear understanding of the final product from the beginning. It's often used in industries such as construction, manufacturing, and aerospace engineering, particularly for projects of short duration or limited complexity.

In contrast to Agile methodologies, which embrace change and iterative development, the Waterfall Model assumes that all requirements can be gathered upfront. While this can be efficient for repeated, well-understood projects, it may struggle with innovative or rapidly evolving projects where requirements are likely to change.

The term "Waterfall Model" was first formally described by Dr. Winston W. Royce in a 1970 paper, although he actually advocated for a more iterative approach. Despite this, the Waterfall Model became widely adopted in software development throughout the 1970s and 1980s, before the rise of more flexible methodologies.