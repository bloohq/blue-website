---
title: Critical Path Method (CPM)
slug: critical-path-method
tags: ["project-management-dictionary"]
description: A project management technique that determines the longest path of planned activities to the end of the project.
sitemap:
  loc: /project-management-dictionary/critical-path-method
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - benefits-project-management-automation
  - project-management-basics-guide
  - great-teamwork
---

A **Critical Path Method (CPM)** is a project management technique used to determine the sequence of tasks that directly affect the project's completion time. By identifying the longest path of dependent activities (the "critical path"), project managers can prioritize tasks and ensure timely project delivery. CPM helps in recognizing which tasks must be completed on time to avoid delays and which ones have flexibility.
