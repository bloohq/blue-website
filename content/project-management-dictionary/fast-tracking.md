---
title: Fast Tracking
slug: fast-tracking
tags: ["project-management-dictionary"]
description: A project acceleration technique that involves overlapping phases or activities that would normally be performed in sequence, often with streamlined processes.
sitemap:
  loc: /project-management-dictionary/fast-tracking
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - project-management-first-principles
  - blue-to-build-blue
  - simple-work-breakdown-structure
---

Fast tracking in project management refers to a schedule compression technique where activities or phases that would normally be performed in sequence are instead overlapped or performed in parallel. This technique aims to reduce the overall project duration by starting certain tasks earlier than they would typically begin in a traditional sequential approach. Fast tracking often involves streamlining processes and potentially bypassing or modifying standard approval procedures to accelerate project progress.

The primary goal of fast tracking is to complete the project more quickly than would be possible with a conventional approach. This can involve starting downstream activities before upstream ones are fully completed, as well as prioritizing certain critical tasks over others. For instance, in a software development project, coding might begin on some modules before the entire design phase is complete, or in construction, foundation work might start while detailed designs for upper floors are still being finalized.

Fast tracking can also involve modifying typical approval processes. Project managers might implement expedited review procedures, use concurrent reviews instead of sequential ones, or empower team members to make certain decisions without going through lengthy approval chains. This streamlining of processes is a key aspect of fast tracking and contributes significantly to time savings.

We've covered a practical example fast-tracking in our overview of [how Blue uses Blue to build Blue.](/resources/blue-to-build-blue)

One of the main advantages of fast tracking is the potential for earlier project completion, which can lead to cost savings, quicker realization of project benefits, and improved competitiveness in time-sensitive markets. It can help meet tight deadlines or recover lost time in projects that have fallen behind schedule. Fast tracking is particularly beneficial in industries where time-to-market is critical, such as technology or product development.

However, fast tracking is not without its challenges and risks. It often requires more intensive coordination and communication among project team members, as activities that were previously sequential now have complex interdependencies. There's an increased risk of rework if changes in upstream activities affect work already started in downstream activities. The compressed timeline and modified processes can also potentially lead to quality issues if not managed properly.

Successful implementation of fast tracking requires a thorough understanding of the project's critical path and the dependencies between different activities. Project managers must carefully analyze which activities can be overlapped or accelerated without introducing unacceptable levels of risk. Robust risk management strategies are essential when fast tracking, as is clear and frequent communication among all project stakeholders.

It's important to note that fast tracking is not suitable for all projects or all phases of a project. It works best when there are clearly defined phases or activities that have some degree of independence and when the project team has the capacity to handle increased complexity. Projects with many intricate interdependencies or those requiring strict sequential approvals may not be good candidates for fast tracking.

Fast tracking is often used in conjunction with other schedule compression techniques, such as crashing (adding resources to critical path activities). The choice between fast tracking and other techniques depends on the specific project constraints, available resources, and the nature of the project activities.

In conclusion, fast tracking is a powerful tool for reducing project duration through overlapping activities and streamlining processes. When applied judiciously and with proper risk management, it can lead to significant time savings. However, it requires careful planning, strong coordination, and effective communication to be successful. Project managers must weigh the potential time savings against the increased risks and complexity when deciding whether to implement fast tracking in their projects.
```