---
title: Scope Creep
slug: scope-creep
tags: ["project-management-dictionary"]
description: Uncontrolled changes or continuous growth in a project’s scope, often leading to project delays or failure.
image: /patterns/circles4.png
sitemap:
  loc: /project-management-dictionary/scope-creep
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - benefits-project-management-automation
  - project-management-basics-guide
  - great-teamwork
---

**Scope Creep** refers to the uncontrolled expansion of a project's scope without corresponding adjustments to time, cost, and resources. This often occurs when additional features or requirements are introduced without proper change control processes, leading to project delays, budget overruns, and potentially even project failure. Managing scope creep requires strict change management practices, clear communication, and constant vigilance to ensure that only approved changes are implemented.
