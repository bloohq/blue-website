---
title: Kanban
slug: kanban
tags: ["project-management-dictionary"]
description: A visual method for managing work by balancing demands with available capacity, and improving handling of bottlenecks.
image: /patterns/circles4.png
sitemap:
  loc: /project-management-dictionary/kanban
  lastmod: 2024/08/11
  changefreq: monthly
relatedResources:
  - kanban-boards-project-management
  - kanban-board-history
  - kanban-boards-common-challenges
---

**Kanban** is like a traffic control system for your work. The word "Kanban" comes from Japanese and roughly translates to "visual signal" or "card." Imagine a big board where you can see all your work laid out in front of you – that's the basic idea of Kanban.

Picture a whiteboard divided into columns. Each column represents a stage in your work process, like "To Do," "In Progress," and "Done." Now, imagine each task is written on a sticky note. As work progresses, you move these sticky notes from left to right across the board. This simple visual setup is the heart of Kanban.

Here's why Kanban is super helpful in managing projects and work:

1. It makes work visible: With everything on the board, you can see at a glance what's going on. No more wondering about the status of a task!

2. It helps limit work-in-progress (WIP): By setting limits on how many tasks can be in each column, you avoid overloading your team. It's like making sure you don't put too many clothes in the washing machine at once.

3. It improves flow: Kanban helps you spot bottlenecks quickly. If one column is getting crowded, you know that's where you need to focus your efforts.

4. It's flexible: Unlike some other methods, Kanban doesn't have fixed timeboxes or sprints. You can add, remove, or shift tasks as needed, making it great for work with changing priorities.

5. It encourages continuous improvement: Regular team check-ins (called "Kanban meetings") help you refine your process over time.

Let's look at a simple example. Imagine you're managing a content creation team:

- To Do: Ideas for new content
- Writing: Articles being drafted
- Editing: Pieces being reviewed and polished
- Publishing: Content being formatted and uploaded
- Done: Published pieces

As articles move through these stages, you shift their cards across the board. If you notice the "Editing" column is always full, you might realize you need more editors to keep things moving smoothly.

Kanban isn't just for software or manufacturing (where it started). It can be used for all sorts of work – from managing household chores to running a marketing campaign. The key is to adapt it to fit your needs.

Remember, Kanban is about more than just the board. It's a mindset of continuous improvement, focusing on delivering value steadily and efficiently. By visualizing your work, limiting your work-in-progress, and continuously improving your process, Kanban can help make your work smoother, more predictable, and more efficient.

So, next time you feel overwhelmed by your to-do list, consider giving Kanban a try. It might just be the traffic control system your work needs!