---
title: LoudEgg Webinar 2 with Blue CEO & Joe Maracic 
slug: loud-egg-webinar-2
tags: ["insights"]
description: Manny (Blue CEO) & Joe Maracic discuss finance, and B2B SaaS Strategy
image: /resources/loud-egg-webinar-2.jpg
date: 07/09/2024
showdate: true
sitemap:
  loc: /resources/loud-egg-webinar-1
  lastmod: 2024/09/07
  changefreq: monthly
---
The CEO of Blue did a second sessino with Joe Maracic from LoudEgg to discuss financial sustainability, B2B Saas, and advice for new founders. 

<youtube url="https://www.youtube.com/watch?v=5wmH8t8zND8" />

## Introduction and Background

**Joe:** Welcome to this second Blue live session. Joined today by my good buddy Francois and of course Manny, founder of Blue. Emanuel, we call you Manny, right?

**Manny:** Yeah, everybody calls me Manny. That's right.

**Joe:** Thanks guys for taking some time to go over Blue, go over other project management system tools as well. The SaaS landscape, I guess we'll cover a little bit of everything today, right?

**Manny:** Yeah, exactly. I thought we'd deep dive on a couple of interesting features in Blue and then talk more in general on the SaaS landscape because I had a lot of questions in the last couple of weeks on that.

**Joe:** That'd be great. We definitely want to get to some hard-hitting questions like, especially here's one I had for you that I've been thinking about: is blue your favorite color, Manny?

**Manny:** Yes, it is. My previous company was red, and I found that a lot, you know, it was great to begin with because it's very loud, but then when you have to go to the office every day and you see red, red, red, then it sort of gets to you over time.

**Joe:** Was it the name was red or it just had red in it?

**Manny:** No, no, it had red. It was a red color, a lot of red. Bright red.

**Joe:** I thought maybe your favorite color would be orange because blue is a complementary color of orange and you would go complete opposite. I was wrong, I guessed wrong.

**Joe:** Francois, thank you also for joining because you're one of the first, I think, Blue users, correct? When it was beta?

**Francois:** I'm Blue user since January 2021, I think. I'm using it nearly every day for my work. That's one of my main tools for trying to work with my clients on their projects.

## Business and Financial Strategy

**Manny:** I'll give you the numbers from the previous campaign. We put it on there in December 2020, and then we went into 2021. We actually left it on for about 10 months. I think we exited August 2021. So it was 10 months, and we actually made about 30k after revenue share and so on. It wasn't a huge deal for us. It was great to get a lot of feedback, but in terms of actual customer numbers, it wasn't crazy.

**Joe:** So going on AppSumo was a good idea? Coming back was yeah?

**Manny:** Yeah, it was interesting. It was definitely an interesting challenge. I wasn't expecting it to take over my life for a whole week, I'll be honest.

**Joe:** So that the idea to come back was really to connect with agencies?

**Manny:** Yeah, we want to do later towards the end of this year and then into next year, launching a reseller program similar to Go High Level where agencies can pay a yearly fee for a right to resell full white label and then sell to their clients. I see this as a very sustainable, very long-term growth channel, something that the big competitors are not doing.

**Joe:** You could be an advisor for founders. You could be actually a good adviser. You know, you have Blue the project management tool, and then Blue advisor, financial advice for that's a big important part of being a SaaS founder which I think a lot of people are lacking in.

**Manny:** Exactly. We use a company, a bank called Relay Financial. They have this built-in Profit First methodology. You can actually set up the automation in the bank account. That's really useful. I think it's one of the key things to do.

**Manny:** The way we follow is a methodology called Profit First. The idea is that you get revenue from customers, in our case everything's through Stripe, and it hits our bank account into a special account that's set up to automatically split the funds. It sends 20% of the funds to a basically locked account for profit. That's the first thing that gets moved. Then the rest of it, 80%, goes to an operational account. That 80% is then my budget to run my business.

**Joe:** You want if you need an orange, go for it. Not red, we already had red. No more red.

**Manny:** It should be green for money.

## Blue Product Overview

**Manny:** Let me go ahead and share my screen for everyone. We did do a session a few weeks back with Sarah that kind of shows more of a walkthrough, a deeper walkthrough of Blue, but we'll try to do a quick whatever I want to do.

**Joe:** Okay, good to go. Yep, you see it.

**Manny:** Cool. Created a special company today called Quiet Tech. I think a few things - we still communicate ourselves as a project management tool, but I would say 90% of use cases is actually a Process Management tool. This is something that we have to improve our communication on in the coming months.

**Manny:** I wanted to show an example of what you can do with Blue in terms of a very common use case: sales CRM. What we introduced, I think it was earlier this year, was the ability to actually sort of merge two projects or two processes together so you can link the data. Typically what happens with a sales CRM setup, you'll have your kind of workflow that you can create in Blue. These are lists, and from left to right, these are your step-by-step process. Each business really needs to think of what their unique process is, but this is a sort of simplified example.

**Manny:** You might have "unqualified", so you're not sure if this customer is going to be a good customer for you, if they have a budget and so on. You're going to have discussions, so you can move this over to qualification. You're typically going to prepare a proposal, send it, do some final negotiations, and then closed won, closed lost. Blue supports this very nicely.

**Manny:** We've also got some tags here for service. For instance, I can click on service B and that would then filter everything out except focusing on deals that have service B. If I click on a deal, you can see that everything is auditable here. All the changes are made, and you can see I've got some custom fields here on the left. I have an amount, which is what I want to try and sell the customer for, then I've also put in here my cost.

**Joe:** I love the flexibility of it because I'm using it right now in two different ways. With other project management tools, I kind of have the same format for each company, but with Blue, I have multiple styles that I'm using. On some, I'll just list the projects on the left side and do the Kanban view there, where on others I'll just have certain workflows on the left side. I feel like Blue is one of the more flexible project management tools. Is that because you created it so that it could kind of adjust to the user more than other tools?

**Manny:** Yeah, exactly. I mean, I think it's based on my experience speaking to a lot of different businesses. It's very difficult for me to guess how somebody else should run their business, and also, why should I? There's different industries, and this is also the challenge of building a horizontal SaaS business where we're not vertically focused on any specific industry. You have to make things configurable.

## Future Plans and Development

**Joe:** AI is at some places in Blue but not extensive. Do you have any plans, future plans for AI in Blue like analysis or project reports?

**Manny:** Yeah, absolutely. Not a well-known fact, I'm actually doing my masters on large language models, specifically on document evaluation using LLMs. Essentially, it's how to scan thousands of documents and extract meaningful data and patterns. So that's my thesis. So yeah, for sure we'll be building that out.

**Manny:** I haven't jumped on the AI bandwagon super strong yet just because I wanted to see what everybody else is doing and then build out something thoughtful. Right now we have two main features. One is a summarization of discussions within records. So if you have 20 comments, you can click summarize, you'll get a summary and then action points. And then auto-tagging, so you can auto-tag your entire project using AI, or you can tag a bug, select only a few of them, or even an individual record.

**Manny:** What we want to do in the future is using AI as an enabler. Imagine that right now if you want to set up a project according to best practices, you either need to really think through that yourself or read our documentation or get on an onboarding call. Ideally, I think AI could really help with that in the future. So you can describe your business, your process, what you're trying to achieve, and then it can build out your project workflow, your automations, your custom field structure, and even across projects like referencing.

**Manny:** Also with the dashboards, right now you have to manually set them up and you've got to have a bit of knowledge for that. It'd be much easier to be able to just type that and the AI has got context of all of this data and then actually build this out.

**Joe:** Here's a question from Rean. A lot of the LTD owners, the early adopters, I'm guessing they don't always use the tool to the max. Do you notice from AppSumo buyers, even from the first time around, they'll use it for the first couple weeks but then put it on the shelf? So it's a different user base than the MRR community which they're using, so you might not get as much feedback in the long run.

**Manny:** Interesting. I notice the opposite. I have some customers that pay four figures a month for Blue. They have quite a few seats and also additional support. I had a call with a CEO of one of these a couple of days back. He wanted some assistance, and he actually read about the reference fields and he wanted to implement that in his business and sales CRM process. I thought, wow, we haven't spoken for a while, when was the last time we spoke? And it was just under a year ago, about 10 months ago.

**Manny:** On the flip side, you get LTD purchases who maybe spent $49, $99, who then email us 20 times in the first week. I kind of feel that the LTD users are very exploratory, they want to learn a lot. They give very high-quality feedback.

**Joe:** That's great. Very cool.

## Integration and Compatibility

**Joe:** Can we connect it with SMTP? That's the plan with the white label?

**Manny:** So essentially, white label will have the custom domains which we already have available today, and it's free of charge. The white label will allow you to essentially have complete control over the branding. So that will be fonts, that'll be the colors of the interface to your exact company color, that would be the logo, and all the mentions of Blue. Then we will also not allow anyone that you invite in Blue to get any welcome or marketing emails from Blue, so they'll be completely excluded from that. But you'll be able to set your own email by SMTP or some other functions as well to send emails, and you can create your own welcome emails, your own onboarding emails, and also all notifications come from your email address.

**Joe:** Can we create databases using AI? Can we populate?

**Manny:** Not right now, no. We don't have that kind of function, but that kind of links back to what I said earlier about being able to create new projects using AI and describing what you want to achieve.

**Joe:** Is it possible to create AI formula?

**Manny:** I do want to do this in the future, where you can create a custom field which you can create a dynamic prompt on that field and pull data from the other fields to basically send that to the AI and get a response back. Which would be incredibly useful for a lot of different use cases.

**Joe:** So we connect with a platform like Logic Sheets?

**Manny:** I don't know that one specifically, but I think if it's on Pabbly Connect or Zapier, you could probably do that. We don't have any direct connection with that.

**Joe:** Do you have any other SaaS product?

**Manny:** No, honestly I see this as something that I want to do for a multi-decade thing, make it my career. I'm not looking to do this and spin it off, sell it, and then start something else. I'm very passionate about project management and workflows, and I'm fortunate enough that it seems to be working out. So I sort of want to dedicate myself to this.

**Joe:** Yan would be interested really knowing about the level AI integrations you have on roadmap apart from the roadmap is on the Blue website.

**Manny:** For this, I mean we have already kind of spoken a little bit about this, but essentially yeah, the ability to create dashboards using AI instead of having to manually set them up, the ability to create projects, semantic search, and then much further down the road, a sort of general project assistant that you can ask questions and it can answer based on the data that's in your project.

## User Experience and Onboarding

**Joe:** Francois, because you've been using Blue for so long, do you feel like you would like it added or are you comfortable with having separate CRM or invoice things?

**Francois:** I prefer it to stay project or process management and not a CRM or invoice thing. I've got another solution for that anyway. I think it's better to focus on what Blue is made for, and if people need such kind of stuff for invoicing, it's really, really big stuff to make and I don't think it's worth it. I think it belongs in your finance system, in your QuickBooks or your Xero.

**Joe:** Manny, does your book come with a bonus with Blue?

**Manny:** It's free, you can just download it off the website.

**Francois:** I've got a question for Manny. It's about the new editor. You asked about the code, it was not possible to put code in the chat, and you replied that you were working on a new editor on the chat section. That can be interesting.

**Manny:** So right now we're using two different editors in Blue. The wiki and the docs use something called Tiptap, which is an open-source, really nice editor which has a lot of real-time features. That's the one that we also want to port over to the record comments section. The current one is actually using an older common system called Trix, which interestingly enough was developed by Basecamp, another project management software, and they open-sourced it.

**Manny:** However, it's got some limitations. Basecamp in a similar way to Blue, they're also very much about simplicity. So it's not surprising that their text editor is relatively basic. Now we've heard feedback from yourself, Francois, but also from hundreds of other users that they want more power and flexibility there too. So we're going to be moving the Tiptap, which is the wiki and doc editor which is much richer, over to the comment section as well. That will come as part of the system upgrades that we're doing over the next couple of months, and that will allow you to have code blocks, so you can get highlighted code.

**Joe:** Here's a question, Francois. From Mael, I was thinking as well, do the customers think the interface is easy to use?

**Francois:** It depends. In fact, when I was using Blue at the beginning, Blue has really evolved. Manny hasn't talked about it, but it's really important to understand that the product has really had a massive evolution in few years. Of course, it's more complex than it was before, that's true, but it's more powerful also. So yes, it depends on the client. I think if you onboard them a little bit, if you make a little video to explain how they can get to the information, it's all right.

## Pricing and Licensing

**Joe:** Is this AppSumo campaign this just a FOMO to collect funds?

**Manny:** To put it into perspective, I mean, I think although this is a good campaign for AppSumo, I'll see this probably will walk away with this in the six figures, I assume, by the end of the campaign.

**Joe:** How much longer is it going to be on AppSumo for?

**Manny:** Great question. We initially planned with our account manager for a sort of six to eight week campaign, but now that it's sort of spiked up a lot, I'm also wondering if I want to maybe close it early and limit the overall number of codes. I didn't really have a chance to think in the last couple of weeks about that too much, so we may sort of bring that timeline forwards.

**Joe:** Just to answer Frieza's question, I mean, I think still with the AppSumo campaign, those will still be a very minority of revenue for us this year or next year. I think the key thing, my personal takeaway is that if I could get anywhere between 100 and 500 solid agency partners out of this who can then be on an agency plan next year and resell Blue, I think that'll be extremely happy with that.

**Joe:** Manny, you've been kind enough to also do a giveaway, right? Five tier ones?

**Manny:** Yep, five tier ones.

**Joe:** We can if give away now as well if you want to give away a couple now for anybody you want to ask a question. Maybe who gets it first? I could ask a question: What was the dominant color in Manny's previous company that he mentioned earlier?

**Manny:** Red.

**Joe:** Sam got it right. Sam, congrats!

**Joe:** Francois or Manny, do you want to ask a question for another winner?

**Joe:** Or I could ask, does anyone know where Francois is from?

**Rean:** France.

**Joe:** Looks like Rean was the first. Congrats Rean!

**Joe:** All right, so we have three more to give away. We can give those away later. I'll talk to Manny, go through the comments, the questions.

**Manny:** Yeah, for sure. Head over to AppSumo, appsumo.com/blue. We've also got our website blue.cc, you can check out there's a lot of documentation, lot of resources. You can start a free trial even before you buy from AppSumo, and it will automatically work once you buy, as long as you use the same email as your AppSumo account.

**Joe:** Great, and thank you Manny for the thorough webinar we had. It was nice to cover some other things, not just Blue.

## Closing Remarks

**Joe:** If you missed it, the beginning, check out the beginning of this video where we dive into more financial strategy, some other good things. Maybe we could do this again. Next time we'll do it in French, Francois will host, I will sit here and just nod, I'll smile.

**Manny:** We could do it every week, every week in a different language.

**Joe:** Could make sometimes. Well, why not once make a video about Blue in French for French community?

**Joe:** Check out Francois. Francois does some nice videos, reviews. Francois, your company is Visit 360, right?

**Francois:** Yes, Visit 360 Pro. That's the name of the company.

**Joe:** Great. Specialize in VR for professionals?

**Francois:** Yeah.

**Joe:** Thank you, thank you everyone for joining. Sorry we ran a little bit long on this one. Thank you again Manny, Francois, you guys are the best. Hope everyone has a good weekend and let's do this again sometime.

**Manny:** Wonderful, thank you so much. Pleasure.

**All:** Bye, see you.