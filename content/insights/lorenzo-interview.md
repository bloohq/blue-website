---
title: Interview with Lorenzo Hickey
slug: lorenzo-interview
tags: ["insights"]
description: An interview with Lorenzo Hickey, founder of ShapeShift World and Blue early adopted. He discusses his entrepreneurial journey and the impact of of Blue on business.
image: /customer-photos/lorenzohickey2.jpeg
date: 05/07/2024
showdate: true
sitemap:
  loc: /resources/lorenzo-interview
  lastmod: 2024/07/06
  changefreq: monthly
---
**Where are you from and what is your background?**

I was born in San Diego, California, a city teeming with diverse cultures and languages due to its proximity to the Mexican border. Growing up, I was surrounded by a Spanish-speaking community, which made me realize early on the importance of learning multiple languages. I even studied German for seven years! My background is in accounting, but my true passion was always to travel the world and work from anywhere.

**What was your first job, and how did it shape your career?**

My first job was at TRW (Thompson Ramo Wooldrige), a massive $2 billion business back in the 80s and one of the top 50 Fortune 100 companies globally in terms of revenue. I was fortunate because they immediately sponsored my MBA and put me on a global financial project. Imagine this: a young professional working on a $500 million portfolio and writing a currency conversion program. This project took me to different corners of the world and taught me the complexities of global business. It was during this time that I realized and understood the value of scalable, global solutions like Blue.

**How did your career evolve?**

After TRW, my career took several interesting turns. First, I worked across various sectors, including oil and gas, automotive, and technology. I managed a $50 million business from a controller's perspective and witnessed the revolutionary shift from mainframes to PCs, which ignited my interest in technology.
Then came the exciting world of startups. Over the years, I helped grow 28 software and technology companies. Two of them went public, two were shut down, and the rest were either merged or acquired. This phase was like a boot camp in solving business problems with technology and creating scalable solutions.

Finally, I spent 20 years in banking, starting as a consultant and eventually becoming a Senior Vice President at Union Bank in California, before it was acquired by Mitsubishi (MUFG). I worked on loan origination software, data marts, and data warehouses, playing a significant role in transforming how small businesses accessed loans.

A second pivotal project was to design, build and deliver the Small Business Lending web portal for Bank of America. My team co-managed all aspects of the UI/UX, deal processing and back-end integrations to the bank's mainframes. It was a wonderful experience to manage and work with a very large company, multiple teams and different sub-projects. The product offerings were business credit cards, loans, leases, lines of credit and merchant services. 

Yikes… I could have used Blue back then! 

**What are you currently working on?**

These days, I have three main lines of business, all leveraging scalable platforms. 
First, I'm involved in lead generation using AI-based tools to provide leads to small and medium-sized companies. We use AI to scrape information from web visitors and turn hidden visitors into real people, who our clients can follow up with.
We have licensed and partnered with a new search technology platform that goes beyond traditional SEO and Google strategies. It highlights the overlooked search activity on many websites outside Google. The team uses AI to track buyer intent across the internet, flag potential leads based on their search behavior, and match them with a consumer database. This database includes people who have opted into marketing campaigns, allowing real-time contact with genuine leads. 

Second, I run a travel benefits platform called [Elevate Travel X](https://elevatetravelx.com/), which actually utilizes Blue as a reporting repository. 

Third, I manage a [joint venture affiliate revenue share model](https://jvease.com/), which handles transactions and revenue splits within one system. Essentially, we take your payment data and automatically calculate all your affiliate or joint venture partner revenue shares and ensure that everyone is paid the right amount (all done automatically). 

Blue is integral to all these lines of business and so much more. I like to say Blue plays a role in every customer we support at [SHAPESHIFT World.](https://shapeshiftworld.com/)

**What advice would you give to someone starting out in business or technology?**

Focus on products or technologies that are configurable, extendable, and scalable. The last thing you want is to outgrow your systems quickly, which can lead to costly and disruptive transitions. When choosing a solution, you need to think long-term and ensure that the technology can grow with your business.
For instance, imagine launching a CRM system designed to manage 5,000 customers. In the early stages, this system might work perfectly, but as your business grows and your customer base expands to 50,000, you don't want to find yourself constrained by a system that can't handle the increased load. This would force you to switch systems, a process that can be time-consuming, expensive, and fraught with challenges.

![](/customer-photos/lorenzohickey3.jpeg)

Choosing scalable solutions means that your technology infrastructure can seamlessly support your growth. This is where Blue excels. Blue’s scalability ensures that as your business evolves, you won't be held back by your software. It’s built to expand and adapt to your needs, whether you're managing 5,000 customers or 50,000.

And scalability isn't just about handling more data or users. It’s also about the flexibility to integrate new features and adapt to changing business requirements. Blue’s configurability allows you to tailor the system to your specific needs without extensive redevelopment. The whole API is fully open and you can even send data out via configurable Webhooks — it’s a dream come true! 

This means you can add new functionalities, integrate with other systems, and adjust workflows as your business environment changes.
Scalability also involves considering the future landscape of your industry. Technology and market conditions evolve rapidly, and your business needs to be agile enough to respond to these changes. By investing in scalable technology, you are essentially future-proofing your operations. This proactive approach can save you from potential setbacks and position your business for sustainable growth.

**What about from a management or business perspective?**

Building strong relationships is crucial. Some of my most successful ventures started with relationships where I was invited to help run the company. One of the things I like about Blue is its culture of open conversation, feedback, and commitment to growth. A company must have a roadmap and commitment to time-based sprints and new features. This dedication is essential for building solutions and relationships that serve our joint customers.

**What initially attracted you to Blue?**

I discovered Blue through AppSumo back in December 2020, and was immediately drawn to it because it reminded me of a product from one of my previous companies that went public. Blue's ability to be configurable for various use cases, without needing to hire a developer is a key factor. Additionally,  its [security](/security), and its browser-based data management were key factors. I saw Blue as a smaller version of a highly successful product I had worked with before.

The ability to create a bulk data import and export at a field level and [100% API coverage](/platform/api) were dealmakers for me. 

Remember… I am a recovering accountant!

**Lorenzo, it is interesting that you mention the API, as we made a very deliberate decision in the early days to expose the very same APIs that we use to build Blue to our customers, ensuring that we would always have a [100% API coverage](/platform/api). This means that anything you can do via the Blue interface, you can do via the API.**

Honestly, that was a fantastic decision. In many companies, the API feels like an afterthought and it’s badly documented and lacks a lot of functionality, so it is a breath of fresh air to be able to integrate with Blue. 

**What are the biggest changes you’ve seen in Blue?**

Blue has evolved into a single source of truth and a system of record. It can adapt quickly to business changes, making it a pillar for business growth. The enhancements in automations, [custom field customization](/platform/custom-fields), and [integrations](/platform/integrations) have been significant. In the last four years, it has morphed it into a completely different product. In fact, I’d say it has gone from a product into a platform.  

**What’s your favorite feature in Blue and why?**

[Automations.](/platform/automations) They allow me to streamline processes and reduce management time for routine tasks, focusing more on strategy and valuable activities.

**What would be your dream new features for Blue?**

Conditional logic for automations would be a complete game changer. Personalized views of data tailored to user roles and on-demand queries would add immense value. These features would make Blue configurable for both enterprise organizations and small businesses, mirroring expensive data builds from years ago. Complete white labeling could also be very exciting.

**Any parting words?**

Thanks for taking the time to speak with me! I’ve been using Blue for years and have been watching as it improves each month. To anyone reading this interview, I want to leave you with one thought: *Blue is extremely powerful and worth it, even if you use just 5% of its capabilities!*

It's an insurance policy on managing change.

**Thank you Lorenzo!**

You’re welcome, it has been a pleasure! 
