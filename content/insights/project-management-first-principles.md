---
title: Project Management First Principles.
slug: project-management-first-principles
tags: ["insights", "project-management"]
description: Starting with first principles, we provide a comprehensive understanding of what is a project, and how to effectively manage projects. 
image: /resources/project-management-first-principles.jpeg
date: 07/07/2024
showdate: true
sitemap:
  loc: /resources/project-management-first-principles
  lastmod: 2024/07/07
  changefreq: monthly
---

At Blue, we spend a lot of time thinking about project management. It’s critical to what we do and the success of what we deliver to our clients. Without a solid understanding of project management, how can we possibly build [project management software?](/)

Every time we begin a new project, whether that's a new feature, a backend engineering upgrade, or marketing campaign, or a recruitment drive, we start by analyzing the project itself and how it is being run. So, having a great deal of experience and insights, we have decided to start formalizing some first principles of project management. 

Although it is common (and acceptable) to navigate a project using a more informal, personal gut feeling, it is hardly a scalable approach — and not without its drawbacks.

## A Project

Let’s start by defining a project.

> A project is a temporary endeavor with a defined start and end to produce a unique product, service, or result. This is how most people think of projects. But there are also some other, more formal properties of projects that are important to consider:

- A project has finite resources (time, money, people);
- A project is undertaken to achieve specific objectives;
- A project has a defined scope (so there are clearly things that it won’t be doing);
- A project is subject to uncertainty and risks.

Now that we have a better understanding of what a project is, we can move on to the first principles — the core building blocks that a project requires to be successful. These include:

- A project manager;
- An objective;
- A list of things to get done — this is often called a plan;
- Someone assigned to do each thing;
- Timelines for each thing;
- A communication strategy with a regular cadence of updates and meetings, and a plan for where information will be stored.

We’ve found that these things typically apply to all projects and, if done well, remove 99% of all problems within projects.

## A Project Manager

The first thing that a project needs is someone in charge of successfully completing it. This might seem like an obvious statement, but it is worth emphasizing.

This person *is* the project manager. They are responsible for ensuring that the project’s objectives are met and that the project is completed on time, within budget, and to the required quality standards. Often, many of us become de-facto project managers without being a trained project manager. If that's you, then you're in luck, [we wrote a guide for that!](/resources/project-management-basics-guide)

It is worth considering what a project manager should and shouldn’t do. After all, they have a lot of responsibility, and they must take this responsibility seriously.

Often project management can take a significant chunk of a project budget — typically between 2% and 10% of it, depending on how risky the project is. The more risk, the more project management is required to ensure that things stay on track and the project does not fail.

Because they are a manager, the project manager should be focused on how to achieve goals through other people. They need to ensure that the project team is effective and that they are working together harmoniously towards the common goal. The project manager is the one who needs to make sure that everyone is doing their job and that they are doing it well.

The project manager is also responsible for communicating with the project’s stakeholders and keeping them updated on its progress.

Much like we discussed in [Prioritization for Leaders](/resources/prioritization-for-leaders), project managers should constantly be focused on the point of constraint. If getting stakeholders to approve something takes too much time, then they need to be hyper-focused on getting that commitment. They need to be constantly asking themselves, “What is holding us up from completing the project objectives, and how do I unblock the problem as quickly as possible?”

In some ways, project management is synonymous with communication. Writing detailed ideas down is a valuable skill for a project manager, as it drastically reduces the number of meetings required — team members can self-serve the information required.

## An Objective

Another core building block of a project is that the project needs a clear and measurable objective. Once the project has finished, it should be trivial to look at the results and know whether the project achieved its goals or failed.

The objectives should be published so that every team member has access to them. This is important because it means that decision-making can be distributed and decentralized, as each team member can look at their small piece of the work and link it to the project’s overarching goals. Then, they can raise issues if there is a misalignment or change their approach in their small area of the project to best serve the larger objectives.

The objectives of a project should be SMART:

- **Specific.** Projects often have high-level objectives that sound more like mission or vision statements. An example of a lousy objective is “improve customer support”. This is not specific enough. Something such as “We aim to email back 90% of customers within 30 minutes by the end of Q4” is far more specific.
- **Measurable.** When you set specific goals, you then generally solve the measuring problem simultaneously because then it is easy to assess if the goal has been reached.
- **Achievable.** Objectives only make sense if they can be achieved. But this is not an excuse to create easy objectives. A good objective should have an element of stretch — challenging enough to be able to be reached, but not impossible or highly unlikely.
- **Relevant.** This may sound highly self-evident, but you’ll be surprised how often this happens. The project objectives should be able to be hit by doing the project. You cannot set objectives that cannot be controlled by the project team who are doing the specific work of the project. For instance, teams working in a corporation’s manufacturing division may not influence the in-store shopping experience of customers.
- **Time-bound.** Good objectives need to have some type of timing attached to them. This can be difficult, especially with modern software projects where the work-to-be-done is not always clear, so timelines can be extremely difficult to estimate. Sometimes you can be off by multiples, and it is nobody’s specific fault. That said, having timelines ensures that the project’s overall scope stays within certain bounds. If you have one month to solve a problem versus one year, you will come up with very different solutions.

The objectives must be repeated to all the team members ad-nauseam: they should be (figuratively) sick of the project manager constantly discussing the objectives. Some real-time (or as close to real-time as possible) dashboards that track progress can also be invaluable to ensuring everyone is aligned towards the objectives and knows how the project is tracking.

## A List of Things to Get Done

We strongly suggest calling a plan “a list of things to get done” instead, because it demystifies the planning process.

Plans are often guesses, but they are still worthwhile. This is because the process of planning itself helps shed light on what we know and what we do not yet know.

> Fail to plan, plan to fail.

The essence of a plan is to get a detailed list of all the steps to go from the start of the project to its successful completion. Some actions will require previous steps to be completed, and so you might end up with a series of linked tasks, often called dependencies. Other actions can be done in parallel and do not block each other.

The project manager should always be focused on the most important actions that are blocking other actions, as this defines what is known as the critical path of the project. That is, the minimum amount of time in which the project can be completed if all the dependencies are lined up end to end.

Working on the plan from both ends of the project can help tremendously in building an effective plan. This means that you start from the beginning and go to the end, but then you also start at the end and work your way backward to understand what is required to finish the project and hit the objectives.

This last part of reversing the planning process will generally unearth missed tasks.

But, the planning process is not complete until we understand who is responsible for each task and how long each task will take to complete.

These points should be covered simultaneously because they are so closely linked. You cannot create estimates for work without involving the experts who know the specifics of that tasks, and even then, you still need to keep an eye on your resourcing. An individual can rarely work effectively on multiple things at the same time, so you need to understand if the project plan is likely to have bottlenecks. This is where one person cannot complete all the assigned tasks because they simply do not have enough time available.

## Someone Assigned to Do Each Thing

Each task in a project needs to have a specific individual who is given responsibility for completing that task. If you find that you need multiple individuals assigned to the same task, you often have not gone to a granular enough level to distinguish tasks.

For instance, in many industries, there will be design and build phases; this could be in manufacturing, construction, or even software. You should break this down so you can assign the designer’s specific tasks and the builder’s specific tasks. You’ll often find a back-and-forth between the two disciplines, which you can factor in. An example might look like this:

1. Design Phase 1 by Designers
2. Feasibility Check 1 by Builders
3. Design Phase 2 by Designers
4. Feasibility Check 2 by Builders
5. Review by Senior Stakeholder
6. Finalization of Design by Designers
7. Final Checks by Builders
8. Approval by Stakeholders

This shows a realistic up-and-down between the different parties involved, instead of having a “Design Phase” alone assigned to the design team to the completion of the design phase.

## Timelines for Each Thing

This may be the most challenging part of creating a plan — how do we measure how long each thing will take? As discussed earlier, timelines should not be done top-down but bottom-up. This means that each item is estimated by the person who is going to do the work (or at least a knowledgeable colleague) and that all these estimates are added and calculated into an overall timeline.

That said, it is helpful to have an overall high-level timeline before the detailed estimations are made because estimating how long something will take and deciding on the specific approach to solving the task are essentially the same.

So if a project has generous high-level timelines, then individual experts can factor that in their estimates and try to optimize solutions for the long-term instead of cutting corners to ensure timelines are hit. If the project has a rushed timeline, then each expert knows they may need to make trade-offs.

One thing often forgotten about during the creation of project plans is that stakeholders will need to review progress and confirm decisions that need to be factored in. The best way to do this is to look at the previous project that includes the same set of stakeholders and understand what a typical turn-around time for them to provide feedback is.

Another danger here is that if there are monthly steering committee meetings with key stakeholders, what happens if a project deliverable cannot be submitted in time for one of those meetings? Does the project go on pause until next month when the stakeholders meet again, or will this be dealt with and approved (or sent back with feedback) in between the monthly steering committees?

## A Communication Strategy

When issues like this arise, they confirm that the approach to communications within a project is of the utmost importance.

Usually, the problem is not that there is a bad strategy but that there is no strategy in place at all. So communication channels open up in an informal and ad-hoc manner, and “good enough” becomes the standard.

The problem with this is that things don’t always go to plan. People change, knowledge is lost, and as the project gets more extensive and more and more people join, the communication overhead can quickly explode.

So, clearly, the tools themselves are not that important; it is just a fact that as you scale past a non-trivial number of people (say, 10), teamwork becomes increasingly more difficult.

That’s because team communication does not scale in a linear fashion compared to the number of people on the team.

For instance, if you have a team of 2 people, there is one communication thread (between the two individuals). Throw another person into the mix; now you have three communication threads. So, while the team size has increased by 50%, the communication threads have increased by 300%.

Let’s see how this grows:

- 2 team members = 1 communication thread
- 3 team members = 3 communication threads
- 5 team members = 10 communication threads
- 8 team members = 28 communication threads
- 10 team members = 45 communication threads
- 15 team members = 105 communication threads
- 20 team members = 190 communication threads
- 30 team members = 435 communication threads
- 50 team members = 1,225 communication threads
- 100 team members = 4,950 communication threads

This can be expressed by the following equation:

n(n-1)/2

Where  *n* is the number of people that need to be involved in the project.

As you can see, this number grows exponentially as the team size increases. If you have a team of 10 people, 45 potential communication threads need to be managed. But if you have a team of 50 people, there are 1,225 potential communication threads — that’s 27 times more! And if you have a team of 100 people, there are 4,950 possible communication threads — that’s 110 times more!

It is important to note that this is not just a problem with larger teams but with any team where individuals are not in the same location. This is because the number of possible communication threads is not limited by the physical proximity of the team members.

For example, you have a team of 10 people, but they are all located in different parts of the world. In this case, there are still 45 potential communication threads that need to be managed — even though the team members are not physically proximate to each other.

And it can get worse. The above calculation assumes just one method of communication. If we want to account for different methods of communication, we would have to rewrite the equation in the following manner:

n(n-1)/2

Where *n* is the number of communication channels.

Let’s retake the number of communication threads above, but this time let’s assume that *n* is comprised of:

- Email
- Group Chat
- Personal Chat
- Comments in a project management system
- Calls
- Notes/Comments in documents

Plugging the numbers in, this is what we get:

- 2 team members = 6 communication threads
- 3 team members = 18 communication threads
- 5 team members = 60 communication threads
- 8 team members = 168 communication threads
- 10 team members = 270 communication threads
- 15 team members = 630 communication threads
- 20 team members = 1,140 communication threads
- 30 team members = 2,610 communication threads
- 50 team members = 7,350 communication threads
- 100 team members = 29,700 communication threads

It is scary how quickly the number of communication threads grows — and if remote work more easily enables larger meetings that would not be practical in real life, then it is a double-edged sword.

So, in short, a strategy is crucial to avoid the communication explosion that can happen with even a relatively small number of team members.

And while the idea of a “communication strategy” can sound complex and grandiose, it does not have to be. The key thing is to set a few principles, state key communication channels, and perhaps also tell people what not to do.

A few fundamental principles to consider:

- **Keep information open.** Assume that everything needs to be easily findable and shareable months/years from now. Ensure everyone on the project team has access to all required information, and be careful to name files and documents with this in mind.
- **Err on the side of clarity.** Imagine someone new is joining the project team. Document information clearly and concisely to ensure that they can easily understand critical decisions.
- **Keep meeting minutes.** Ensure that all significant meetings are recorded and easily findable.

In terms of communication channels, the fewer, the better. We would suggest reducing it to the following:

- [A project management software](/) to have task-specific discussions.
- Meetings (in person or remote) with meeting minutes and [somewhere to find all the project meeting minutes in one place.](/platform/wiki-docs)
- Documents: this could be comments and discussions within Google Docs or specific tools (i.e. Figma for UX design) that allow comments.

It is also acceptable to add [group chat](/platform/chat) to this list — but then it has to be carefully managed as it can become an enormous all-day (and everyday!) meeting without a clear goal or agenda.

The project manager needs to set these rules clearly and be a shining example of how to collaborate while occasionally using a stick (metaphorical, not literal!) to hit project team members back into communicating correctly.

## Final Thoughts

Regardless of one’s level of expertise, it’s always valuable to step back and view a topic or process at the foundational level. So, in taking a deep dive into the subject of project management, we’ve identified the key first principles required to successfully execute a project.

Each project should have a project manager: they ensure that the project has clear objectives, which are broken down into tasks to be completed to reach the final goal. The project manager then needs to assign a team member to each task. Together, the team can identify the timelines for each individual to-do and for the entire project.

It’s often the seemingly matter-of-fact things that can cause issues, rather than advanced subjects, that tend to receive more attention — for instance, communication. This is why there should be a well-defined communication strategy both within the project team and externally, with the stakeholders.

Project management is a complex subject, so this is by no means an exhaustive guide. There are still some factors to consider that we have not covered in this initial overview, such as budgets, methods to resolve conflicts between options, and how far to take delegation within a project.
