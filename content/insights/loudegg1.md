---
title: LoudEgg Webinar with Blue CEO & Joe Maracic
slug: loud-egg-webinar-1
tags: ["insights"]
description: Manny (Blue CEO) & Joe Maracic discuss product, strategy, and more. 
image: /resources/loud-egg-webinar-1.jpg
date: 07/09/2024
showdate: true
sitemap:
  loc: /resources/loud-egg-webinar-1
  lastmod: 2024/09/07
  changefreq: monthly
---
The CEO of Blue recently sat down with Joe Maracic from LoudEgg to discuss product, strategy, and more. 

<youtube url="https://www.youtube.com/watch?v=yqTs_mC-HOo" />

## Introduction

**Joe:** Good morning, good afternoon, and good evening everyone tuning in for this Blue session. I have my Blue shirt on for the occasion.

**Manny:** Hello everyone, it's great to be here.

**Joe:** Manny, for people who don't know where you're from, could you share that with us?

**Manny:** Sure, I was born in Italy. I'm half Italian, half British. I've been living all over the place for many years. I lived in the UK, then in Asia for 10 years. For the last three years, I've been traveling around the world. Right now, I'm in Panama, and I'll be heading back to Italy around October to finally settle down after three years on the road.

**Joe:** That's quite a journey. So Italy is where your primary home is, I guess?

**Manny:** Exactly. I haven't lived there full-time for about 15 years, so it'll be interesting to acclimatize myself again to Europe.

**Joe:** Do you miss it? Do you miss Italy?

**Manny:** I miss the food, I'll be honest. It's something special there.

**Joe:** I definitely miss the food when I go to Croatia, but my doctor does not because my cholesterol goes through the roof from all that eating. You gain a little weight while in Europe, but nothing beats European food.


## Blue's Origin Story

**Joe:** What's the origin story for Blue? Why did you create a project management system tool like this, which is great by the way? How did it all start and when did it start?

**Manny:** That's a great question. A lot of people say I'm a bit crazy to have done it because it's such a crowded space with many very well-funded companies. We're a sort of bootstrapped company; we didn't take any venture capital.

The reason was I had a previous company called Mad. It was quite a successful agency doing product design, mostly for large companies and funded startups. We designed mobile apps, websites, brands, that kind of stuff.

I really struggled in the first few years. We grew very quickly - we were 30-35 people in six months, and I just struggled to stay organized in terms of managing my projects and ensuring our clients had success.

We started out with Basecamp, and that was good actually. I kind of liked Basecamp, but then we quickly outgrew it. There were too many people, too many to-dos, and there wasn't the flexibility to track processes.

So we upgraded to Asana. That was pretty good too, but the problem was the price. It was expensive - $24 if you pay yearly, $30.49 if you pay monthly. With a team of 35 plus some contractors, plus potentially clients that need access, you're looking at $1,000-$1,500 a month for a team. That was quite expensive for me at the time.

The second problem was that I had to keep onboarding my team and my clients onto the software because they didn't really want to use it. It was a bit too complex. I was in Asia at the time, and I really felt that it was designed for American knowledge workers, not for the other billions of people that live everywhere else. It wasn't intuitive.

So I had this idea to hire an engineer specifically for this. We took about three months to build the first version, and you know what? It was terrible, honestly. It was a mess.

**Joe:** You needed a project management system tool for the project management system!

**Manny:** Exactly! But the advantage we had was that I had the engineer and me working, and we had these 30-odd people who were forced to use it in the same office. We were getting feedback and complaints in real-time because we were sitting next to them. This built a virtuous cycle where we could get a lot of product ideas and feedback very quickly, and then we made it good enough that we could actually shift all our projects away from Asana.

About a year in, some of our clients started asking, "Hey, this system that we're using to manage the projects, it's pretty good. Can we use it for our business?" The answer was, "Not yet, but let me get back to you." Then we registered a company, hooked up on Stripe, and we launched and started billing people. That's kind of how we got started.

## Key Features of Blue

**Manny:** Let me share my screen and talk you through some of the features and how it works.

First of all, we've got this Records Tab, and this is really where most of the work happens. This is where you can add your items, your records, create your processes, and so on. In this case, I've got a general sort of project management process which goes from ideas, backlog, in progress, all the way to done.

These lists are flexible, so you can obviously rename them. You can even lock them, do bulk actions on them, and create new ones as well as drag and drop to change the order.

This is one way of viewing your data. You've got all these filters up here. For instance, I can say, "Okay, I want to just take a look at the high priority items," I can click and it will instantly filter down. You can also layer on filters as well.

You can filter on multiple items, filter by assignees, and also with any of the custom fields in the projects. I can filter by those too. I can say, for instance, "person in charge," I can do a condition, and then I can add a value. I can also stack these. Essentially, what this gives you is if you've got thousands of records, you can easily filter down to exactly what you need.

You can click in to see an individual record. We have the discussions on the right, so users can add comments and also see a log of everything that has been changed. On the left, you've got your custom fields, your metadata of the particular record.

The great thing is that with these fields, you can delete any fields that you want, including the default fields. So if you don't need assignees or tags for this project, you can actually go ahead and delete those. You can also rearrange this left side completely.

We've got like a couple of dozen options for fields, some advanced ones as well where you can pluck data from other projects.

We have six different views as well. Another one is calendar, which essentially takes all your due dates and puts them on a calendar. You can easily sync them to Google Calendar, Outlook, Apple calendar just by copying and pasting this link and dropping it into your calendar app.

We have a sort of Gantt chart or timeline so you can see what things are coming up in a visual manner. We also have a database view which is kind of an Excel-like view.

**Joe:** I noticed with Blue that it's hard to find a way that it works for everybody. When you're working with that group and you're starting it out, you're going to get a lot of comments. How did you decide which ones were priority, which ones weren't?

**Manny:** That's a great question. Really, the way I see it is that my role is not only CEO but I'm essentially the product manager for the product. I don't have anybody else in a product management role.

The first thing is that I look at volume: how many inbound requests for a particular feature am I getting? That's one thing I need to look at. The other thing is my own vision for the product: what do I want to see it going forward?

We also keep an eye on the market. We look at what other companies are doing, what's working for them, what people are saying on forums online. Taking these different signals, I can then choose out of the 400 things that we could do in the next quarter, how do I pick the three most impactful ones?

By impact, I mean the ones that are going to help us retain customers, stay unique compared to the other platforms, be a unique competitive advantage, and also what's going to help us attract new customers.

## Blue's Unique Selling Points

**Joe:** I love Blue. I'm glad you guys are coming back to AppSumo because that's how I think I first discovered it. The way you could customize it, I haven't seen on a lot of other project management systems. I really like that you can add these certain fields to make it kind of my own project management system. It identifies with the user more, where some of the project management systems are very general and this is what you have to work with. I don't like a lot of the other ones.

**Manny:** So really, that's the shift that we've been making. We started out as a pure play project management system, and we still sort of call ourselves that, but I see it much more as a process management system. People are using it for hiring, for sales, for onboarding, and so on. They're not really managing projects that have a definite start and end date, but they're managing ongoing processes that will be running for years.

What we've done in terms of customization, for instance, we used to have everything called "to-do" before the rebrand from Bloo to Blue. Now we've recalled that "records" because essentially it's one system of truth. You've got these records, but then customers on each project or each process can rename that to whatever they want. If they're tracking deals, they can rename that to "Opportunities" or "Sales." If they're tracking an onboarding process or hiring process, they can rename that to be "Employees."

The same thing with custom fields. The future vision of this is that we will enable all the features to be fully renamed. Then also the ability to add new tabs with either different views or variations of the features. You can have one tab that's your Kanban board, add another tab which is a filtered view on a database of the same data.

Our strategy is horizontal. We're not focusing on a particular vertical, so it really does need to be customizable so people can use it how it best works for them instead of us guessing what works for people.

**Sarah:** How can you support beginners? Because as someone who loves tools like this absolutely, and I've been using them for years, the learning curve when I first started was ridiculous. Will you guys have some kind of templates to help people get started?

**Manny:** We've got a few strategies for onboarding. Obviously, when you sign up, we kind of do a tour, an automatic tour of the system. We also send you a series of emails kind of teaching you bit by bit about the system in a logical fashion.

We've got ridiculously in-depth documentation that we completely revamped last year. You can search on it, you can also ask questions using AI which is quite interesting - it will search through the documentation and pluck out the answer for you.

A few additional improvements we want to make that we'll release later this year is that we're actually going to have sample projects as well. We already have templates which you can get up and running quickly, but you can also check out sample projects. This will be like, "Okay, you can see how you could use Blue to open a coffee shop or to run a construction project or to run a sale," and it will be populated full of dummy data.

One of the pain points before, when my other company was using Asana, was that we found it difficult to use because I think it was too centric on developed countries' knowledge workers. When I started Blue, I was living in Cambodia. My first customers were in Cambodia. The level of education there and the digital literacy is not what it is in Europe or the US, and so those first customers we were onboarding, we had to make it really easy to use.

## Technical Aspects

**Joe:** I have a quick question. Is there a way to see, like, high level what you've shared with people? On a data governance level, like what if one of our team members shares something but we don't realize that it's still a live link. Is there a way to see what's live in public?

**Manny:** Yes, so basically on here, we add a little sharing icon under files. So you can see here we've got the file sharing is on. You get a sort of visual view. We don't have like a report on everything that's shared across the organization, but I think that's a good idea for the future.

**Joe:** How does data egress work? Obviously, if someone's coming onto the platform, they're looking to stay, but some businesses need to be able to egress their data in a certain way. I know you can export projects or boards as a CSV, but how do you get images or attachments and stuff like that?

**Manny:** For exporting all your data in terms of a project, you can export via the CSV. But obviously, as you pointed out, that's not going to export files and other kinds of data like that. The way you can do this is that every Blue account comes with an API, and it's 100% coverage. So every single thing that you can do by clicking on Blue, you can do through the API.

We made a very conscious decision right at the beginning, like six-seven years ago, to do that. So basically, the API that we expose to customers is the same API that we use to power Blue. It's not like a baby version or dumbed-down version. Essentially, you can loop through to then download all your files as well.

On the point of file storage, the other nice thing about Blue is that we offer unlimited file storage to accounts, and the maximum individual file size that you can upload is 5 gigabytes, which is a lot more than many other systems. Some systems limit you to only 100 megabytes, for example.

**Joe:** Can you speak to server locations, data governance stuff like that, just kind of in a general sense or at least give some place people can explore?

**Manny:** We've got a sort of security page on our site which is at the bottom of the footer. In general, we post up all our third-party processors, so you can see actually where we're hosting things. We also have our full technology stack online. Every single piece of technology that we use is outlined, down to the library.

In terms of the overall data hosting, the bulk of the data is hosted on AWS. That's both for your files, which is on S3, and we use the Asia Pacific region at the moment in Singapore. That's mostly because of historical reasons - I was living in Asia when we first started. We use the databases also running on AWS.

In the future, we definitely want to migrate to a sort of global setup where we have databases in Europe, databases in the US, and then customers will be automatically redirected to the closest database. The information will be shared across everything. So if you've got a business that has employees in Asia and employees in the US, both of them will access local databases and then they will stay in sync in the background automatically.

**Joe:** Do you offer that for Enterprise clients? Because Enterprises is going to be a totally different ball game.

**Manny:** No, we're strict on having one set of infrastructure overall for everybody and the multi-tenancy model, just because it scales very well. We do have Enterprise customers, and the reason they pay quite a lot more is that we'll listen to them more for the roadmap. They can also sometimes contribute to the cost of specific features they need. Finally, it's the support that they really value - having dedicated communication channels where we can do training and answer questions.

## Future Plans and Roadmap

**Joe:** Many small businesses are moving to ClickUp. What features are in your pipeline, although Blue is quite solid? Basically, what is in your roadmap?

**Manny:** Sure thing. A lot of companies don't share roadmaps publicly. We do. Let me just bring it up. On the platform, you can see "Roadmap," and essentially you can see everything here.

Just to talk you through: Public views - you can share any view as a link that people outside of Blue can access. You can add preset filters and so on. You can see who's been viewing a project. AI email automation - being able to have AI write the emails for you. Also, an AI project assistant, essentially a little widget where you can chat to your project, and it knows all the context.

We've got two AI features right now. One is the AI auto-tagging which I mentioned, and the other is simply the ability to summarize a discussion in a record. So if you've got 20 comments, you can create an action summary. We will allow these to be turned off in the future. We don't have that now, but I do realize that some companies just don't want AI features there.

In terms of the specifics of the actual AI feature implementation, we strip out data that's kind of personal. For instance, in the AI tagging, we don't include assign data because that would include people's real names. We automatically hide that, and then we send it across. We use OpenAI for this. The way that it's set up in our agreement with them is that the data is not stored after it's being processed. We temporarily share the data, we get a response back, and then they delete the data on their side. So there's nothing that's kept with third parties.

We have an engineering blog where we did a big write-up on our AI Auto-categorization - like an engineering deep dive. This shows all the challenges and how we thought about the data and all the prompts and so on.

Back to the roadmap: We've got the idea of having multiple tools for projects, customizable cards so you can decide what you want to show on the front of your card, more login methods. Right now we just have magic link, but we'll bring in SSO, Google, email, and password. Also, an assign custom field - a new custom field that you can get a drop-down of users, and you'll be able to decide that the drop-down list will only be for specific user roles, for instance.

The other thing that's not there but is basically the white label - that's probably the big thing for us until the end of the year. Allowing you to upload your own logo, turn on White Label which means a few things: you've got your custom domain, you've got your logo and the branding so Blue is completely removed, and then the emails that we send out can come from your email address instead of ours. Also, all your users are excluded from our onboarding emails so you can create your own, and they're excluded from our newsletter.

**Joe:** Will the calendar sync with Google Calendar?

**Manny:** Absolutely. You get a link for a calendar you can drop into Google Calendar, and then all the events will pull that. You've got three choices there: project calendars, the company calendar (everything that's got a due date in all the projects you're in), or also your personal calendar, which is all the items that are assigned to you across all your projects. You can pull that into your calendar.

## Pricing and AppSumo Deal

**Joe:** How come you came back, you're coming back to AppSumo?

**Manny:** Great question. Long story short, they essentially reached out and it was an interesting opportunity. I think also when we were first there, we were, I think, a baby product. I think we were 14 months in, so we didn't have 80% of what we have now. So it was, you know, they made an interesting case to actually go in.

I've seen also the sort of pre-hype before coming to AppSumo this last couple of weeks, so I think it's going to be a very good launch. But I also think, long-term, our mission in general is to make project management simple, flexible, powerful, and the final fourth one is affordable. That's part of our mission.

I kind of feel that the whole industry is broken right now. The way it is, you've got these companies like Monday.com, ClickUp, Notion - they've raised, I think on average, about 500 million each. And then they're also losing tons of money every year. I mean, to put it into perspective, I think Asana lost like 100 million in 2023. They spent 450 million on marketing.

What's happening is that they're essentially flooding the market, they're raising the cost of customer acquisition. So unless you play by the same game where you are also paying thousands of dollars to acquire one customer upfront with the hope that they stay on for a long time, you basically can't compete. But it also raises the price for everybody. So for the end customers, you're now forced to pay to get a full system, you've got to pay 20-30 dollars per user, which is actually really expensive when you scale that.

I start to ask myself, okay, what are the advantages that Blue has compared to these companies? We've got a comparison system on our website for a lot of them. We're equivalent in many aspects. There's things that they do slightly better, there's things that we do better.

One of the key advantages we have is that I don't have a board full of venture capitalists who need to tell me what to do. The only thing I'm focused on is my customers. I've got a much lower cost base. We have a small, very focused team. We're not trying to do everything under the sun.

I may consider in the future moving the whole business over to a lifetime deal model, and then having just a few modules where you can subscribe if you want extra features. For instance, it could be a space upgrade, it could be the full white label, this kind of stuff. And then really make project management accessible to the 50 million small businesses that right now are not using project management systems.

**Joe:** The deal will stay on for AppSumo, for how long? Do you have a specific end date?

**Manny:** We don't have a specific end date. I mean, we'll see how it goes. I think there's definitely a top-end of licenses in my mind that I want to reach. And then after that, we probably would close it. So it really depends.

**Joe:** Is the white label feature part of the LTD deal?

**Manny:** In general, it's going to be a split-out module, even for subscribers who are paying monthly. It won't be included in the LTDs, but we'll definitely probably make exceptions for very early sign-ups that have been supporting us for a long time.

**Joe:** How do we add it on as an LTD buyer? Because I already know I'm gonna buy it.

**Manny:** Once that's live under company settings, there'll basically be an area that you can unlock by paying, unless your account has it included. And then you'll be able to change all the settings there.

## Comparison with Competitors

**Joe:** How do you compare with Nifty or Nimbus?

**Manny:** Nimbus I'm not that familiar with. For Nifty, I would say Nifty has got a lot going on in the interface, so I'd say it's kind of a little bit easier to use. I think some of the unique features that I showed, like custom permissions and that kind of stuff, we have that they don't have. But you know, I think they've also got quite a few things like mind maps and that kind of stuff.

I will take the time to highlight something that I have on the website. If you go to resources and compare platforms on our website, you can compare any plan from any of these major project management platforms against each other. It doesn't even have to be Blue.

So you can say, "Okay, I want the standard plan of Blue, I want the Pro Plan of Basecamp, and I want the Enterprise of Asana and the unlimited plan of ClickUp," and you'll get a breakdown of every single feature, which has got what, and any limitations.

**Joe:** So I don't have to go to G2 or another third-party place. I can just go to you and you actually show us. That's so cool.

**Manny:** Exactly. We've done research, we've signed up for all of these and actually done that. So that's also a way if you're doing research on a project management system and you want to compare plans, you can do it because I think typically the features that you see on a lot of these plans, they're really only at the more expensive ones.

**Joe:** What are the main integration features Blue has?

**Manny:** In terms of native integrations out of the box, we've got the calendar integration so you can actually pull that in and put it into your calendar. Then we've got the API and the webhooks, so that enables, if you're a bit technical, you can make your own custom integrations. I know some customers integrate to their own systems.

Then we have connections with IFTTT and Zapier, so that allows you to basically connect to 5,000 other tools, both sending data in and sending data out.

We've also got plans in the future to allow one-click imports from ClickUp and other platforms. During onboarding, we know some customers are already on those systems and are looking for a simpler, more affordable alternative. So you'll be able to import your full project from those platforms.

**Joe:** Can we restrict the view settings for specific persons? Is user access management controlled by the different roles of the persona?

**Manny:** The way the user access works is, first of all, every project is completely private by default. So you have to add somebody specifically to the project for them to see it. You don't get this confusion where you add somebody to a team and then suddenly they're seeing all the projects underneath, some of which were supposed to be private.

In terms of assigning user roles to somebody, essentially the way we do it with custom permissions is it's a new user role, and then you drop people into that user role for that project. If you want to assign different versions to different people, you can create different custom user roles or use some of our kind of predefined ones.

The other nice thing to mention is that with Blue, the users that count against the lifetime deal are only the administrators, the team members, and the custom user roles. The comment-only, view-only, and client roles are unlimited for everybody. We don't charge for those because I know from experience as an agency that you might have 30 core team members, but you could easily have 150 client users.

## Closing Remarks

**Joe:** Manny, you've been on for over an hour now. You still have a little time to answer questions?

**Manny:** Yeah, absolutely.

**Joe:** Okay, great. So I guess we can go through that quick. So the deal is not live yet, right? It's the 14th and then the 17th?

**Manny:** That's right.

**Joe:** How many employees does Blue have?

**Manny:** We're a small team. We have seven full-time people, and that's basically everybody is an engineer and then one person on support, or customer success as we like to call it. So it's basically doing onboarding calls and then answering the kind of email questions and that kind of stuff.

**Sarah:** I was so surprised how easy it was and how flexible it was. The only thing I want is, I know mobile app is being worked on more, but I think that the platform now, I know why a lot of the AppSumo users that were posting in Ken Mo's group a couple weeks ago were saying "Blue's coming back, Blue's coming back!" Oh my God, everyone was so excited and now I get it.

**Manny:** Yeah, exactly. I think also that comes to the heart of the thing, which is product-led growth. So the idea that essentially you try and build a product that is good enough that people then talk about it and tell their friends or their colleagues and get them on board.

**Joe:** Manny, do you want to start showing us, for people that haven't seen it, a lot of great questions in the chat, so I guess we'll do questions towards the end.

**Manny:** Yeah, absolutely. Let me share my screen.

[Manny proceeds to give a detailed demonstration of Blue's features]

**Joe:** Manny, thank you so much for doing the session. It's been wonderful. Thank you.

**Manny:** Thank you, guys.

**Joe:** So it's coming back to AppSumo tomorrow for Plus members only, then it'll be on for us non-plus regular folks, us peasants. And you can contact help@blue.cc, correct? That's your email?

**Manny:** Correct, and blue.cc is the website.

**Joe:** So that's great, and they have a great roadmap, so check those out. Okay, thank you again. Comments, you could keep asking questions. The thread will be live on YouTube, Facebook. Sarah Parker, you have a new tool, right? You want to mention it real fast?

**Sarah:** Absolutely. Those of you that know my software selection process, I'm very particular and I go very in-depth. So as you know, if I'm excited about Blue, you know that I've already researched it and there's a lot of things I like about it. I created a tool that is a spreadsheet, an app, and a printable worksheet in one that you can purchase that has my entire five-step process and full education built in to be able to select software. Blue is going to pass with flying colors in my tool, I can tell you that already.

**Joe:** Awesome. All right, so thank you. Thanks, everybody, for joining. Thank you, Sarah. Thank you, Manny, for making an awesome tool, and see you all next time.

**Manny:** Thank you, guys. Bye-bye.