--- 
title: Cultivating Deep Work In The Modern Workplace
slug: deep-work
tags: ["insights", "modern-work-practices"]
description: Cultivate a culture of deep work for enhanced productivity. Discover how to make deep work a priority in your organization.
image: /patterns/lines2.png
sitemap:
  loc: /resources/deep-work
  lastmod: 2024/06/01
  changefreq: monthly
---

Work has changed tremendously over the past decades. While the generations before us might have had one job their whole lives, the abundance of information in our generation allows us to shift between multiple professions without much difficulty. Because our ancestors had their whole lives to perfect their craft, they were able to perform at their peak level and produce high quality of work. Take a biology teacher for example, who has worked for more than 30 years before she retires; With time and intensity of focus put into her work, her teaching skill in biology could possibly outrank the others as she had had 30 years to perfect it.

Work in the 21st century is nowhere close. The rapid advancement of technology today has revolutionized work and our understanding of the world. It is almost impossible to be in one job for the rest of our lives, because jobs evolve and become irrelevant quickly. Yet, the constant change has created so much noise around our lives that we lack the basic skill our ancestors had - the ability to perform deep work.

The concept of deep work was termed by Cal Newport as:

> The ability to focus without distraction on a cognitively demanding task. It’s a skill that allows you to quickly master complicated information and produce better results in less time. - [Cal Newport in Deep Work](https://www.calnewport.com/books/deep-work/https://www.calnewport.com/books/deep-work/).

Our generation is becoming more and more distracted. At work, we are constantly nudged by urgent emails, pointless meetings, and the need to be ‘busy’. Thus, our ability to do deep work gradually diminishes.

## The need to always be ‘available’.

The internet and various social media channels have made it so convenient for us to communicate. In fact, it’s harder now to not communicate. Most workplaces require their employees to always leave the chatroom on, so if anyone needs anything, you can respond immediately. This makes perfect sense if you work in customer service, but most of us don’t. You could be in the middle of an important task, and be distracted by a ping from a colleague discussing a certain matter. Before long, you would join and completely miss out on the task you’re trying to complete.

Distractions in the age of technology are inevitable, but we can be less distracted by adopting what experts call [‘asynchronous communication](https://doist.com/blog/asynchronous-communication/)’. It is the kind of communication where you don’t expect an immediate response back. 

This means that instead of structuring your day around your work group chats, emails, and meetings, you take control of how you spend your time, and that usually leads to meaningful work progress. Most of the things that people claim as ‘urgent’ at work are not truly urgent. Companies that value deep work practice asynchronous communication. They understand that the price of interruption to an important task is higher than a quick response to work email.

> To produce at your peak level you need to work for extended periods with full concentration on a single task free from distraction. - Cal Newport in Deep Work.

Companies can start incorporating asynchronous communication to their work culture through simple actions.


**Allow notifications-off hours.** 

Instead of expecting your team to be online all the time, give people permission to be offline whenever they need. This will help your team gain work autonomy, thus boosting their productivity and motivation more. However, it is important that you establish trust and open communications within the team so they feel comfortable being offline.

**Use no more than 2 tools.**

The problem a lot of teams face is the confusions and miscommunications caused by using too many tools. Try to limit to only one project management tool to track your work progress and one real-time team chat tool if necessary. Having your team switch around multiple tools for work distracts them from focus and causes productivity loss.

**Respect everyone’s deep work space.**

If you want to create space for your team to do quality work, you must value everyone’s deep work space. This implies that, if you need to speak to a coworker who’s putting on noise-cancelling headphones, perhaps find a chance to speak to them another time or leave them a message (with no expectations for quick response).

## The ‘all-day’ meetings.
You probably met a person whose response to ‘how’s your day?’ is a description of how many meetings they joined. Some meetings are great, but some are completely unnecessary and time-wasting. An average knowledge worker’s day consists mostly of replying to emails, sending documents to their coworkers, joining meetings, and perhaps some work. But if you spend 80% of your time in meetings, then where’s the time to do deep-focused work? Organisations have to distinguish between meetings that are necessary and ones that could be just a text.

**Progress updates.**
An update meeting that could go over 2 hours can be just an online status update. A team at a design agency, Mäd, posts their updates in Bloo everyday instead of hosting a physical meeting. It takes far less time and everyone stays updated on what others are doing. They can use the time saved by doing tasks that help them reach their goals.
However, in some cases, face-to-face meetings are needed. It is important that you define the outcome and agenda of physical meetings clearly before asking your team members to dedicate 1-2 hours’ worth of their time.

**Quarterly or business plannings.**

It is certainly better to meet face-to-face to discuss something as important as your next business goals or objectives. It is also a chance for your team to pitch in their ideas and take ownership of the goals.
**One-on-one coaching. **Sometimes, this can be done via online call, but it feels more alive to see the person sitting across from you.

**Meeting with clients.**

If your job role requires you to meet clients often, then you might have to spend a lot of your time attending meetings. What you can do is to schedule those meetings on only 1 or 2 days of the week and dedicate the rest for deep work. It is all about blocking the time you are most productive for meaningful work that brings about progress.

## The physical workspace.
Our modern day workplace cherishes an open-plan office where employees are expected to share a desk and common spaces. That type of set-up is great for fostering collaboration, sharing ideas, and generating more creativity as you are less confined to a small space. But it doesn’t support deep work. It is extremely difficult to produce at a peak level when you are just one poke away from answering to your coworkers or joining a discussion. We have come this far from a closed-door office, and we can’t possibly go back to when everyone had their own office space.

**A space for deep work.**

Organisations can balance it out by providing a space to do deep work. Having a closed-space which everyone can use when they want to deepen their focus, and still keep a shared space for meaningful collaborations . The room should be equipped with all necessary office tools for work. If they need to jot down a quick idea, it is better to have a sticky note at the reach of their hands instead of walking out to get it.

## Working long hours vs. producing great work.
Long hours of work have long been equated to hard work and productivity, but not [great teamwork](/resources/great-teamwork). In fact, the majority of HR policies in companies and organisations reward based on hours worked. But long hours don’t necessarily generate quality work. [Cal Newport’s principle of deep work](https://knowledge.wharton.upenn.edu/article/deep-work-the-secret-to-achieving-peak-productivity/) suggests that maximum output requires not just the hours, but the intensity of focus invested in. 

If your long hours are all about responding to emails, checking social media, joining pointless meetings, and switching across different working tools, then you are setting yourself up for being busy rather than being efficient.

To produce at a peak level, you need to enhance your deep work skill. Just like a muscle, your ability to work deeply gets better after every exercise.

**Start by scheduling your hours for deep work.**

You may notice yourself being extremely focused during some hours than others, so use that time to focus on completing a task. It is natural to feel the tendency to shift to a different task after awhile, but the key is to persist on and stick to one task at a time.

**To know which task to work on, prioritization must be done.**

It starts from setting quarterly or yearly goals for yourself and team, and breaking it down to what must be done today to achieve those. Prioritizing tasks also requires you to say no to other tasks that won’t contribute to the big picture.

**Practice restraints from social media.** 

A high level of performance requires your deep focus and elimination of everything else that distracts you from it. This means restraining yourself from social media whenever possible, turning notifications off when you do deep work, and using a tool that has everything in one place instead of you having to switch around.

**Don’t work on all your tasks at once.** 

If a task is so important that it deserves your full attention, then don’t shift your focus to other tasks before you complete this one. It may take a long time and you may have to put other tasks on hold, but if you are serious about doing a great job, you have to immerse yourself in it and keep your focus on the objectives.

**Small breaks.**

Deep work is not about the time invested, it’s about the intensity of focus you put into the work. Your deep work capabilities may sometimes tire out. When that happens, take small breaks off of focus and restart again when you can.

## How Blue Team Does Deep Work.

**Flexible work hours.**

Each one of us is a different chronotype. We perform better at a certain time of day than others. Flexible hours allow our team to have more control over our work schedule. We place more value on the results than the hours put in.

**Meetings only when it’s necessary.**

We have a weekly team update to review work, discuss new features, and get updates on customer communications. All progress can be tracked on Blue.

**No deadlines for Software Development.** 

It is one of the jobs that require extreme focus and attention to detail. Our engineers work without strict deadlines and we don’t put a due date on when a feature should be developed, unless it is a demanding bug that needs to be fixed. This way, our engineers can focus on developing a great product for our users. 

**Communication in one place.** 

For us, flexible work hours means we must maintain great communications most of the time. 80% of our communications are in Blue, be it updates, discussions, or comments on specific tasks. The other 20% is on Telegram; it is where we do synchronous communications and informal chats. Doing deep work is also one of Blue’s principles, as we believe we can only do as good of a job as our ability to work deeply and focus intensively.

