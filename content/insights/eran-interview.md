---
title: Interview with Eran Bucai
slug: eran-interview
tags: ["insights"]
description: Interview with Eran Bucai, founder of DotComTruths.com and a Blue power-user. He shares his journey, insights on getting started, and tips for solopreneur success.
image: /customer-photos/eranbucai.jpeg
date: 28/05/2024
showdate: true
sitemap:
  loc: /resources/eran-interview
  lastmod: 2024/05/28
  changefreq: monthly
---
**Hey Eran! Thank you for agreeing to be interviewed. Before we discuss how you use Blue, why don’t you introduce yourself and your work?**

Sure thing, I'm Eran Bucai, the founder of DotComTruths.com.

**Why call it DotComTruths?**

When I read "DotComSecrets" by Russell Brunson, I found it incredibly helpful. However, I noticed a trend where many people were copying Brunson and using the term “secrets” in their marketing. This "hush-hush" approach felt disingenuous to me. It contributed to a kind of marketing hype that I found misleading. Many of these pitches made it seem like achieving success online was easy, which is far from the truth—it’s really hard.

For beginners, it’s especially challenging because they often lack the knowledge to make informed decisions about investing in expensive courses. In my opinion, these high-ticket courses are unnecessary until you have a clear understanding of what you need.

That's why I named my site DotComTruths—to provide a more transparent and realistic perspective on what it takes to succeed online.

![](/resources/dotcomtruths.png)

**You position yourself as the opposite of Dan Lok, the King of High Ticket Sales. Can you elaborate on that?**

I don’t really compare myself to Dan Lok or any other marketer as such. I just try to serve my customers as best as possible, with reasonably priced products and services, while still being profitable.

Absolutely. In the SaaS industry, the failure rate is notoriously high—around 95% or more. This is similar for entrepreneurs in general. My approach is different because my customers aren't required to invest huge sums of money just to get started.

Unlike Dan Lok, and pretty much most coaches, who focus on high-ticket sales, I believe in a more accessible and realistic approach. I don’t hard sell success because I can’t guarantee it. Success depends significantly on the individual’s work habits and dedication.

My goal is to help people understand that while success is possible, it requires hard work and smart strategies, not just expensive investments.

**Is there a correlation between hard work and success in this field?**

Absolutely. Hard work is crucial, but it’s not just about putting in long hours—it’s about focusing on the right tasks. For instance, spending a year perfecting your website is largely pointless if you haven’t validated your idea or secured any customers. The key is to prioritize activities that directly contribute to your business's growth.

This means getting your product or service in front of potential customers as quickly as possible, gathering feedback, and making necessary adjustments based on real-world interactions.

It's about being efficient and strategic with your efforts, ensuring that every hour spent is moving you closer to your goals.

Building a successful business requires a combination of smart work and hard work, focusing on customer acquisition, validating your ideas, and constantly iterating based on market feedback.

**You’ve been a nomad since late 2022. What do you like most about the nomad lifestyle?**

The freedom to choose where to go without affecting my work. Meeting people is easy through expats, Facebook groups, and co-living spaces. In Brazil, for instance, just hearing someone speak English was a good reason to strike up a conversation because it was quite rare!

**What’s the downside of being a nomad?**

Constantly dealing with accommodation and flight logistics.

**Tell us about your work with funnels and landing pages.**

I specialize in building funnels and landing pages, and I do this interactively on Zoom by sharing my screen. This way, my clients can follow along and learn how to create these crucial components themselves.

My focus is on helping individuals and businesses sell digital products online. This involves not just the creation of funnels and landing pages, but also teaching them how to automate and systemize their operations to streamline the tech side of their business.

To support this, I run a YouTube channel where I offer free tutorials on various aspects of digital marketing, including funnel creation, automation, and tech setups. These videos provide a solid foundation for anyone looking to improve their online marketing skills.

For those who want to dive deeper, I offer online courses that cover more advanced marketing strategies and detailed tech setups. One of these courses is specifically designed to teach users how to effectively use Blue!

My approach is all about empowering my clients with the knowledge and tools they need to succeed. By combining live, hands-on training with a wealth of free resources and in-depth courses, I aim to make digital marketing accessible and manageable for everyone, regardless of their starting point.

**Who are your typical customers?**

Anyone wanting to sell information products or expertise online. This includes coaches, affiliate marketers, membership site owners, course creators, and service providers like freelancers, web designers, and video editors.

**Can you explain your 'Strangers to Customers' concept?**

Of course. The 'Strangers to Customers' concept is about transforming people who have never heard of you into paying customers. When someone encounters your brand for the first time, they typically don't know who you are, what you do, or why they should trust you. Your goal is to bridge that gap and build enough trust and rapport to make them comfortable with making a purchase.

**How is this done?**

To achieve this, you need to create multiple touchpoints that gradually build trust and familiarity. These touchpoints can be anything from a blog post or a video on your YouTube channel to a social media post or a referral from a friend. Each touchpoint should provide value and help potential customers understand who you are and how you can help them.

Here’s how it works:

The first stage is where strangers discover your existence. This could be through social media, a blog post, an advertisement, or a YouTube video.

Once they are aware of you, the next step is to engage them with valuable content. This could be a free guide, a helpful video, or a webinar that addresses their pain points and showcases your expertise.

After engaging with your content, they start considering your offerings. This is where more detailed information about your products or services comes into play, such as case studies, testimonials, or a free trial.

At this stage, they are ready to become customers. This might involve a sales call, a demo, or a direct purchase from your website. Your job is to make this process as smooth and reassuring as possible.

Once they have made a purchase, the goal is to keep them engaged and satisfied, turning them into repeat customers and advocates for your brand.

I wrote an [article](https://eranbucai.com/blog/the-hustle-of-finding-clients-online-and-building-an-audience-for-real/) on this and [also made a video on how to get more traffic.](https://www.youtube.com/watch?v=phcUPTdi2nw)

Throughout these stages, it’s important to have both automated and manual touchpoints. Automated touchpoints, like email sequences and online videos, allow you to scale your efforts and reach more people. Manual touchpoints, like one-on-one calls or personalized emails, can build deeper trust and address specific concerns.

The key is to have a balance between these methods and to ensure that each touchpoint is designed to move the person closer to becoming a customer.

Ideally, you would want to have a personal approach to every customer, but there are not enough hours in a day for that! So I try to get the next best thing.

This systematic approach ensures that you are consistently nurturing relationships and building trust, which ultimately leads to conversions.

**How did you get started?**

Honestly, my journey began with a simple desire to make more money. I didn’t go to high school, so I needed to find a way to generate income that didn't rely on formal education. Before transitioning into entrepreneurship, I worked in HR-related roles.

By late 2016, I felt a strong urge to make a significant change. I was determined to pursue something more fulfilling and lucrative. In March 2017, I started to see the fruits of my efforts and began making money through my new ventures. This early success gave me the confidence to keep pushing forward.

In 2019, I started building websites as a freelancer and things started to evolve and grow from there.

**So the typical “overnight” success story that takes close to a decade!**

That’s right—anyone who tries to sell a get-rich-quick scheme is lying. Real success takes a lot of time and effort. My journey is a testament to that. It wasn't instant or easy, but it was incredibly rewarding. It took years of hard work, learning, and perseverance to reach the point where I could sustain my business full-time. While the process was challenging, the ability to build something of my own and achieve financial independence has been worth every bit of effort.

**Any advice for aspiring solopreneurs?**

Focus on proof of concept and get your first customer as soon as possible without overcomplicating things. You need a lot less than you think to get started. Before securing your first customer, you have no real indication if there’s a market for what you’re offering, so you could be heading in the wrong direction without realizing it.

Many people avoid the crucial step of getting customers because they fear rejection or failure.

However, real learning happens through action, not by reading another book or taking another course. Embrace the possibility of failure as a learning opportunity and use it to refine your approach.

The key is to start small, validate your idea with real customers, and iterate based on their feedback. This approach minimizes risk and ensures that you’re building something people actually want. Don’t wait for perfection—launch quickly, learn fast, and keep improving.

**How can someone get started with you?**

You can get started without any commitments by checking out my membership PDF at this [link](https://eran.link/pdf). It explains the five routes and how to get started for free. If you decide to commit later on, there are also [subscription options available here.](https://eran.link/dct)

**How did you hear about Blue?**

It all started with a cold email from the Blue team. They reached out, asking if I could review their product on my YouTube channel. Now, I receive a lot of cold emails, and most of them go straight to the trash. But something about Blue caught my eye—maybe it was the way they personalized their message or the clear value they presented. So, I decided to give it a shot.

The Blue team’s email wasn’t just another generic pitch; it was tailored and engaging, which made all the difference.

A few years later, I actually met the CEO of Blue in Bulgaria—we had great fun playing lots of chess matches and speaking business.

![](/resources/eranmanny.jpeg)

**Who won the chess game?**

We're pretty evenly matched, which made it great fun!

**Perhaps you should do a live chess battle at some point! You know, Blue actually got its first ever paying customer—a design agency in Australia—via cold email!**

Ah! That’s super interesting—it does work if done well.

**So what attracted you to try Blue?**

I’m always open to trying new software tools, especially since my niche involves helping people navigate the tech side of their businesses. When I first encountered Blue, what really drew me in was its intuitiveness. I could figure it out without needing a tutorial, which is a significant advantage. Many tools require a steep learning curve, but Blue was user-friendly right from the start. This ease of use is crucial because it allows me to quickly implement it and help my clients do the same without getting bogged down in complicated instructions or setups.

**What were you using before Blue?**

Before switching to Blue, I was using Airtable.

Airtable is a powerful tool, primarily focused on data management, which makes it great for organizing information but less ideal for collaboration.

Its complexity often posed challenges, especially when coordinating with multiple virtual assistants (VAs). The data-centric approach of Airtable required a lot of setup and maintenance, which made the collaboration process cumbersome and time-consuming.

In contrast, Blue offered a more intuitive and user-friendly interface that was designed with collaboration in mind.

The transition to Blue was seamless because it simplified our workflow, allowing my VAs and me to focus more on our tasks rather than getting bogged down by the tool itself.

The collaborative features in Blue, such as real-time updates, easy task assignment, and communication tools, made it much easier for us to work together efficiently.

This shift significantly improved our productivity and streamlined our processes, making our overall workflow more effective and less stressful.

**What’s your favorite feature in Blue?**

Definitely the automations. As a solopreneur, you need to automate as much as possible to stay efficient and effective. Automation helps you become process-based, which is crucial for scaling your business without getting overwhelmed.

One of the best ways to leverage automation in Blue is by creating checklists based on specific actions, which then become automated Standard Operating Procedures (SOPs).

**What’s the most valuable use case for you in Blue?**

Managing my entire video production pipeline—from video editing to thumbnails to YouTube SEO—with checklists for each step. It’s also handy for my other workflows, like coaching and tech setup services, which are completely handled by my VAs and automated, yet I can still keep oversight.

For instance, when I create content for my YouTube channel, I have a checklist that covers every step of the process—from scripting and recording to editing and publishing. Blue allows me to automate these steps so that each task is assigned and tracked without manual intervention.

This ensures nothing falls through the cracks and that my workflow remains consistent and reliable.

Automated SOPs are also invaluable for onboarding new virtual assistants (VAs) and team members.

Instead of spending time training each new person individually, I can rely on these automated processes to guide them through their tasks. This not only saves me time but also ensures that everyone is following the same procedures, maintaining high standards across the board.

In addition, automation in Blue helps with project management, client follow-ups, and even marketing campaigns.

For example, I use Blue to automate email sequences for nurturing leads and engaging with my audience, which allows me to maintain regular communication without having to send each email manually.

Overall, the automation capabilities in Blue are a game-changer for solopreneurs. They enable you to focus on high-level strategic work while the routine, repetitive tasks are handled automatically, boosting your productivity and allowing you to scale your business more effectively.

**What would be your dream new feature for Blue?**

There are two features I’d love to see integrated into Blue.

First, the ability to upload videos and leave annotations directly on them.

Currently, I use Markup.io for this purpose, but having this functionality within Blue would streamline my workflow significantly. It would be incredibly helpful for providing feedback on video content, collaborating with my team, and ensuring that everyone is on the same page without needing to switch between different tools.

The second feature I dream of is the ability to leave voice notes that automatically transcribe.

This would be a game-changer for capturing ideas, providing instructions, and giving feedback quickly and efficiently. Transcribed voice notes would save time and ensure that important information is documented and easily searchable. It would also enhance communication within the team, especially for those who prefer speaking over typing.

These features would greatly enhance Blue's usability and make it an even more powerful tool for solopreneurs and teams alike, streamlining processes and improving overall efficiency.

**All right, we will take a look at those potential features and see what we can do with our roadmap!**

Thanks! I really appreciate you considering those features!

**Have you noticed some of your customers using Blue?**

Yes, dozens! The beauty of Blue lies in its intuitive interface and user-friendly design.

One of the biggest barriers to adopting new software is the learning curve, and Blue effectively eliminates this hurdle.

My customers, who range from coaches and course creators to freelance service providers, often don't have the luxury of time to sit through lengthy tutorials or training sessions. They need tools that they can pick up and start using immediately, and Blue fits this need perfectly.

In my experience, the simpler and more intuitive a tool is, the more likely people are to actually use it and integrate it into their daily workflows.

With Blue, my clients can dive right in, explore the features, and start leveraging the platform to enhance their productivity without feeling overwhelmed. This has been a significant factor in why so many of my customers have adopted Blue.

They appreciate that they can get up and running quickly, focusing on their core business activities rather than struggling with complicated software.

**Any parting words?**

Yes, I have a full course that details how I use Blue in my business, and it’s available as part of my membership program. This course covers everything from setting up your workflows to automating tasks and optimizing your processes using Blue.

It’s designed to help you get the most out of the platform and see tangible results in your business operations.

I am happy to share this free of charge to existing Blue customers!

**That’s awesome! If you’re reading this and you’re an existing Blue customer, feel free to email help@blue.cc to request access free of charge. Thank you for your time, Eran!**

It was a pleasure!
