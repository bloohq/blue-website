---
title: How to Write an Outstanding RFP
slug: how-to-write-an-outstanding-rfp
tags: ["insights", "modern-work-practices"]
description: Requests for Proposals (RFPs) are key tools that businesses and organizations of all sizes use to choose the best partners for any tasks and projects.
image: /resources/whiteboard-brainstorm.jpeg
date: 06/07/2024
showdate: true
sitemap:
  loc: /resources/how-to-write-an-outstanding-rfp
  lastmod: 2021/07/06
  changefreq: monthly
---

Requests for Proposals (RFPs) are a key tool that businesses and organizations of all sizes can use to ensure that they choose the best partners for any tasks and projects that are not handled in-house.

The upside of a correctly structured RFP is that it reduces risk during the buying process and creates a clear platform to evaluate the different choices available. From a seller's perspective, it gives a straightforward understanding of the buyer's needs and objectives and ensures that the typically time-consuming and frustrating back-and-forth negotiations are minimized.

Before Blue, we ran an agency and we have responded to hundreds of RFPs sent to us by everyone from government organizations, NGOs, and large multinational conglomerates to startups and smaller local firms. Alongside this, we've also helped clients navigate complex, mission-critical buying decisions with millions of dollars at stake.

This guide is based on these experiences, with the aim to:

* Create a compelling business case on why taking the time to compose a thorough RFP is almost always a smart choice.
* Explain to our existing client base the process they should go through when creating an RFP.
* Provide our current and future suppliers and partners an insight into how we make purchase decisions.

## Why RFPs?

The core idea behind having a structured buying process with an RFP vs. a more casual buying process is to *make better-quality decisions.*

**That's it.**

The reason why RFPs lead to better decision-making when choosing between various services and products is that it forces the buying organization to think through their goals and needs and gives them an objective way to compare multiple alternatives.

Having several vendors working with precisely the same information allows the buyer to make an "apples to apples" comparison — unlike an unstructured process where each vendor simply proposes their solution or product based on a loose understanding of the requirements.

## The Structure of an RFP.

A well-structured RFP will do the following:

* Clearly state what's required, as well as what's not required.
*  Provide a potential vendor with an unambiguous understanding of the objectives of the purchase and the key successful outcomes.
*  Answer frequently asked questions.
*  Give a clear indication of the evaluation criteria and the timelines of the buying process so that no one's time is wasted.

Our recommended structure for buying professional services and achieving the above objectives is as follows:

**Introduction.**

* About the Company. Provide a brief overview of your company, including geographic area, key services/products, history, etc.
* Purpose & Objectives of RFP. State why the RFP is being sent.
* Key Stakeholders. Give a list of stakeholders that will be evaluating the RFP, and note if there are any final decision-makers.
* Timelines & Submissions. List all the important tasks, dates, and times by which certain actions must take place or be completed.
* Evaluation Criteria. Provide the list of criteria that you will use to evaluate proposals and how you plan to weigh each component.

**Project Overview.**

*  Scope of Work. List, at a high level, the types of activities you expect from the vendor.
*  Key Requirements. List, in as much detail as possible, the requirements for the project.
*  Out of Scope. Be explicit in what you expect the vendor not to do.
*  Other Considerations. Provide other considerations in regard to the project.
*  FAQ. If during internal discussions there are questions that often come up, list them here.

**Proposal Format & Guidelines.**

‍This section should state the precise format and composition you expect to receive in the proposal from the vendors.

## The Buying Process.

- **Create the RFP.** Make sure that everyone who's involved in the project is aware that this document is being created, and solicit their opinions on key points.
- **Select vendors for initial evaluation.** Ask partner organizations and friends, and do your research on both local and international options. Where possible, try to include a "black horse": that is, not a partner you would consider on the first review, but one who may be able to offer an interesting angle, perspective, or solution for your project.
- **Shortlist vendors to send the RFP to.** Reach out to each vendor and ask for the company credentials. Note key details, such as how quickly they respond, the quality of the presentations, and the type of questions they ask during the initial call or meeting. Once you've shortlisted the vendors, send the RFP out.
- **Q&A sessions.** Invite vendors to send questions concerning the proposal or set calls/meetings to address complex points. Always keep detailed notes of questions. Our suggestion is to have an online hosted document (like a Google Doc) shared with all vendors that you update with all the questions and answers so that everyone is working on their proposals with the same shared understanding.
- **Review draft proposal (optional).** If you have the internal capacity, invite the vendors to send you a draft proposal one week prior to the final submission. It's a good sign if they do take you up on the offer. Send back the proposal with comments within 24 to 48 hours.
- **Score final proposals.** Once you've received the final proposals, review each one carefully and score each vendor according to the evaluation criteria. Where possible, allow the vendors to also present their proposals via conference call or an in-person meeting.
- **Notify everyone.** Once you've finalized your decision and have had senior stakeholder approval, notify all vendors of the results. For the losing vendors, consider either a follow-up call or meeting to explain why they didn't win, how they ranked compared to other vendors, and how they could improve in future bids. This is extremely appreciated by vendors and ends the relationship on a positive note — you never know if you may find yourself working with them on a future project.
- **Start the project.** Once you've got your ideal partner for the project, it's time to kick things off and focus on the execution.

## FAQs on RFPs.

**Who should write the RFP?**

Ideally, it should be whoever will be managing the relationship with the chosen vendor or partner. RFPs are best when written by one individual: while the other team members can review and comment on various versions of the document, it's important that one person takes full accountability and responsibility for the quality and accuracy of the RFP. If the team does not have experience, it's a very technical project, or the consensus is that an objective third party would be more suitable, then you can contract specialist consultants like Mäd to guide the buying process.

**What are typical timelines for RFPs?**

Again, this depends on the industry you're in and the complexity of the proposed project. Generally speaking, you should set a final response timeline of 2 to 4 weeks from the day the proposal is sent.

![](/resources/rfp-timelines.png)

**Should you accept draft proposals for feedback?**

Allowing vendors to send draft proposals that will be returned with comments ensures that the final proposals are of a significantly higher quality. However, depending on the number of vendors pitching, this can significantly increase the workload of the buying team. If they don't have the time available to fully review draft proposals and give meaningful feedback, then it's not worth adding this step to the process.

**How many vendors to select?**

This is also linked to the previous point about the buying team workload. The more vendors you ask to reply to the RFP, the more work you'll have answering questions, having calls and meetings with them, and evaluating the final proposals. Also, smart vendors will often ask how many other vendors are bidding and will often decline to bid if the number is too high, as the risk-reward ratio of the time spent on the proposal vs. the chances of winning does not make sense. Typically, 3 to 5 vendors is an appropriate number, but in larger organizations, you may want to check with the procurement department for any specific rules, as they may have a specific minimum number of bidders required for compliance purposes.

**Should I include a budget?**

This is a divisive topic, with several good arguments on either side. Including an indicative budget range or maximum budget is positive because it will instantly allow vendors to self-select in regards to deciding to bid or not, and it can also save you and your team time as you won't be evaluating vendors whose final proposed pricing is significantly above your maximum budget. However, if this is a complex purchase (i.e. custom software development) or a type of project that is best suited to a time and materials engagement model vs. a fixed price one, then it may not be a good idea to include any specific budget. This is also the case if this is the first time that you're purchasing this type of service, you may not have enough information or experience to accurately set a budget, and you can allow the various vendor proposals to give you a better understanding of market pricing.

**What should be in the evaluation criteria?**

This depends on what you value most from an ideal vendor. If the project is in a rush, then give more weight to the timelines and the availability for the vendor to start immediately. If it's a more long-term strategic project, then you should value prior experience, team CVs, and case studies more strongly. You can see two examples below that have different criteria and weights:

![](/resources/rfp-criteria1.png)

![](/resources/rfp-criteria2.png)

**Who should be involved in the scoring process?**

Everyone who was involved in the buying process from the start and had conversations and meetings with the vendors. Senior stakeholders who were not involved deeply in the process can also be invited to evaluate the proposal and score it, or they may just wish to review the final scoring to help them make the final decision. Additionally, both external and internal subject matter experts can be recruited to help with the scoring. For instance, the MarComms team may ask a member of the IT department to help in evaluating CRM software choices.

**How to score the vendors?**

For each evaluation criterion, you should give a score ranging from 0 to the maximum allowed for that particular criterion. Each individual that is involved in the buying process should evaluate each vendor privately, without sharing their scoring with any other team members. Then, the buying team should share their scoring simultaneously and take an average of the total score to give each vendor a final grand score based on all the evaluation criteria. You can then rank the vendors by scoring.

## Conclusion.

We hope that this guide has been useful in understanding the importance of a well-structured RFP in aiding key buying decisions. In the modern business world, it is rare for any organization to work in isolation, so being able to consistently pick highly effective, specialist partners is more crucial now than ever before.

