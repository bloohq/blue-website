---
title: Push vs Pull Workflows in Kanban
slug: push-vs-pull-kanban
tags: ["insights"]
description: Learn the difference between push workflows and pull workflows.
image: /resources/custom-permissons-background.png
date: 05/08/2024
showdate: true
sitemap:
  loc: /resources/push-vs-pull-kanban
  lastmod: 2024/08/05
  changefreq: monthly
---

How often do organizations and teams *consciously* think about how they work? 

More often than not, it just happens to be "the way it is done around here."

But what is the likelihood that whichever default work style a team has landed on is the best way of working?

It's likely they are working in a suboptimal way. This is why it is so important to *think* about *how* you work, not just the actual work itself.

There are two different ways of assigning work and getting things done: push and pull.

A push workflow involves tasks being assigned and distributed to team members by a central authority or manager. Tasks are pushed to individuals regardless of their current workload, often leading to overloading and inefficiencies. This approach emphasizes a top-down management style, where decision-making is centralized and team members have limited control over their work.

A pull workflow allows team members to take on tasks as they have the capacity, pulling work from a prioritized queue when they are ready. This method promotes a self-managed, decentralized approach where individuals have greater autonomy and responsibility. Pull workflows are designed to balance workloads, reduce bottlenecks, and increase overall efficiency and team morale.

In the end, this is about power dynamics and responsibility. 

Does the project manager have all the power to decide who works on what, when? 

Or do team members have the power to start working on something new based on the information that they have at hand?

If you have the right people on the team, moving to a pull-based system empowers those people to make better and faster decisions than anyone else. 

That's because they are the ones with the first-hand knowledge of the work at hand. 

They are not reading reports or getting second-hand data; they know *precisely* what is going on.

As long as they have the right organizational context, you can trust them to make the decision. 

After all, if you are trusting them with *doing* the work, you should also trust them on *what* to work on next. Your job as a manager is to provide all the tools and context required to make the best choice and to provide a healthy backlog of items to pick from.

That's it.

We often see organizations implementing a kanban board, but not the cultural shift required to work in the Kanban way. [There are various common problems in implementing Kanban](/resources/kanban-boards-common-challenges), but in this case, all that these organizations have done is simply create a good-looking todo list.

Organizational change goes much further than simply implementing better software, although it's a good start.