---
title: The History of Kanban Boards
slug: kanban-board-history
tags: ["insights"]
description: Learn the history of Kanban Boards, and how they evolved from factory floors into flexible digital tools. 
image: /resources/japan-kanban-boards.jpg
date: 23/07/2024
showdate: true
sitemap:
  loc: /resources/kanban-board-history
  lastmod: 2024/07/23
  changefreq: monthly
---
At Blue, we love [Kanban boards](/platform/kanban-boards). 

They are our primary way of managing our work, whether that's building our platform, managing our marketing strategy, and even our hiring pipeline! We believe that almost any process can be turned into an easy-to-use Kanban board that provides a clear visualization of the current state of work.

![](/product/dark-cta-screenshot.png)

But let's take a step back — what exactly *is* a Kanban board? 

It's simple, it's a board (digital or physical!) that has two key elements:

1. **[Lists (or Columns)](https://documentation.blue.cc/records/lists)**: These represent the different stages in your workflow. For example, a basic Kanban board might have lists labeled "To Do," "In Progress," and "Done."
2. **[Cards](/platform/records)**: These represent individual tasks or work items. Each card typically contains information about the task, such as its description, who's responsible for it, and its due date.

Optionally, Kanban boards can have Work-in-Progress (WIP) Limits, so each list may have a limit on how many cards it can contain at one time. This helps prevent overload and keeps work flowing smoothly.

The beauty of a Kanban board lies in its visual nature.

At a glance, you can see the status of all your tasks, where bottlenecks might be forming, and what needs attention next. It's a dynamic tool that evolves as work progresses - cards move from left to right across the board as tasks advance from start to completion.

But Kanban boards are more than just a visual task list. They embody a philosophy of continuous flow and incremental improvement. By making work visible and limiting work-in-progress, Kanban encourages teams to complete current tasks before starting new ones, reduce multitasking, and identify process improvements.

Whether you're managing a software development project, a marketing campaign, or even your personal to-do list, a Kanban board can help you visualize your workflow, balance your workload, and boost your productivity. It's a versatile tool that can be as simple or as complex as your process requires.

---

Today, we're going to doing a deep-dive into the history of Kanban. 

Originating from Japanese manufacturing, the word "Kanban" literally translates to "visual signal" or "card." Over the decades, it has evolved from its roots in factory floors to become a versatile methodology applied across various industries and knowledge work sectors.

At its core, Kanban is built on the principles of visualizing work, limiting work in progress, and fostering continuous improvement. However, one of its most powerful yet often overlooked benefits is its ability to create a self-propelling workflow. As the CEO of Blue notes:

> Setting up a proper Kanban structure ensures that there is less need for a project manager, although their role remains crucial.

The significance of Kanban in modern project management cannot be overstated. It has become such an integral part of workflow optimization that you should not even consider a project management software that does not include this view.

Kanban transforms project management from what could seem like a massive, overwhelming to-do list into a dynamic, prioritized activity. This visual approach allows small teams to accomplish great things by forcing prioritization and providing instant clarity on work status.

Despite its widespread adoption, Kanban is not without its misconceptions. Some erroneously believe it's difficult to set up or not powerful enough for *"serious work"* that always requires long written plans and [Gantt Charts](/platform/timeline-gantt-charts). In reality, Kanban's simplicity *is* one of its greatest strengths, making it adaptable to various scales and complexities of work.

For many leaders, Kanban brings a sense of calm and control. One CEO that uses Blue reported:

> "I can go to any board and instantly see what's in progress, under review, and yet to start. It means less pinging my team for updates." 

This efficiency and transparency demonstrate why Kanban has become a cornerstone of agile project management, evolving far beyond its manufacturing origins to help teams across the globe visualize, optimize, and accelerate their work.

As we delve into the rich history of Kanban, we'll explore its journey from Toyota production lines to digital kanban boards, and understand how its principles have stood the test of time while adapting to the ever-changing landscape of work management.

## The Origins of Kanban

The story of Kanban begins in the bustling factories of post-World War II Japan, specifically at Toyota Motor Corporation. In the late 1940s, a young industrial engineer named [Taiichi Ohno](https://en.wikipedia.org/wiki/Taiichi_Ohno) was tasked with improving Toyota's manufacturing efficiency. 

![](/resources/taiichi-ohno.jpg)

Ohno identified seven sources of waste in production, which are now recognized as crucial in [lean manufacturing](https://en.wikipedia.org/wiki/Lean_manufacturing):

1. Delay, waiting or time spent in a queue with no value being added
2. Producing more than you need
3. Over processing or undertaking non-value added activity
4. Transportation
5. Unnecessary movement or motion
6. Inventory
7. Defects in the Product

We've found an interesting 5-minute interview that's worth watching:

<video controls>
  <source src="/resources/taiichi-ohno-video-interview.mp4" type="video/mp4">
</video>



Ohno is also known for his "Ten Precepts" to think and act to win:

1. You are a cost. First reduce waste.
2. First say, "I can do it." And try before everything.
3. The workplace is a teacher. You can find answers only in the workplace.
4. Do anything immediately. Starting something right now is the only way to win.
5. Once you start something, persevere with it. Do not give up until you finish it.
6. Explain difficult things in an easy-to-understand manner. Repeat things that are easy to understand.
7. Waste is hidden. Do not hide it. Make problems visible.
8. Valueless motions are equal to shortening one's life.
9. Re-improve what was improved for further improvement.
10. Wisdom is given equally to everybody. The point is whether one can exercise it.

Little did he know that his innovations would not only revolutionize Toyota but also transform production systems worldwide.

![](/resources/japan-kanban-boards.jpg)

Ohno's inspiration came from an unlikely source: **American supermarkets.**

He observed how customers would take only what they needed from the shelves, and the shelves would be restocked based on what was taken. This simple yet effective system became the foundation for what we now know as Kanban.

To understand the revolutionary nature of Ohno's approach, we need to first grasp the concept of "push" versus "pull" systems in manufacturing.

- **Push manufacturing**: A production system where goods are manufactured based on forecasted demand and then "pushed" to the market.
- **Pull manufacturing**: A production system where goods are produced in response to *actual* customer demand.

Let's dig a little deeper into both of these approaches! 

## Push System (Traditional Approach)

Imagine a restaurant where the chef, hearing that Saturdays are usually busy, decides to prepare for the worst-case scenario every single day. This approach, mirroring a push system in manufacturing, leads to a host of problems that ripple through the entire restaurant operation.

Every morning, the chef arrives early and starts preparing 100 meals, including appetizers, main courses, and desserts. This happens regardless of reservations or weather conditions that might affect customer turnout. On a typical weekday, the restaurant might only serve 30-40 meals, meaning that 60-70 prepared meals go to waste. Fresh ingredients spoil, and prepared dishes have to be thrown away, leading to significant financial losses.

To avoid waste, the chef might try to save some dishes for the next day. However, this compromises the quality and freshness of the food, potentially leading to customer dissatisfaction. The kitchen becomes cluttered with excess prepared food, making it difficult for the staff to work efficiently. Extra refrigerators are needed to store the overflow, increasing electricity costs and further straining the restaurant's resources.

This push system creates inflexibility in the restaurant's operations. If there's a sudden change in customer preferences or a new food trend emerges, the chef can't quickly adapt. The kitchen is stuck with pre-prepared meals that may no longer be in demand. This lack of agility can severely impact the restaurant's ability to stay competitive in a dynamic market.

The overproduction also complicates the supply chain. Because the chef always prepares for maximum capacity, ingredients are ordered in bulk. This leads to a need for larger storage spaces, increased risk of ingredients spoiling before use, and inflexibility in adapting to price fluctuations in the market. It can also strain relationships with suppliers who must always deliver maximum quantities, even when it's not necessary.

The constant overproduction puts a significant strain on the kitchen staff. They're always working at maximum capacity, preparing meals that often go to waste. This can lead to frustration and burnout, potentially increasing staff turnover. Ironically, despite all the preparation, customers might still have a subpar experience. On genuinely busy days, the staff might be too exhausted from constant overproduction to handle the rush effectively.

Financially, this approach is unsustainable. The restaurant's money is tied up in excess inventory, both in ingredients and prepared meals. This reduces cash flow and makes it difficult for the restaurant to invest in improvements or weather slow periods.

Perhaps most critically, this push system **disconnects the chef from actual customer demand.** 

So focused on pushing out meals, the chef misses opportunities to innovate or adjust the menu based on customer feedback. This disconnect can lead to a gradual decline in customer satisfaction and loyalty.

This push system in the restaurant mirrors the problems faced by traditional manufacturing approaches. Just as the chef prepares meals based on maximum forecasted demand, factories would produce parts based on projected maximum sales, leading to excess inventory, waste, and inflexibility. 

The challenges faced by the restaurant - waste, quality issues, inflexibility, and financial strain - are the same issues that led innovators like Taiichi Ohno to develop the pull system and Just-In-Time manufacturing principles, aiming to align production closely with actual demand.

## Pull System (Kanban Approach)

In contrast to the push system, imagine a restaurant that operates on a pull system, mirroring the Kanban approach in manufacturing. Here, the chef runs the kitchen with a focus on responding to actual demand rather than anticipating it.

The day at this restaurant starts with minimal prep work. The chef and kitchen staff prepare only a small amount of basics - chopped vegetables, basic sauces, and par-cooked ingredients. The real cooking begins when a customer places an order, much like a Kanban card signaling demand in a factory.

When an order comes in, it triggers a series of actions in the kitchen. The chef starts preparing that specific dish, and simultaneously, this 'pull' ripples back through the kitchen's supply chain. If preparing the dish uses up a certain ingredient, it signals the need to prep more of that item, ensuring a steady but lean supply.

This approach brings numerous benefits to the restaurant. Firstly, it dramatically reduces waste. Since meals are only prepared in response to actual orders, there's little to no excess food at the end of the day. This not only saves money on ingredients but also reduces the need for extensive storage space and energy for refrigeration.

Quality also improves under this system. Every dish is freshly prepared, ensuring that customers always receive food at its best. The chef can easily adapt to customer requests or dietary requirements, as each dish is made to order. This flexibility extends to the menu as well - if a dish isn't selling well, it can be quickly removed or modified without wasting pre-prepared ingredients.

The pull system also allows the restaurant to be more responsive to unexpected changes. If there's a sudden rush of customers, the kitchen might be briefly overwhelmed, but it won't run out of supplies as it would in a push system where all meals are pre-prepared. Conversely, on slow days, the kitchen doesn't waste resources preparing unneeded meals.

Financially, this approach is much more efficient. The restaurant's money isn't tied up in excess inventory or wasted on discarded food. Instead, resources are used precisely where and when they're needed. This improved cash flow allows the restaurant to invest in quality ingredients, staff training, or kitchen improvements.
The staff benefits too. Rather than the constant high-pressure environment of the push system, the workload in a pull system ebbs and flows with customer demand. This can lead to a more balanced and less stressful work environment, potentially improving staff satisfaction and retention.

Perhaps most importantly, this system keeps the chef and staff closely attuned to customer preferences. They get immediate feedback on which dishes are popular and can quickly adjust. If a new food trend emerges, the restaurant can adapt its menu almost immediately, without worrying about using up a backlog of pre-prepared meals.

This pull system in the restaurant clearly illustrates the principles of Kanban in action. Just as the chef prepares meals in response to actual orders, a Kanban system in manufacturing produces parts only when they're needed downstream. The benefits seen in the restaurant - reduced waste, improved quality, greater flexibility, and better financial performance - are the same advantages that made Kanban revolutionize manufacturing and, later, knowledge work.

In essence, the pull system transforms the restaurant from a rigid, forecast-driven operation into a flexible, demand-driven one. It embodies the core principles of Kanban: visualizing work (through orders), limiting work in progress (by only preparing what's needed), and enabling a continuous flow (of meals to customers). This approach, whether in a restaurant or a factory, allows for a more efficient, responsive, and customer-focused operation.

## Kanban at Toyota

Ohno implemented this pull system at Toyota using visual cards, or "kanban" in Japanese. These cards acted like a customer's order in our restaurant analogy. When a part was used on the production line, its kanban card would be sent back to the supply area, signaling the need for more of that part. This simple yet effective method ensured that parts were only produced when needed – a principle known as Just-In-Time (JIT) manufacturing.

The most revolutionary aspect of Ohno's system was its ability to dramatically reduce waste while improving efficiency. In post-war Japan, resources were scarce, and this system allowed Toyota to do more with less. It minimized overproduction, reduced inventory costs, and improved overall quality control.

This approach, which became known as the Toyota Production System (TPS), was a key factor in Toyota's rise to become one of the world's most respected car manufacturers. Today, Toyota is renowned not just for the quality and reliability of its vehicles, but also for its lean and efficient manufacturing processes.

The cultural context of post-war Japan played a significant role in the development of TPS. The scarcity of resources, coupled with a cultural emphasis on efficiency and continuous improvement (known as "kaizen" in Japanese), created the perfect environment for Ohno's innovations to take root and flourish.

As we'll see in the following sections, the principles of Kanban that Ohno developed at Toyota would eventually transcend the world of car manufacturing, inspiring efficiency improvements across various industries and even in our daily lives.

## Key Elements of Early Kanban

The early Kanban system, as developed by Toyota, was elegantly simple yet remarkably effective. It revolved around three core elements that worked in harmony to create a lean, efficient production process. These elements - visual signals, work-in-progress limits, and continuous flow - formed the foundation of Kanban and continue to be central to its modern applications.

### 1. Visual Cards/Signals

At the heart of the Kanban system were the visual cards, or "kanban" in Japanese. These were physical cards that acted as a signal to trigger action. In Toyota's factories, these cards were used to indicate when more parts were needed at a workstation.

Imagine a simple system with three boxes: Box A is at the assembly line, Box B is in a nearby storage area, and Box C is at the supplier's facility. When Box A empties, a worker would send its kanban card to Box B, signaling the need for a refill. As Box B is emptied to refill A, its card would be sent to Box C, triggering a new order from the supplier.

This visual system made the flow of work immediately apparent to everyone. At a glance, managers and workers could see what was needed, where, and when. It eliminated the need for complex scheduling systems and reduced the risk of overproduction or shortages.

### 2. Work In Progress (WIP) Limits

A crucial aspect of Kanban was the strict limit on work-in-progress. Each stage of the production process had a cap on how many items could be in that stage at any given time. This limit was often determined by the number of kanban cards in circulation for each part.

To understand this, let's return to our box example. If there were only five kanban cards for a particular part, then there could only ever be five boxes of that part in the system at once. This limit prevented overproduction and reduced inventory costs.

WIP limits also helped identify bottlenecks quickly. If parts started piling up at one stage of production, it was immediately clear that this stage was a bottleneck, allowing managers to address the issue promptly.

### 3. Continous Flow

The ultimate goal of Kanban was to create a smooth, continuous flow of work through the production system. By using visual signals and WIP limits, Toyota was able to move away from batch production towards a more fluid, ongoing process.

In a continuous flow system, work moves forward as soon as it's ready, rather than waiting for a large batch to be completed. This reduces wait times, decreases inventory, and allows for quicker identification and resolution of quality issues.

Think of it like a relay race, where each runner (or workstation) smoothly passes the baton (or product) to the next, maintaining a steady pace throughout. This contrasts with a batch system, which would be more like having each runner complete multiple laps before passing all their batons at once to the next runner.

These three elements - visual signals, WIP limits, and continuous flow - worked together to create a system that was self-regulating and highly efficient. Visual signals made the state of work clear to all, WIP limits prevented overload and highlighted problems, and the focus on continuous flow kept everything moving smoothly.

The genius of Kanban lay in its simplicity. 

By implementing these straightforward principles, Toyota was able to dramatically improve its production efficiency, reduce waste, and increase quality. As we'll see in later sections, these same principles would prove to be highly adaptable, allowing Kanban to evolve beyond the factory floor to a wide range of industries and applications.

## Spread to Other Industries

The success of Kanban at Toyota did not go unnoticed. As news of Toyota's remarkable efficiency and quality improvements spread, other manufacturers began to take notice and adopt similar practices. This marked the beginning of Kanban's journey beyond the automotive industry and its eventual global adoption.

### Japanese Manufacturing Adoption

Initially, the adoption of Kanban principles spread among other Japanese manufacturers. Companies in electronics, appliances, and other industrial sectors began to implement their own versions of the system. This spread was facilitated by Japan's close-knit business culture, where practices often diffused through networks of affiliated companies and suppliers.

For instance, companies like Sony and Panasonic (then Matsushita) began implementing Kanban-style systems in their electronics production lines. They found that the principles of visual management and pull production were equally effective in managing the complex supply chains of consumer electronics as they were in automotive manufacturing.

### Western Manufacturing Expansion

The expansion of Kanban to Western manufacturing began in earnest during the 1970s and 1980s. This period saw increasing competition from Japanese manufacturers in global markets, prompting Western companies to examine and adopt Japanese manufacturing techniques.

Several factors contributed to this expansion:

- **Economic Pressures:** The oil crises of the 1970s and increasing global competition put pressure on Western manufacturers to improve efficiency and reduce costs.
- **Academic Interest:** Researchers and business scholars began studying Japanese manufacturing techniques, publishing influential works that brought concepts like Kanban to a wider audience.
- **Consultants and Training:** Japanese experts, along with Western consultants who had studied these methods, began offering training and implementation services to Western companies.
- **Success Stories:** Early adopters in the West who successfully implemented Kanban and related lean manufacturing techniques served as powerful case studies, encouraging others to follow suit.

One notable early Western adopter was General Electric. Under the leadership of CEO Jack Welch in the 1980s, GE began implementing lean manufacturing principles, including Kanban systems, across its diverse range of products. This high-profile adoption helped further legitimize these techniques in Western business circles.

### Challenges and Adaptations

The adoption of Kanban in Western manufacturing was not without challenges. Cultural differences, existing management structures, and different labor relations all posed obstacles. Many companies found that they needed to adapt the Kanban system to fit their specific contexts.

For example, some Western manufacturers developed hybrid systems that combined elements of Kanban with their existing production planning methods. Others focused more on the visual management aspects of Kanban, creating elaborate systems of color-coded cards and boards to manage workflow.

### Beyond Traditional Manufacturing

As Kanban proved its versatility, it began to spread beyond traditional manufacturing settings. Companies in industries such as pharmaceuticals, food production, and even some service industries began experimenting with Kanban principles.

For instance, hospitals started using Kanban systems to manage medical supplies, ensuring that stocks were replenished based on actual usage rather than arbitrary schedules. This helped reduce waste and ensure that critical supplies were always available when needed.

By the 1990s, Kanban had become a fundamental part of lean manufacturing philosophy, which was being taught in business schools and implemented in companies worldwide. Its principles of visual management, pull production, and continuous flow had proven to be universally applicable, transcending cultural and industry boundaries.

The spread of Kanban to other manufacturing industries laid the groundwork for its eventual leap into knowledge work and software development, which we'll explore in the next section. This expansion demonstrated Kanban's adaptability and set the stage for its evolution into a versatile management tool applicable far beyond its origins on the Toyota factory floor.

## Adaptation to Software Development

While Kanban had proven its worth in manufacturing, its journey was far from over. The early 2000s saw a revolutionary adaptation of Kanban principles to the world of software development and knowledge work, opening up entirely new avenues for the methodology.

The pivotal moment in Kanban's transition to software development came in 2004 when David J. Anderson, a software developer and management consultant, applied Kanban principles to an IT work team at Microsoft. Anderson was faced with a struggling software team that was overwhelmed with work and failing to meet deadlines. Drawing inspiration from manufacturing Kanban, he developed a system to visualize the team's workflow, limit work in progress, and improve the flow of tasks.

Anderson's experiment was a success. The team saw dramatic [improvements in productivity and morale](/resources/great-teamwork). This marked the birth of what would come to be known as "Kanban for knowledge work" or "Software Kanban."

###  Key Differences from Manufacturing Kanban
While the core principles remained the same, Software Kanban had to be adapted to suit the unique characteristics of knowledge work:

* **Intangible Work Items**: Unlike physical parts in manufacturing, software tasks are intangible. The "cards" in Software Kanban represent user stories, bugs, or other units of work.
* **Variable Task Sizes**: In manufacturing, items tend to have consistent processing times. In software, tasks can vary greatly in complexity and time required.
* **Non-Linear Workflow**: Software development often involves feedback loops and rework, unlike the typically linear flow in manufacturing.
* **Emphasis on Collaboration**: Software Kanban puts a greater emphasis on team collaboration and continuous improvement through regular meetings and reviews.
* **Flexible WIP Limits**: While manufacturing Kanban often has fixed WIP limits, Software Kanban allows for more flexible limits that can be adjusted based on team capacity and project needs.

## Kanban in Project Management


A significant milestone in the spread of Kanban in software development and beyond was the launch of [Trello](/alternatives/trello) in 2011. Trello, a [web-based project management platform](/solutions/project-management), popularized the concept of [digital Kanban boards](/platform/kanban-boards) for a wide audience.

Trello's simple interface made Kanban accessible to non-technical users. It allowed anyone to create boards with columns representing workflow stages, and cards representing tasks that could be moved between these columns. This intuitive representation of Kanban principles helped spread the methodology beyond software development into general project management, marketing, education, and personal productivity.

![](/resources/trello-card.png)

The success of Trello sparked a wave of similar tools, such as [Blue](/),  and soon, Kanban board features became a standard offering in many [project management software solutions](/solutions/project-management):

![](/resources/blue-card.png)

This digital adaptation made it easier than ever for teams of all sizes and industries to implement Kanban principles in their work.

As Kanban continued to evolve in the software world, it began to influence project management practices across various industries. [Even non-project managers started to become project managers!](/resources/project-management-basics-guide)

The visual nature of Kanban boards, whether physical or digital, proved to be a powerful tool for managing workflow and improving productivity in almost any context where work needs to be organized and tracked.

## The Future of Kanban Boards

While in this article we have mostly focussed on the history of Kanban boards, it is also worth spending time to consider the future. 

In essence, asking *"what's next"*, especially at the intersection of project management and Kanban boards. 

At [Blue](/), we're actively building the future of Kanban boards, and this is why it's so important to understand their history. As we go to work on fulfilling out mission to build the [world's best project management software.](/solutions/project-management), we see a few trends coming in the future:

### AI Integration
AI promises to revolutionize the way we interact with and manage Kanban boards. Here are some of the ways we envision AI enhancing Kanban boards at Blue:

* **Automatic Monitoring**: AI can continuously monitor Kanban boards, identifying potential bottlenecks and inefficiencies. It can alert teams to these issues in real-time, allowing for swift corrective action.
* **Finding Duplicates**: In large Kanban boards, duplicate tasks can easily occur. AI can help by automatically detecting and flagging these duplicates, ensuring a more streamlined and organized workflow.
* **Auto-Categorizations**: AI can assist in categorizing tasks automatically based on their content and context, making it easier to manage large volumes of tasks and ensuring that everything is in its right place.
* **Summarizing Changes**: Keeping track of changes in Kanban boards over time can be challenging. AI can summarize these changes, providing clear reports on what has changed between specific dates, helping teams stay updated with minimal effort.

### Interconnected Kanban Boards
The future of Kanban boards includes a more interconnected approach, where a single item can be tracked across multiple boards, each managed by different teams. This interconnectedness ensures that:

- **Unified Data**: All teams work with the same data, preventing discrepancies and ensuring consistency across the organization.
- **On-Topic Conversations**: Discussions related to a specific task can happen in one place, regardless of which board the task appears on. This promotes better communication and collaboration.
- **Focused Workflows**: Each team can maintain its own workflow, focusing on its specific tasks while still having visibility into the larger picture. This ensures that while teams operate independently, they are aligned with the overall goals of the organization.

### Kanban of Kanban Boards
As organizations grow, the need to manage multiple projects and workstreams becomes crucial. The concept of a "Kanban of Kanban boards" addresses this need by:

- **Portfolio Management**: Tracking entire portfolios of projects in a Kanban format allows for high-level oversight and management. It helps in visualizing and managing the flow of work across multiple projects and teams.
- **Enhanced Visibility**: Leaders can get a comprehensive view of all ongoing projects, understanding their status, progress, and any potential issues. This holistic view aids in better decision-making and strategic planning.
- **Scalability**: This approach ensures that Kanban remains effective even as the number of projects and the scale of operations increase. It provides a structured yet flexible way to manage complex and large-scale initiatives.

So that's it! We hope you've enjoyed learning about the history of Kanban boards. If you're actively looking to implement a kanban board software in your team, [feel free to sign up to a free trial of Blue.](https://app.blue.cc) 

