---
title: The Brand Pyramid
slug: brand-pyramid
tags: ["insights"]
description: Discover why its important for brands to build their brands in a structured manner.
image: /resources/brand-pyramids.jpeg
date: 01/08/2024
showdate: false
sitemap:
  loc: /resources/brand-pyramid
  lastmod: 2024/08/01
  changefreq: monthly
---

This article comes from our previous experience in running a successful design agency. 

If you're an agency owner, you owe it to yourself to read our [Agency Success Playbook](/resources/agency-success-playbook). 

It distills a decade of agency leadership experience to help you grow your agency and learn to master cash-flow, project management, client relationships, proposal writing and more.

--- 
Over the years, the marketplace has moved from being company-centric to customer-centric. This has created the need for companies to build more meaningful brands that their customers can join and co-create.

Companies are now using various tools, like the brand pyramid, to 'humanize' their brands, making it easier for them to connect with their customers on an emotional level. It is these emotional connections that makes the customers feel as though they are part of the brand and that the brand part plays an important role in shaping their own identities and lives.

The primary function of a brand pyramid is to give the brand direction, allowing it to develop in a consistent and meaningful way. Traditional brand pyramids are made up of 5 sections, with each tier tapping into different aspects of the brand.

## A traditional brand pyramid

- **Essence**: The core value that drives the brand's performance
- **Brand Personality**: The manifestation of the brand in human characteristics
- **Emotional Benefits**: The emotion that results from interaction with the brand
- **Rational Benefits**: The benefits that distinguish the brand from its competitors
- **Functional Attributes**: The features, services, or goods that the brand provide

![](/resources/brand-pyramid-structure.png)

## The Blue Approach

When we consider brand pyramids, we take a slightly different approach with our brand. Due to similarities and overlaps of the rational and functional benefits, we have streamlined them into a single tier. This has helped us avoid confusion and focus on what really matters - HOW our brand is different to that of our competitors. To achieve this, it is important that at least one of the functional benefits stipulates the brand's differentiating feature.

![](/resources/blue-brand-pyramid.png)

## Brand Essence

The peak of the pyramid and the pinnacle of the brand is the brand essence. This is defined as the core value that drives the brand's performance. It is the key element that sets the brand apart from its competitors. Every business and design decision is made with the brand essence as the guiding factor.

Here are the brand essences of well known brands:

- Nike: Authentic athletic performance
- Walt Disney World: Magical
- BMW: Sheer driving pleasure
- Apple: Think different

The brand essence is often the most difficult tier of the brand pyramid to define. If need be, completing the rest of the brand pyramid and circling back to the brand essence will help inspire ideation.

## Brand Essence vs Slogan

It is important that the brand essence is not confused with the slogan. The difference between a brand's essence and its slogan is simple. The brand essence is used internally, and the slogan is customer facing.

The brand essence is a short, impactful value or statement that instantly communicates to any new employees what the brand is about while the slogan is a short, memorable phrase that is used to differentiate the brand in the minds of the its target audience.

Let's have a look at Nike to demonstrate the difference between a brand essence and slogan. Nike has taken the world by storm with their slogan, **Just Do It**, yet their internal brand essence is **Authentic Athlete Performance**. Nike wants their employees to work towards creating authentic athletic wear aimed at high performance, allowing their customers to Just Do it.


Blue's brand essence is **Organize The World's Work**. The core reason our team gets out of bed in the morning is to help our the world organize its work. Think of all the downstream benefits of more successful projects and streamlined processses! 

Our slogan, on the other hand, is **Teamwork, Made Simple™**. 

This being said, it is possible for some brands to use their brand essence as their slogan, if appropriate.

Unlike Nike and Blue, Apple's brand essence, Think Different, is also their slogan. Not only do they want their employees to think different about the technology they create, but they also want their customers to think different about the way they experience Apple's technology.

## Brand Personality

The second section of the brand pyramid is the brand's personality. This is the manifestation of the brand in human characteristics.

Examples of popular brand personality traits:

![](/resources/personality-traits.png)

Think about it like this: 

If your brand was to walk through the door, what would everyone in the room say about them? How would they be perceived? How would they behave? How would they interact with others?

Even though a brand may have a range of personality traits, it is important to define and focus on the most appropriate and impactful. We have found that sticking to three personality traits results in brands that are memorable and distinctive.

When looking at Blue's brand, the personality traits we chose to focus on are:

- Modern
- Trustworthy
- Innovative

These traits guide our approaches, processes, the way we interact with our clients, the content we create, and the choices we make when developing our platform. 

## Emotional Benefits

The next section is the emotional benefits. These are the emotions that your customers feel after each interaction with the brand. These interactions include every brand touchpoint, from your website and social media presence to your businesses cards, product packaging, and the way you interact with your customers.

The emotions we strive to elicit at Blue are:

- Assured
- Impressed
- Valued

Our clients feel assured that they are working on a platform that is crafted with care by experts, they are impressed with our thinking and executions and they feel as though their opinions, hopes, and dreams have been understood and valued.

It is important to ask yourself: How do you want your customers to feel?

> Your brand isn't what you say it is. It's what they say it is.

**Marty Neumeier**

## Functional Benefits

The last section of the brand pyramid is functional benefits. This can be defined as the tangible features, products and services of your brand. While the brand essence focuses on the soul of the brand, the functional benefits refers the actual benefits the distinguishes the brand from its competitors.

Blue has focused on 4 simple functional benefits that are reflected in our mission:

> Our mission is to organize the world's work by building the best project management platform on the planet—simple, powerful, flexible, and affordable for all.

1. Simplicty
2. Power
3. Flexibility
4. Affordability

Our strategic and purposeful approach allows us to execute high quality projects that achieve impressive results.

Our team members are experts in multiple design disciplines, allowing us to provide our clients a broader understanding of their goals and a more effective way of achieving them.

All of our internal admin and client projects are run on an easy to use project management software called Bloo. This allows us to spend less time on admin and more time delivering results. This also allows our clients to track their projects in real time while keeping all ideas, comments and files in one place.

## Conclusion

Customers are no longer only focused on a company's products and services, but on their purpose and values. Having a well crafted brand pyramid will allow brands to cultivate a space in which their customers can find meaning. Often, hyper-successful brands create a 'tribe-like' following, whereby customers buy into a brand so much that is sewn into the fabric of their own individualism.