---
title: How to use Kanban boards for project management
slug: kanban-boards-project-management
tags: ["insights"]
description: 
image: /resources/kanban-background.png
date: 10/08/2024
showdate: true
sitemap:
  loc: /resources/kanban-board-software-core-components
  lastmod: 2024/08/10
  changefreq: monthly
---

At Blue, it's no secret that we love [Kanban boards for project management.](/platform/kanban-boards)

We think that they are a fantastic way to manage the workflow of any project, and they help keep project managers and team members alike sane! 

For too long, we've all been using excel spreadsheets and todo lists to manage work. In fact, this is one of our [major criticisms of Basecamp](/alternatives/basecamp-alternative), another project management platform. Even they eventually added their own take on Kanban for their platform (warning: unfortunately they didn't do a great job, check our write up which includes a breakdown video!).



[Kanban boards have an interesting history](/resources/kanban-board-history). They originated in Japan on the factory floor, but now have become an indispensible tool that should be in every project manager's back pocket. 

![The original Japanese Kanban Boards](/resources/japan-kanban-boards.jpg)

[Trello](/alternatives/trello-alternative) was the software that first popularized kanban boards for project management. 

That said, you [can still buy physical kanban boards for your home or office!](https://www.patboard.com/)

So, before we jump in to how to use Kanban boards for project management, let's take a quick refresh on how they work. 

At its core, a Kanban board consists of columns representing stages of a process, such as "To Do," "In Progress," and "Done." Individual tasks are represented as cards that move from one column to the next, offering an immediate snapshot of the project's current status.

Kanban boards provide a clear visual representation of work items as they move through different stages of a workflow.

And it is this simplicity makes Kanban boards incredibly effective, transforming abstract concepts of "work" into tangible, manageable units. 

When it comes to project management, **this visual transparency is crucial.**

Kanban boards help team members spot bottlenecks, prioritize tasks, and allocate resources efficiently, increasing productivity and reducing errors and miscommunication. 

Their visual nature provides an intuitive way for non-technical stakeholders to track project progress, clarifying task status and deadlines, reducing the need for frequent updates and meetings, and promoting involvement, accountability, transparency, and continuous improvement.

[We've covered the core components of Kanban in our in-depth review](/resources/kanban-board-software-core-components), but it is worth refreshing our memories here as well. 

Let's go — the core components are: 

1. **Visualize Work**: The first step in Kanban is to create a visual representation of the tasks. This is typically done using a Kanban board, which delineates different stages of a workflow—for example, "To Do," "In Progress," and "Done." This visual approach makes it easy for all stakeholders, including non-technical team members, to understand the project's current state at a glance.
2. **Limit Work-in-Progress (WIP)**: One of Kanban's critical tenets is to limit the amount of work in progress. By doing so, teams can focus on completing tasks rather than juggling multiple incomplete ones. This helps reduce bottlenecks and ensures a smoother flow of work.
3. **Focus on Flow**: Kanban emphasizes the importance of maintaining a steady flow of work items. Teams strive to ensure that tasks move through the workflow as efficiently as possible. This focus on flow helps in identifying and addressing any interruptions or inefficiencies promptly.
4. **Continuous Improvement**: Also known as "Kaizen," this principle encourages ongoing evaluation and enhancement of processes. Kanban encourages teams to regularly review their workflows and identify areas for improvement, fostering an environment of continual growth and learning.

So now that we have a clear idea of what Kanban Boards are, let's discuss some of benefits of using Kanban Boards in Project Management, and after that we will dive into *how* to use them for project management. 

## Benefits of Using Kanban Boards in Project Management

As mentioned further up, at Blue we ❤️ Kanban Boards — this is why they are the default project view when someone first creates a project in Blue. We believe that, more often than not, Kanban Boards are the best way to manage project workf.

So why this love affair? 

Well, there are a lot of reasons, and it mostly comes down to being able to see the work that is happening more clearly, as well as creating a long-term cultural mindset shift about the very nature of work itself. 

### Kanban Boards Benefit 1: Improved Visibility

One of the standout advantages of using Kanban boards is the unparalleled level of visibility they provide. 

By displaying tasks and project statuses on a single board, all team members, including non-technical stakeholders, can immediately grasp the progress of ongoing projects. This transparency ensures everyone is on the same page, thus minimizing misunderstandings and miscommunication.

You don't have to *ask* anyone to know the latest status of the project – assuming everyone keeps the board up to date (more on that later!)

This idea of "putting everything in one place" is central to how successful teams adopt Kanban Boards in general, and [Blue](/) specifically. 

### Kanban Boards Benefit 2: Better Workflow Management

Kanban boards are also incredibly effective at managing workflows. By breaking down projects into manageable tasks and visual stages, teams can quickly identify bottlenecks and continually optimize their process. This agile approach ensures that work flows smoothly from inception to completion, reducing idle time and increasing productivity.

### Kanban Boards Benefit 3: Enhanced Collaboration

Collaboration is the cornerstone of successful [project management](/solutions/project-management) and [great teamwork](/resources/great-teamwork), and Kanban boards facilitate this by providing *the* space to collaborate.

Sure, there will still be team chat, some 1-2-1s, sticky notes on people's desk. But, the kanban board is where the *meaningful* updates go, and where you can expect to find shared information. 

It also means that *the* project plan is not some secret document that lives on an excel spreadsheet in the project managers laptop, ready to be whipped out at each monthly or quarterly review like a deadly weapon. 

You mean — we were late and we didn't even know it? We committed to something three months ago and we haven't even started working on it? 

Ouch. 

## Kanban Boards Benefit 4: Reduced Cognitive Load

This is often an under-appreciated benefit of using a [work-management platform.](/platform). Instead of having everyone keep everything in their head, and then having lots of meetings and "sync ups" to keep up to date, everyone can sleep well at night knowing that everything is tracked. 

Everything is documented, allowing team members to focus on the work itself rather than worrying about tracking it.

It also creates this cultural shift towards continuous improvement. Management realize that projects are not just "one big push", but actually the sum of hundreds or perhaps even thousands of decisions and tasks. 

And there is also an implicit move towards having work-in-progress limits. After all, if you see 200 items that are currently "in progress", this is meaningless. *Nobody* is working on 200 things at the same time, which then begs the question of what is *actually* being worked on, and what is blocked by other dependencies, lack of approval, or unclear requirements. 

## How to use Kanban Boards for Project Management

Ok, we are finally here.

We're going to tell you a secret. *There is no real secret* to using Kanban Boards for project management. 

You should track the management of your project in the same manner as any other process! This may sound somewhat absurd — surely how Toyota managed their production line in 1940s post-war Japan is not how I should manage my growth-marketing project in 2024. 

Well...yes and no. 

The *process of creating process* is the same, the end process of course is different. 

So first things first, you need to consider the shared process that every work item is going to go through. Don't consider edge cases, branching patterns, or any of that fancy stuff. 

Just image: if everything goes smoothly, what would a typical "unit" of work go through in terms of process, from the time it is first concieved to when it is marked as done?

Most *knowledge* projects have a similar shape. By *knowledge* projects we mean projects whose main output are ideas, words, or code. 

- **Ideas**: Initial brainstorming and concept generation.
- **Backlog (Long Term)**: Items that are important but not urgent.
- **Backlog (Short Term)**: Items that need to be addressed soon.
- **In Progress**: Work that is actively being done.
- **Under Review**: Work that needs quality checks or feedback.
- **Client Review**: This is optional depending on if it's a client project.
- **Done**: Completed tasks that have met all requirements.

Then, the next thing that you need to consider is the meta-data that you need to collect for each work unit. 

These will be your "fields".

In [Blue], we split out fields into defaults fields and [custom fields](/platform/custom-fields). 

Our default fields are:

- **Name**: The name of the work item.
- **Start/End Date**: The start and end dates of the work item.
- **Assignees**: The team members who are working on the work item.
- **Tags**: Keywords or categories that describe the work item.
- **Dependencies**: Other work items that this work item depends on.

We then have lots of additional custom fields that are configurable based on the type of project or process that is being worked on:

- **[Single Line Text](https://documentation.blue.cc/custom-fields/single-line-text)**: A field for entering a short text.
- **[Multiple Line Text](https://documentation.blue.cc/custom-fields/multiple-line-text)**: A field for entering longer text.
- **[URL / Link](https://documentation.blue.cc/custom-fields/url-link)**: A field for entering a web address or hyperlink.
- **[Currency](https://documentation.blue.cc/custom-fields/currency)**: A field for entering monetary values.
- **[Country](https://documentation.blue.cc/custom-fields/country)**: A field for selecting a country.
- **[Date](https://documentation.blue.cc/custom-fields/date)**: A field for entering a date.
- **[Formula](https://documentation.blue.cc/custom-fields/formula)**: A field for calculated values based on other fields.
- **[File](https://documentation.blue.cc/custom-fields/file)**: A field for uploading and attaching files.
- **[Single Select](https://documentation.blue.cc/custom-fields/single-select)**: A field for selecting a single option from a list.
- **[Multiple Select](https://documentation.blue.cc/custom-fields/multiple-select)**: A field for selecting multiple options from a list.
- **[Location / Map](https://documentation.blue.cc/custom-fields/location-map)**: A field for entering geographical information.
- **[Phone Number](https://documentation.blue.cc/custom-fields/phone-number)**: A field for entering a phone number.
- **[Email](https://documentation.blue.cc/custom-fields/email)**: A field for entering an email address.
- **[Star Rating](https://documentation.blue.cc/custom-fields/star-rating)**: A field for giving a rating out of five stars.
- **[Checkbox](https://documentation.blue.cc/custom-fields/checkbox)**: A field for binary yes/no or true/false values.
- **[Number](https://documentation.blue.cc/custom-fields/number)**: A field for entering numerical values.
- **[Percent](https://documentation.blue.cc/custom-fields/percent)**: A field for entering percentage values.
- **[Unique ID](https://documentation.blue.cc/custom-fields/unique-id)**: A field for generating a unique identifier.
- **[Reference](https://documentation.blue.cc/custom-fields/reference)**: A field for linking to another record or entry.
- **[Lookup](https://documentation.blue.cc/custom-fields/lookup)**: A field for retrieving information from another table or list.
- **[Duration](https://documentation.blue.cc/custom-fields/duration)**: A field for entering a length of time.

And based on your unique project needs, you need to decide which of these fields you should have.

For instance, if you are doing a web redesign project, then you would like want to have a custom field for "URL" for each work item, so that the specific URL of the feature or page can be directly accessed. 

Once you have created your process and custom fields, then you need to create your actual work plan, often called a WBS.  

A Work Breakdown Structure (WBS) is an essential project management tool that divides a project into manageable sections, ensuring clarity and organization.

First, identify the project's major deliverables, such as "Design," "Development," and "Testing" for a website redesign project. Then, break these deliverables into smaller, manageable tasks. For example, under "Design," tasks could include "Create Wireframes," "Develop Prototypes," and "Review Designs." Ensure tasks are detailed enough to manage but not overly granular to avoid micromanagement.

Consider versioning deliverables into V1, V2, and V3 for iterative development and continuous improvement. For instance, "Develop Prototypes V1" might involve basic functionality, "Develop Prototypes V2" could include user feedback improvements, and "Develop Prototypes V3" would polish the final design. This approach ensures progressive enhancement and better quality control.

Assign resources to each task to clarify responsibilities and ensure resource availability. Define milestones to mark significant project stages, such as "Design Completion," "Development Completion," and "Project Launch." Estimate the time and costs for each task to help plan the project schedule and budget.

Create a hierarchical structure to visually organize tasks. Use tools like a Kanban board to represent this hierarchy, aiding in better understanding and management of the project's scope and complexity.

A WBS plan improves organization by providing a clear, structured view of the project. It breaks down the project into manageable tasks, facilitating effective tracking and management. Enhanced communication ensures all team members understand their roles and responsibilities.

Effective resource allocation is achieved through a WBS, preventing overallocation or underutilization of resources. It also allows for accurate progress tracking, making it easier to monitor progress against the project plan and address issues early.

In summary, a well-crafted WBS plan is essential for successful project management. It provides clarity, enhances communication, ensures effective resource utilization, and ultimately leads to the successful completion of the project.


## Task Prioritization Strategy

First: why prioritize?

Well, because resources are *scarce*. 

We never have enough time, money, people, or mental health to do everything that a project (or the world!) demands from us. 

This means that we have to compromise. We have to make value judgements. What is more important, what is less so? What needs to be done now, what can be done later, or **perhaps not at all?**

This last point is very important. Often, as project managers, we can get so stuck in "delivery" mode that we forget *why* we are doing the project in the first place.

This means that we focus too much on *outputs* instead of *outcomes*. We treat the project like a large checkbox ticking exercise, instead of trying to understand what is the future that we are trying to create.

This is why it is fine to sometimes look at a piece of work and decide that it is no longer necessary, and that it should be cut. 

However, this takes a spine, especially when there are contracts and clients involved. However, in the long term, doing the right thing aligns everyone's interest. 

Okay, so now we know that we will be forced to prioritize, but how do we *practically* do this on a kanban board? 

Let's assume that you *have* prioritized, now you want to make your priorities clear to the rest of the team. 

Well, you can simply create a new column in your process (remember, we said you *should* be flexible with your process!) called "Priority" and stick all the things that are a priority in that list.

Next, tag them or colour them in red, as everyone associates this color with urgency and importance. It is important to tag them, because when these work items move to "in progress" or "under review", they will no longer be in the "priority" list, so you need something else that clearly marks them as priority items. 

## Conclusion

Kanban boards are an invaluable tool for project management. They provide clear visual transparency, improve workflow management, enhance collaboration, and reduce cognitive load. By breaking down projects into manageable tasks and visual stages, teams can quickly identify bottlenecks and continually optimize their processes.

If you want to experience the transformative power of Kanban boards, consider trying Blue. Our platform is designed to make project management intuitive and effective. 

[Sign up today for a free trial](https://app.blue.cc) and see how Blue can streamline your workflow and enhance your team's productivity.















