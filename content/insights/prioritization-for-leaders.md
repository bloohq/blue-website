---
title: Prioritization for Leaders
slug: prioritization-for-leaders
tags: ["insights", "project-management"]
description: Explore essential prioritization strategies for leaders. There's always too much to do — so having a framework to decide what to priotitize is crucial for success.
image: /resources/prioritization-for-leaders.jpeg
date: 07/07/2024
showdate: true
sitemap:
  loc: /resources/prioritization-for-leaders
  lastmod: 2024/07/07
  changefreq: monthly
---

The key issue is that many leaders confuse business with busyness. Being busy does not *automatically* mean that you are being effective, and often it can actually mean quite the opposite.

If a leader actually finds that they has plenty of spare time, then that is a sign that they is doing a good job, and that everything is running smoothly.

> "Hire a lazy person to do a difficult job, because they will find an easy way to do it" 

~ **Bill Gates.**

We spend far too much time tackling the low-hanging fruit, instead of working on what really matters.

So the key question we should always be asking is:

### What To Do Next?

The reality of leadership is that if you try and tackle everything on your todo list, you can easily end up working 100+ hour weeks, and this can lead to a quick burn out, and a general lack of effectiveness.

Business is not a sprint, but a marathon. There will always be too much to do, so the trick is to build a framework of looking at things to ensure that you can choose the most important thing to work on.

Imagine that you have a garden hose and there is plenty of water running through it. Things are great, and you can provide water to all the plants in your garden without much effort. Smile with inner contentment, finish early, and relax!

However, if the water flow is blocked and only trickles slowly, then you might need to spend the entire day watering your garden unless you fix the underlying problem, the knot in the hose that is stopping the water flow.

And yet, many of us are content to spend more and more time watering the garden, instead of stopping what we are doing and focus on that really one important task that will make the difference.

This key point is called the Point of Constraint. It's whatever is stopping your organization from sprinting forwards and achieving amazing results, and this should be the primary focus of all leaders, because it is so important that it is not something that should be delegated.

In fact, I would go as far as to say that all other work that is not at the Point of Constraint is actually a waste of time for leaders, and should either be delegated or ignored.

Many leaders fall into the trap of uniform time allocation, where they try and please all stakeholder groups with attention. They will spend time with everyone:

- Board of Directors
- Shareholders
- Direct Reports
- Employees
- Clients
- Suppliers
- Partners
- Prospects

The problem here is that by diluting their attention across so many groups, they are not able to spend much time on the Point of Constraint.

This is most obvious when comparing a Hard Working CEO that pleases all the above groups, to the Lazy CEO that will mostly focus on the Point of Constraint.

The Lazy CEO will spend 50% to 80% of his or her time on the Point of Constraint, while the Hard Working CEO will also be able to spend perhaps 5% or 10% of his or her time on that task.

The Lazy CEO will discover and remove key blocking points for business five to ten times faster than the Hard Working CEO!

So one piece of advice here: ignore departments, individuals, and divisions that are doing well, they don't need you!

## How to Tackle the Point of Constraint

There is an extremely obvious and simple approach to tackling issues that are Points of Constraint:

1. Find what is a Point of Constraint.
2. Fix it!
3. Give it to someone else to manage, and move on.

However, when we say "Fix it!", we don't mean a temporary fix, but a system, permanent fix that ensures that this problem or type of problem will not happen again, and that someone else can easily resolve it next time by following simple processes. If you think about it, this type of behaviour actually compounds over time, because the more and more of these issues come up, the better the organization is at being able to solve them and fix them permanently, and the benefits stack up over time.

## Finding the Point of Constraint

When we say that someone should wear different hats, we mean having different mental models to deal with different situations. Having a wide variety of mental tools to fix problems is a huge advantage, because you don't run into the everything-is-a-nail problem:

> "If your only tool is a hammer then every problem looks like a nail." ~ Abraham H. Maslow.

So, let's examine the five mental models that you can use when you're working at the Point of Constraint.

### The Learner Hat

Having a learning mindset as a leader is perhaps one of the most important things that you can do, for multiple reasons.

Firstly, it sets a great example to the rest of the organization, and one could argue that in this fast-paced globalized economy, organizational learning is perhaps the only real differentiator.

Secondly, the expiry date of knowledge is becoming shorter and shorter. It's quite interesting to think that at the date of writing, 2018, the entire phone and app industry has only been around for ten years, and there have been plenty of micro-revolutions during this time. So, whatever you know right now may well not be that useful in a few years time.

At Blue, we try to institutionalize this by certain things:

- Courses. Anyone can request a purchase for any online course related to the profession, and we will buy it. 
- Book Purchasing. Anyone at Blue can buy a book and get refunded for the full amount.

> "Anyone who stops learning is old, whether at twenty or eighty. Anyone who keeps learning stays young. The greatest thing in life is to keep your mind young." ~ Henry Ford.

### The Architect Hat

Imagine three builders, all working at the same construction site.

You ask the first one what they are doing, and he replies: *"I'm doing a job, to earn some money."*

You ask the second builder what they are up to, and he replies: *"I'm building a wall."*

You ask the third builder what he is doing, and he replies: *"I am building a cathedral."*

This is how we should aim to think when we are wearing the Architect hat. It's about thinking of the long-term picture, the vision, and connecting what you are doing right now to the results that you want to see a long way down the road.

The key point here is that one can waste a huge amount of effort on a bad strategy and get nowhere, and conversely you can have moderate effort on a great strategy and get awesome results without too much sweating.

Specifically in terms of business, you should be thinking about:

- How to be different? It's unlikely that you'll have no competitors, so how does one go about creating and clearly communicating what makes your organization different from everyone else.
- Ask yourself what won't change in the long term. It is tempting to always focus on new stuff, but ensuring that you understand what the stable factors in your industry are, and to keep investing in them, in something that is often overlooked. After all, nobody is going to wake up in ten years time and wish that your product was harder to use, took longer to deliver, or was more expensive.
- Developing a Mafia Offer. Essentially, building offers for your various stakeholder groups that are difficult to refuse, but not because of extortion, but because the offer is so good.

If you're not yet a leader, you can still apply this mental model to your own work by asking the following two questions:

1. Do you understand the mission and vision of your company?
2. Does your work, on a day to day basis, relate to this?

If the answer to either of these questions is "no", then you should stop what you're working on and start asking some questions.

### The Engineer Hat

If you are wearing the Architect hat you are asking "What", then when you are wearing the Engineer hat you are asking "How".

Specifically, how do I craft a great experience for everyone that comes into contact with our organization, especially customers.

In 2017, the airline industry pulled an incredible feat. There were zero passenger deaths in commercial jet travel. This is staggering when you think of that there were over 4 billion individual trips across 36.8 million flights that year. To put this into perspective, it was probably more dangerous to walk down the road that year than to fly in an airliner.

So how did they do it?

Well, by an obsessive focus on procedures and checklists, for every potential situation, so that when things inevitably do go wrong, the likelihood of a fatal incident is minimized.

At Mäd, we have an entire system dedicated to build up SOP (Standard Operating Procedures) that helps our team create consistently high-level work.

I strongly encourage to make your own set, you'll reap the benefits right away.

### The Coach Hat

In many businesses, the teams are told that they are like a "family".

However, I much prefer the analogy of a business as a professional sports team, and the leader as the head coach, with the key responsibility being to find and develop the best talent, and cut the non-performers.

After all, the reality is that as a business evolves, certain people end up being bad fits and should go and find careers elsewhere, but this is not what happens in a family!

If Little Johnny has consistent bad table manners, his parents don't sit him down and say: "Look, we tried this for six years, but it obviously isn't working out, you are still not learning to behave at a table, so unfortunately we're going to have to cut you from the family."

So if you tell team members that they are family, and then they get laid off, they will feel angry, embarrassed, and, quite rightly, betrayed. If, however, you treat the business like a sports team, then everyone will be more understanding, and they can focus on finding another career elsewhere.

That said, I strongly believe that work should be fun. We inevitably spend quite a lot of time at work, and having a fun environment where great work is accomplished is of the utmost importance.

### The Player Hat

Often, a leader is a great individual performer, which is what led them to become leaders in the first place, and so it is natural that they should occasionally jump in and get their hands dirty and actually do the core work that their business requires.

This is a fantastic way to gain additional insight, as well as develop empathy with the people who actually do the work each day, and how their work lives can be improved.

It can also be great in an emergency if there is suddenly a large workload, or someone is absent.

However, leaders should be careful not to overuse their Player hat, especially if it is to cover underperforming team members. The priority in this case would be to fix the performance issue, not cover for the team member.

## A Few Examples

If you don't know where to start in terms of finding and fixing Points of Constraint, then this actually means that the Point of Constraint right now is you, and the most important thing for you to do is to improve and learn as much as possible.

If you suspect that you have business model problems, then the Architect hat is the most appropriate, and these following points are worth exploring:

- **Lack of Momentum:** If you're not growing above inflation year on year, and growth is difficult, then you're probably suffering from a lack of momentum.
- **Capital Intensive:** If you're buying revenue (i.e. Spending $1.50 to get $1 from customers) or if you need to go to the bank each time you get a new large order, then this is definitely a point of constraint.
- **Low Gross Margins or Profits:** If your gross profit is less than 50%, or if your profits are less than 20%, then this is holding you back.
- **Low recurring revenue:** If 50% of your revenue is not recurring, then this may be a Point of Constraint.

If you believe that team issues are what is holding you back, then put your Coach hat on and look at the following:

- Would you rehire a current employee right now? If not, then they should go.
- Would you fight to keep a current employee if they told you that they were going to leave? If not, then they should go.
- If, as an organization, there is little commitment to learning, then this is worth investigating.
- If individuals are putting themselves or their departments above the company, then often this means that there is little alignment.
- If ideas and energy don't bubble up to you all the time, and if you ask for feedback and get little in return, then this means that your team don't care that much, and that's something to also address.

If you think you might have process problems, then wear the Engineer Hat, and start by asking:

- Are we delivering our products or services at the quality our customers expect?
- Also, if you have a recurring need to hire superstars for every position, then this might also be a cause for concerns. Great process allows even moderate performers to do good work. Think of McDonalds, who have managed to build a global multi-billion dollar brand by mostly employing rather unmotivated paid-by-the-hour teenagers.

## Summary

So, the key takeaway point:

- The Point of Constraint is whatever is stopping the business from moving forwards.
- Leaders should work at the Point of Constraint as much as possible, not spread their time around to all stakeholders evenly.
- Discover the key issues.
- Fix those issues in a permanent manner.
- Handover the responsibility to someone else.
- Repeat.
