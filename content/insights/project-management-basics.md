--- 
title: The Non-Project Manager's Guide to Project Management
slug: project-management-basics-guide
tags: ["insights", "project-management"]
description: This guide offers practical and straightforward rules to help non-project managers effectively plan, execute, and complete projects successfully.
image: /resources/project-management-guide-background.png
date: 27/05/2024
showdate: false
sitemap:
  loc: /resources/project-management-basics-guide
  lastmod: 2024/05/27
  changefreq: monthly
---

Welcome to **The Non-Project Manager's Guide to Project Management.**

This guide is designed to demystify the core concepts and help those who are new to project management or have suddenly found themselves managing projects. While experienced project managers may also find this a useful refresher on the basics, the primary aim is to provide clear and straightforward rules that are both simple and flexible for newcomers. Project management doesn't have to be complicated; with the right approach, you can streamline your processes, optimize resources, and deliver successful projects consistently. By focusing on essential principles and practical tips, this guide will equip you with the tools needed to navigate the complexities of any project, enhance team collaboration, and ensure timely and cost-effective outcomes.

Effective project management is crucial for career advancement and helping your business grow. Regardless of your industry, whether you are building software, a car, or a hotel, the basics of project management remain the same. 

At Blue, we believe that [everything is either a project or a process](/resources/projects-processes), and today we are focusing on the project side of things. 


## Everyone is a project manager

The reason we wrote this guide is that many people who are not formally trained as project managers now find themselves managing projects in various capacities. This shift is due to the increasing complexity of tasks and the collaborative nature of modern work environments. Professionals across different fields, such as marketing, IT, healthcare, and education, often take on project management roles to drive initiatives and achieve specific objectives.

These individuals must quickly learn to juggle multiple responsibilities, coordinate with diverse teams, and ensure that their projects meet deadlines and budgets. Many of these "accidental" project managers have to learn on-the-go and often need to improvise to keep their projects on track. While they may not have formal project management training, they benefit greatly from understanding basic project management principles and techniques. This knowledge helps them navigate challenges, optimize resources, and deliver successful project outcomes.

In addition to technical skills, the importance of soft skills in project management cannot be overstated. Effective communication, leadership, problem-solving, and emotional intelligence are crucial for managing teams, resolving conflicts, and maintaining motivation. These soft skills help project managers build strong relationships with stakeholders, foster a collaborative team environment, and navigate the complexities of project management with greater ease. A successful project manager should be liked, but also tough yet fair. Being approachable and empathetic helps in gaining the trust and cooperation of the team, while being firm ensures that standards are maintained and objectives are met. This balance is key to effective project management.

In other words, get things done — without being an asshole.

Effective project management skills are valuable for everyone, *regardless of their official job title.*

## What is project management?

Let's start with the real basics — what exactly *is* project management? 

Project management is a structured approach to planning, organizing, and controlling resources, such as time, people, money, and materials, to achieve specific goals *within* a defined timeline. 

It involves initiating, planning, executing, monitoring, and closing projects to meet objectives and deliver results. Project management encompasses a range of processes and methodologies, such as defining the project scope, setting objectives, developing schedules, and managing budgets. It also includes coordinating team members, communicating with stakeholders, and ensuring that the project stays on track and meets its intended outcomes. Effective project management requires a blend of technical skills, strategic thinking, and leadership abilities to guide a project from inception to completion successfully.


## Why project management in important

Let's stop and ask ourselves: what happens *without* project management?

Without proper project management, projects can quickly become chaotic and unmanageable. Tasks may be overlooked, deadlines missed, and budgets exceeded. Teams can become disorganized and demotivated, leading to miscommunication and conflicts. Stakeholders might not receive regular updates, resulting in misaligned expectations and dissatisfaction. Ultimately, the lack of structured project management can cause projects to fail, wasting valuable time, resources, and opportunities. 

By chance, some projects may succeed without a designated project manager, but this is often because someone — *and this could be you right now!* — ends up being the de facto project manager without officially being given the role. 

At its core, project management is essential because projects are more likely to succeed when there is a structured approach in place. The reality of project management is that you cannot opt out of it; you can only manage a project poorly or manage it well. Since project management is an inevitable aspect of achieving goals and completing projects, it's crucial to learn the proper methods to do it effectively. 

Project management provides a structured framework for achieving project goals efficiently and effectively. It involves planning, organizing, and controlling projects to meet specific objectives and deliver results. This structured approach helps ensure that projects are completed on time and within budget. Clear steps and procedures help maintain focus, streamline processes, and manage changes effectively, leading to more predictable and successful outcomes.

Additionally, project management helps organizations allocate resources more effectively by identifying the skills and resources needed for a project. This ensures that the right people and resources are assigned to the right tasks. Effective resource allocation prevents team overload, reduces waste, and maximizes productivity.

Managing risks is another critical aspect of project management. It helps identify potential issues early, allowing for proactive problem-solving and adjustments. By anticipating risks and implementing mitigation strategies, project managers can prevent minor issues from becoming major problems. This proactive approach leads to higher quality outcomes, increased customer satisfaction, and improved overall performance.

Project management also promotes clear communication and enhances collaboration among team members. By establishing a common language and set of processes, it ensures that all stakeholders are on the same page and working towards the same goals. This clarity reduces misunderstandings, fosters cooperation, and facilitates smooth project progress.

Furthermore, project management helps align project objectives with broader organizational goals. By ensuring that project objectives support the organization’s strategic vision, project management ensures that projects contribute to overall success. This alignment helps prioritize valuable projects and directs efforts towards achieving long-term goals.

In essence, project management is vital for turning strategic visions into tangible results, driving progress, and maintaining a competitive advantage in any industry. By providing a structured approach, better resource allocation, risk management, clear communication, and alignment with organizational goals, project management enhances the likelihood of success and contributes significantly to individual and organizational growth.

## Key components of project management

### 1. A list of things that need to get done

At the most basic level, you need to identify what needs to get done to "achieve" the project. Where are you now vs where you need to be? You would be surprised how many projects just move forwards on a day by day basis, with no clear idea of the entirity of things that need to get done. 

Identifying project goals and objectives is the first step in project management. Goals are broad statements about what the project aims to achieve, while objectives are specific, measurable actions that help realize these goals. Defining these clearly provides direction for the project and sets the stage for planning and execution. This involves understanding the needs and expectations of stakeholders and ensuring the project aligns with the overall vision of the organization. Well-defined goals and objectives guide the project team and keep everyone focused on the desired outcomes.

Defining the project scope is crucial for setting boundaries and expectations for what the project will deliver. The scope outlines the specific tasks, deliverables, and features the project will include. It helps prevent scope creep, which happens when extra tasks are added without proper approval, potentially derailing the project. A clear project scope provides a roadmap for the team, ensuring all efforts are directed towards the agreed-upon objectives. It includes detailed descriptions of the work required, as well as any exclusions, constraints, and assumptions related to the project. 

Interestingly, defining what is out-of-scope is just as important as defining what is in scope! Clear boundaries help avoid misunderstandings and miscommunications between the project team and stakeholders. Since no one can read each other's minds, explicitly stating what will not be included in the project is crucial. This prevents stakeholders from assuming certain features or tasks are part of the project when they are not. By documenting these exclusions, constraints, and assumptions in writing, everyone has a clear and mutual understanding of the project’s limits, which helps maintain focus and prevents scope creep.

Outlining project deliverables involves specifying the products or results that the project will produce. Deliverables can be documents, reports, software applications, infrastructure components, or any other outcomes needed to achieve the project goals. Clearly defined deliverables ensure that stakeholders understand what will be delivered and help in tracking progress throughout the project lifecycle. Each deliverable should have criteria for acceptance, so the project team knows exactly what is expected and can verify the quality and completeness of the work.

Creating a Work Breakdown Structure (WBS) is a critical step in organizing and managing the project’s tasks. A WBS breaks down the total scope of work into smaller, manageable parts, or work packages, which can be easily assigned and tracked. This helps clarify the tasks that need to be completed, their dependencies, and the order in which they should be tackled. It facilitates better planning, resource allocation, and risk management, ensuring all aspects of the project are covered. In the end, a WBS is just a detailed list of related granular tasks—nothing more!

[Kanban boards](/platform/kanban-boards) are an excellent tool for visualizing and managing tasks within a project. They help show the amount of work in the backlog and the status of all tasks. By using columns to represent different stages of the workflow, such as "To Do," "In Progress," and "Done," team members can easily see the status of tasks and what needs to be done next. This transparency enhances collaboration and ensures that everyone is aware of the project's current status, preventing bottlenecks and improving overall efficiency.

Estimating resources and costs involves determining the human, financial, and material resources required to complete the project and calculating the associated costs. Accurate estimation is crucial for budgeting, scheduling, and resource allocation. This process includes identifying the skills and expertise needed, the number of team members, equipment, and materials, as well as any external services that might be required. Cost estimation involves forecasting expenses related to salaries, materials, equipment, travel, and other project-related activities. A realistic and detailed estimate helps secure necessary funding, manage expenditures, and ensure the project stays within budget.

### 2. Who Needs to Do What?

Now that you have a list of things that need to get done, you need to figure out who is going to do what. Understanding the roles and responsibilities within a project is crucial for its success. Clearly assigning tasks ensures everyone knows their responsibilities, preventing overlap and gaps in the workflow. 

Ideally, you want your team members focussing on doing what they need to do, not trying to figure out what they should be doing. 

Here’s an overview of key roles and their responsibilities:

#### Project Manager
The project manager is the person responsible for planning, executing, and closing the project. They oversee the project from start to finish, ensuring it meets its goals, stays within budget, and is completed on time. Key responsibilities include:
- Defining project scope and objectives
- Creating and maintaining the project plan
- Allocating resources and setting deadlines
- Monitoring project progress and making adjustments as needed
- Communicating with stakeholders and team members
- Managing risks and resolving issues
- Reporting on project status

#### Project Team
The project team consists of individuals with various skills and expertise who work together to complete the tasks outlined in the project plan. Each team member has specific roles and responsibilities, which may include:
- Completing assigned tasks on time and to the required standard
- Collaborating with other team members
- Providing input and feedback during project meetings
- Reporting progress and any issues to the project manager
- Contributing to risk identification and mitigation strategies

#### Stakeholders
Stakeholders are individuals or groups who have an interest in the outcome of the project. They can be internal (e.g., employees, managers) or external (e.g., customers, suppliers). Their involvement and influence vary, but common responsibilities include:
- Providing requirements and feedback
- Reviewing project deliverables
- Approving project phases and milestones
- Supporting the project team with resources or expertise
- Communicating expectations and concerns

#### Sponsors
Sponsors are typically high-level executives or managers who provide financial resources and support for the project. They have a vested interest in the project’s success and are responsible for:
- Securing funding and resources
- Defining high-level project goals and objectives
- Approving major project decisions
- Providing strategic direction and guidance
- Ensuring alignment with organizational goals
- Removing obstacles that may hinder the project’s progress

#### Subject Matter Experts (SMEs)
SMEs bring specialized knowledge and expertise to the project. Their responsibilities may include:
- Offering technical guidance and advice
- Assisting in the development of project deliverables
- Providing training and support to the project team
- Ensuring that the project meets industry standards and regulations

#### Clients or End Users
Clients or end users are the recipients of the project’s final deliverables. Their responsibilities often include:
- Communicating needs and expectations
- Participating in user acceptance testing
- Providing feedback on deliverables
- Ensuring the final product meets their requirements

Clearly defining who needs to do what helps streamline project execution, enhances accountability, and improves collaboration. By understanding and respecting each role’s responsibilities, the project team can work more effectively towards achieving their common goals.

### 3. When do things need to get done by?

After determining what needs to be done and who will do it, the next crucial step is figuring out when everything needs to be completed. Setting clear deadlines and milestones is essential for keeping the project on track and ensuring timely delivery of results.

The most fundamental aspect is establishing a defined project end-date. This provides a clear constraint—the overall amount of time available—which then informs how you can adjust other factors such as quality, the number of team members, and other resources. It's important to recognize that sometimes the project end-date can be more flexible than initially thought. Engaging in an open discussion with key stakeholders about the reasons behind the chosen end date can reveal opportunities for adjustments. Sometimes, the end date may have been set arbitrarily (e.g., end of Q2), and understanding this can help you better plan and allocate resources, or push back on timelines to ensure that the project has enough time to be successful. 

Establishing a project timeline involves outlining the major phases and setting key milestones, which are significant points or events that indicate progress. Clear timelines and milestones provide a roadmap for the project and help in tracking progress against the plan. Developing a detailed project schedule then breaks down this timeline into specific tasks and activities with assigned start and end dates. This schedule should be realistic and take into account all aspects of the project, including resource availability and potential risks.

Our suggestion is to break managing timelines into two distinct parts. 

First, create a high-level plan that outlines the major buckets of work that need to be completed. This high-level plan can be reviewed with stakeholders, allowing everyone to align themselves with the overall project plan without getting lost in the details. It provides a clear overview of the project's trajectory and ensures that all parties have a common understanding of the project's scope and objectives.

Second, develop a separate, detailed plan that includes all the smaller tasks that need to be accomplished. This detailed plan should be reviewed regularly with the project team to ensure there is no slippage. If delays do occur, this plan will help identify them early and allow the team to devise strategies to address them. Remember, projects become months late one day at a time, so staying on top of daily progress is crucial to maintaining the overall schedule.

This is where [Gantt Chart software](/platform/timeline-gantt-charts) becomes invaluable. Gantt charts provide a clear visual representation of the project schedule, showing the start and end dates of each task, their duration, and dependencies between tasks. By using Gantt charts, anyone can easily track progress, identify potential bottlenecks, and adjust timelines as needed to keep the project on track. Regular updates to the Gantt chart ensure that all team members are aware of the current status and any changes to the schedule, facilitating better communication and coordination within the team. 

The key idea here is that anyone involved in the project can independently view the project plan and understand where the project stands at any given moment. This transparency fosters accountability and ensures that everyone is on the same page, reducing the risk of miscommunication and confusion. It empowers team members to take ownership of their tasks, make informed decisions, and collaborate more effectively towards the project's goals.

Remember that projects rarely go exactly as planned. If they did, there would be little need for project managers! A large part of a project manager's job is to figure out what to do when things don't go as expected. It's important to be flexible and adjust the schedule as needed. Regularly review the project’s progress and make necessary adjustments to timelines and tasks to address any delays or changes in scope. Effective communication with the team and stakeholders is crucial for managing these adjustments smoothly.

Setting clear deadlines while maintaining a flexible, well-organized schedule ensures that all project tasks are completed on time, keeping the project on track for successful completion.

## Understanding critical paths

We often see people's eyes glaze over at the mention of "critical path." It sounds like a complex term reserved for experienced project managers, but this couldn't be further from the truth!

The critical path is a concept in project management that helps you identify the sequence of tasks that determine the *minimum* time needed to complete a project. Understanding the critical path is crucial because it highlights which tasks cannot be delayed without affecting the project's overall timeline.

In simple terms, the critical path is the longest stretch of dependent tasks in a project. These tasks must be completed on time to ensure the project finishes on schedule. 

**Any delay in these tasks will directly impact the project's end date.** This is really important to understand. If you critical path slips by a day, your project will be one day late. 

Let's define some key terms:

* **Dependent on:** A task is dependent on another task if it cannot start until the other task is completed. For example, you cannot book a venue until you have chosen a date.
* **Blocking:** A task is blocking another task if it must be completed before the other task can begin. For instance, booking the venue is blocking the sending of invitations because you need to confirm the venue before inviting guests.

Let's make this tangible with an easy-to-understand example. Let's plan a community picnic. 

Here are the tasks you need to complete:

* Choose a date (1 day)
* Book the venue (2 days) [dependent on choosing a date]
* Send out invitations (3 days) [dependent on booking the venue]
* Plan the menu (2 days) [dependent on sending out invitations]
* Order food (1 day) [dependent on planning the menu]
* Set up the venue (1 day) [dependent on ordering food]
* Hold the event (1 day) [dependent on setting up the venue]

Some of these tasks can be done at the same time (in parallel), while others need to be done in sequence (in series). For instance:

* You cannot book the venue until you choose a date.
* Invitations can be sent out once the venue is booked.
* You need to plan the menu before ordering food.

Let's break it down:

* Choose a date (1 day)
* Book the venue (2 days) [dependent on choosing a date]
*Send out invitations (3 days) [can start after booking the venue]
* Plan the menu (2 days) [can start any time]
* Order food (1 day) [dependent on planning the menu]
* Set up the venue (1 day) [can only be done the day before the event]
* Hold the event (1 day)

Now, let's identify the critical path:

* Choose a date (1 day)
* Book the venue (2 days)
* Send out invitations (3 days)
* Order food (1 day) [must be ordered in time to set up the venue]
* Set up the venue (1 day)
* Hold the event (1 day)

Adding these up, the critical path is 1 + 2 + 3 + 1 + 1 + 1 = 9 days.



Tasks like planning the menu and ordering food are not on the critical path because they can be completed without delaying the event if done within the time allowed by the critical path tasks.

By identifying the critical path, you can prioritize tasks that are crucial for the project's timely completion. You can also allocate resources more effectively and manage potential delays better. Knowing the critical path helps you understand which tasks have flexibility (known as "float" or "slack") and which do not. This means you can focus on ensuring that bottlenecks are kept out of the critical path, which leads us nicely to the next point.

## Not everything is of equal importance. 

As a project manager, it's essential to understand that not all tasks are equally important. Your primary focus should be on the point of constraint and the *"next most important thing"* to ensure that the critical path remains clear of blockages.

The key issue is that many project managers confuse busyness with effectiveness. Being busy does not automatically mean you are being productive, and often it can mean quite the opposite. If you find that your schedule is packed with tasks, it's crucial to step back and evaluate what truly matters.

Imagine you have a garden hose with plenty of water running through it. If the flow is blocked and only trickles slowly, you might spend the entire day watering your garden unless you fix the underlying problem—the knot in the hose. Similarly, in project management, focusing on low-priority tasks can waste valuable time, while the real issue—the point of constraint—remains unaddressed.

The point of constraint is whatever is currently stopping your project from moving forward and achieving its objectives. This should be your primary focus because it directly impacts the project's progress. If you can identify and resolve the point of constraint, you will clear the path for smoother operations and quicker completion.

To tackle the point of constraint, follow these steps:

1. **Identify the Point of Constraint**: Determine which task or issue is currently hindering your project's progress.
2. **Fix It**: Address the problem with a permanent solution that ensures it won't recur. Implement a system or process that can handle similar issues in the future.
3. **Delegate and Move On**: Once the constraint is resolved, delegate its management to someone else and shift your focus to the next most important task.
Effective project managers focus their time and energy on the tasks that have the most significant impact on the project's success. 

This means prioritizing high-impact activities over low-priority ones and ensuring that the critical path is always clear of any blockages. By doing so, you can keep your project on track and lead it to a successful completion.

Remember, the goal is not to please all stakeholders equally but to ensure that the project moves forward efficiently and achieves its main goals. Spend your time on the tasks that matter most, and delegate or ignore those that don't. This way, you can maximize your effectiveness and achieve the best possible outcomes for your project.

One common pitfall is spending too much time on low-hanging fruit—tasks that are easy to complete and offer a quick sense of accomplishment. While it feels good to check these items off your to-do list, focusing on them can distract you from more critical issues that have a larger impact on the project's success. The key is to identify which tasks truly drive progress and which ones simply make you feel productive. By consciously avoiding the temptation to tackle only easy tasks, you can ensure that your efforts are directed towards activities that significantly contribute to the project's goals and timelines. This strategic focus helps in keeping the project on track and prevents delays that arise from neglecting more challenging but essential tasks.

Balancing short-term milestones with long-term goals is crucial for maintaining momentum and demonstrating progress to stakeholders. While achieving quick wins is important, it's equally vital to prioritize tasks that contribute significantly to the overall project objectives. This dual focus ensures that while you're meeting near-term expectations, you're also laying the groundwork for sustained project success. By aligning your efforts with long-term goals, you avoid the pitfall of getting sidetracked by less impactful tasks. This approach helps in building a coherent project strategy where each step taken moves you closer to the ultimate vision, ensuring a smooth progression and a higher likelihood of achieving comprehensive project success.


## Communication & Documentation

Effective communication is perhaps the most crucial aspect of a project. The ability to relay information between stakeholders and project team members is vital, ensuring that everyone is aligned on their tasks and responsibilities.

You might think that more communication is always better. However, paradoxically, the role of documentation is to *reduce* the need for constant communication by allowing anyone involved in the project to access key information independently. So communication and documentation are two sides of the same coin.  

Unfortunately, documentation is often the first thing that gets neglected when people are busy, but this neglect can actually make you busier as it increases the need for more communication to keep everyone informed.

### Weekly status meetings

Weekly project meetings are a must to keep everyone on the same page and address any issues promptly. These meetings should have a clear structure to ensure they are productive and efficient.

Here’s a suggested structure for your weekly project meetings:

* **Review of Completed Tasks**: Review what was accomplished since the last meeting. Celebrate successes and ensure everyone is aware of the progress made.
* **Current Status of Ongoing Tasks**: Discuss the status of tasks currently in progress. This helps identify any potential delays or issues early on.
* **Upcoming Tasks and Priorities**: Outline what needs to be done in the upcoming week. Make sure everyone knows their responsibilities and the priorities for the week.
* **Blocked Tasks**: Identify any tasks that are currently blocked and discuss the reasons. This is crucial for finding solutions and keeping the project moving forward.
* **Risks and Issues**: Highlight any new risks or issues that have arisen. Discuss mitigation strategies and assign responsibility for managing these risks.
* **Decisions Needed**: Identify any decisions that need to be made and ensure the necessary information is available for decision-making.

### Weekly stakeholder communications

In addition to weekly meetings with the project team, it's essential to send a weekly written update to key stakeholders. This update provides a clear snapshot of the project's progress and highlights any issues that need attention. Here’s a suggested structure for these updates:

* **Done This Week**: Summarize the key tasks and milestones completed during the week.
* **Blocked This Week**: Identify any tasks or activities currently blocked and explain the reasons for these blockages.
* **Risks/Issues**: Highlight any new risks or issues that have arisen and provide an update on previously identified risks.
* **Next Steps**: Outline the key tasks and milestones planned for the upcoming week.
* **Decisions Needed**: List any decisions that need to be made by stakeholders or the project team.
* **Feedback and Support Required**: Indicate any specific areas where feedback or support is needed from stakeholders.

Additionally, it is important to provide the overall project status, indicating whether the project is on track, at risk, or behind schedule.

## On-topic written communication

To ensure effective communication and easy access to information, it is crucial to have all project communications and documentation in one centralized place. On-topic written discussions should be easily accessible for future reference. Team chat and email are not sufficient for this purpose; dedicated project management software is essential, especially for non-trivial projects that may have 100+ tasks to complete. 

[A centralized project platform](/) allows team members to stay organized, keep track of progress, and quickly find information, which enhances overall project efficiency and effectiveness. At no point should anything become a he-said-she-said situation. If you need an approval or have made a decision, write it down! Documenting key discussions, decisions, and approvals ensures clarity and accountability, and helps avoid misunderstandings or disputes later on.

## Conclusion 

Project management is an essential skill that anyone can master with the right tools and knowledge. This guide has provided a comprehensive overview of the fundamental aspects of project management, from identifying tasks and assigning roles to setting timelines and ensuring effective communication. By understanding these core concepts and applying the practical tips outlined, you can manage projects more efficiently and achieve successful outcomes.

Remember, effective project management involves not only technical skills but also soft skills such as communication, leadership, and emotional intelligence. Striking the right balance between being approachable and maintaining firm standards is key to leading your team effectively and ensuring project success.

At Blue, we understand the challenges that come with managing projects. That's why we've built our platform to be an easy-to-use project management tool that simplifies the process and helps you stay organized. Blue offers a better way to manage projects by providing a centralized place for communication, documentation, and task management. With Blue, you can streamline your workflow, optimize resources, and keep your projects on track.

Whether you're an experienced project manager or someone new to the role, the principles and techniques outlined in this guide will help you navigate the complexities of project management with confidence. By implementing these strategies, you can ensure that your projects are completed on time, within budget, and to the highest quality standards.

Thank you for reading The Non-Project Manager's Guide to Project Management. We hope this guide has demystified project management and provided you with valuable insights to improve your project management skills.













