export default defineAppConfig({
  ui: {
    primary: 'brand-blue',
    gray: 'cool',
    content: {
      prose: {
        code: {
          icon: {
            curl: 'i-ph-terminal-window-duotone',
            python: 'vscode-icons:file-type-python',
            node: 'vscode-icons:file-type-js',
          }
        }
      }
    }
  },
  // seo: {
  //   siteName: 'Nuxt UI Pro - Docs template'
  // },
  toc: {
    title: 'Table of Contents',
    bottom: {
      title: 'Help',
      links: [{
        label: 'Email Support',
        to: 'mailto:help@blue.cc',
        target: '_blank'
      },{
        label: 'System Status',
        to: 'https://status.blue.cc/',
        target: '_blank'
      },
       {
        label: 'Changelog',
        to: 'https://blue.cc/platform/changelog',
      }, {
        label: 'Roadmap',
        to: 'https://blue.cc/platform/roadmap',
      }]
    }
  }
})
