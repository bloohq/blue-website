import { defineNuxtPlugin } from '#app'
import YouTubeEmbed from '~/components/YouTubeEmbed.vue'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('YouTubeEmbed', YouTubeEmbed)
})