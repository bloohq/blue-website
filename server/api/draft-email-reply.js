import OpenAI from 'openai'

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
})

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const { emailThread, keyMessages, urgencyLevel, tone, isBoss, extraClarity } = body

  if (!emailThread) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Email thread is required',
    })
  }

  try {
    const completion = await openai.chat.completions.create({
      model: 'gpt-4o-mini',
      messages: [
        { role: 'system', content: `You are an AI assistant that drafts email replies. 
          Urgency level: ${urgencyLevel}/5. 
          Tone: ${tone}. 
          ${isBoss ? 'This is a reply to your boss.' : ''}
          ${extraClarity ? 'Provide extra clarity in the response.' : ''}
          Draft a reply that incorporates these aspects and any key messages provided.
          Format the email with proper spacing:
          - Include a subject line
          - Add a line break after the greeting
          - Separate paragraphs with a blank line
          - Add a line break before the closing` },
        { role: 'user', content: `Email thread:\n${emailThread}\n\nKey messages to include:\n${keyMessages}` },
      ],
      max_tokens: 1000,
    })

    const reply = completion.choices[0].message.content.trim()

    return { reply }
  } catch (error) {
    console.error('OpenAI API error:', error)
    throw createError({
      statusCode: 500,
      statusMessage: 'Failed to draft email reply. Please try again.',
    })
  }
})