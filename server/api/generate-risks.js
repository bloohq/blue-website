import OpenAI from 'openai'

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
})

const system_prompt = `You are an advanced AI system specializing in comprehensive project risk analysis across various industries and project types. Your task is to identify potential risks in projects, considering multiple aspects such as:
1. Technical risks
2. Financial risks
3. Operational risks
4. Strategic risks
5. Compliance and regulatory risks
6. Environmental risks
7. Human resource risks
8. Stakeholder risks
9. Schedule risks
10. Quality risks

For each identified risk, provide:
1. A concise, descriptive risk name (bolded)
2. A detailed explanation of the risk
3. Potential impact on the project
4. Possible mitigation strategies

Ensure your analysis is thorough, insightful, and adaptable to any project type.`

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const { projectDescription } = body

  if (!projectDescription) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Project description is required',
    })
  }

  const user_prompt = `Please analyze the following project description and provide a comprehensive list of potential risks. Format each risk as follows:

  ---
### [Risk name here]

**Description:** 
[Detailed explanation]

**Potential Impact:** 
[Brief description of the impact]

**Mitigation Strategies:** 
[1-2 possible strategies]

Ensure there is a blank line between each section of the risk description.

Project Description:
${projectDescription}

Please identify at least 5 significant risks, ensuring a diverse range of risk types.`

  try {
    const completion = await openai.chat.completions.create({
      model: 'gpt-4o-mini',
      messages: [
        { role: 'system', content: system_prompt },        
        { role: 'user', content: user_prompt },
      ],
      max_tokens: 3000,
    })

    const risksText = completion.choices[0].message.content.trim()
    const risks = risksText.split('\n\n').map(risk => risk.trim()).filter(risk => risk !== '')

    return { risks }
  } catch (error) {
    console.error('OpenAI API error:', error)
    throw createError({
      statusCode: 500,
      statusMessage: 'Failed to generate risks. Please try again.',
    })
  }
})