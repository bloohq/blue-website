import OpenAI from 'openai'

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
})

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const { inputText } = body

  if (!inputText) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Input text is required',
    })
  }

  try {
    const completion = await openai.chat.completions.create({
      model: 'gpt-4o-mini',
      messages: [
        { role: 'system', content: 'You are an expert system that extracts action points and next steps from text. For each action point, provide a short, bolded step name followed by a colon and a description. Include who should do the step and by when, if applicable.' },
        { role: 'user', content: `Please analyze the following text and extract action points and next steps. Format each point as "**Step Name:** Step description (including who and when, if applicable)":\n\n${inputText}` },
      ],
      max_tokens: 1000,
    })

    const actionPointsText = completion.choices[0].message.content.trim()
    const actionPoints = actionPointsText.split('\n').map(point => point.trim()).filter(point => point !== '')

    return { actionPoints }
  } catch (error) {
    console.error('OpenAI API error:', error)
    throw createError({
      statusCode: 500,
      statusMessage: 'Failed to extract action points. Please try again.',
    })
  }
})