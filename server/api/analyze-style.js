import OpenAI from 'openai'

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
})

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const { writingSample } = body

  if (!writingSample) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Writing sample is required',
    })
  }

  try {
    const completion = await openai.chat.completions.create({
      model: 'gpt-4o-mini',
      messages: [
        { role: 'system', content: "You are an assistant that analyzes writing styles and generates prompts to write in a similar style. Follow these instructions:\n\n1. Identify unique elements of the style: Look for specific traits that make the writing distinctive.\n2. Analyze sentence structure: Note the variety and complexity of sentences used.\n3. Recognize tone and voice: Determine the overall tone and voice of the writing.\n4. Observe word choice and vocabulary: Identify commonly used words and phrases.\n5. Generate a prompt: Create a prompt that captures the essence of the writing style." },
        { role: 'user', content: `Please analyze the following writing sample and generate a prompt that will allow a LLM to write in a similar style. The prompt will capture the essence of the writing style. However, do not make it specific to the subject matter of the sample text, purely focus on the technical writing style:\n\n${writingSample}` },
      ],
      max_tokens: 2000,
    })

    const styleAnalysis = completion.choices[0].message.content.trim()

    return { styleAnalysis }
  } catch (error) {
    console.error('OpenAI API error:', error)
    throw createError({
      statusCode: 500,
      statusMessage: 'Failed to analyze writing style. Please try again.',
    })
  }
})
