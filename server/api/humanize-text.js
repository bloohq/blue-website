import OpenAI from 'openai'

const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
})

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const { aiText } = body

  if (!aiText) {
    throw createError({
      statusCode: 400,
      statusMessage: 'AI-generated text is required',
    })
  }

  try {
    const completion = await openai.chat.completions.create({
      model: 'gpt-4o-mini',
      messages: [
        { role: 'system', content: "You are an assistant that converts AI-generated text into more natural, human-like language. Follow these instructions:\n\n1. Use adverbs sparingly: Only include adverbs when they add significant value to the sentence.\n2. Choose analogies over metaphors: Analogies are more straightforward and relatable for readers.\n3. Integrate natural collocations: Use word combinations that sound natural and are commonly used together.\n4. Vary sentence length: Alternate between short and long sentences to create a more engaging rhythm.\n5. Be clear and concise: Avoid unnecessary words and be straightforward.\n6. Use a conversational tone: Write as if you are speaking directly to the reader.\n7. Use active voice: Prefer active voice over passive voice to make sentences more direct and dynamic.\n8. Avoid jargon: Use simple, everyday language that anyone can understand.\n9. Engage the reader: Ask rhetorical questions, use direct address, and include interesting facts or anecdotes.\n10. Maintain smooth flow between sentences and paragraphs: Ensure each sentence and paragraph transitions naturally to the next.\n11. Maintain appropriate perplexity: Ensure the text complexity is suitable for the target audience, neither too simple nor too complex.\n12. Incorporate burstiness: Include variations in sentence length and structure to make the text more dynamic and engaging." },
        { role: 'user', content: `Please humanize the following text:\n\n${aiText}` },
      ],
      max_tokens: 2000,
    })

    const humanizedText = completion.choices[0].message.content.trim()

    return { humanizedText }
  } catch (error) {
    console.error('OpenAI API error:', error)
    throw createError({
      statusCode: 500,
      statusMessage: 'Failed to humanize text. Please try again.',
    })
  }
})
