const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      typography: (theme) => ({
        DEFAULT: {
          css: {
            p: {
              fontSize: '1.125rem', // 18px
            },
            ol: {
              fontSize: '1.125rem', // 18px
            },
            ul: {
              fontSize: '1.125rem', // 18px
            },
            a: {
              color: theme('colors.brand-blue.DEFAULT'),
              textDecoration: 'none',
              '&:hover': {
                textDecoration: 'underline',
              },
            },
            'h2 a, h3 a, h4 a, h5 a, h6 a': {
              color: 'inherit',
              textDecoration: 'none',
              '&:hover': {
                textDecoration: 'none',
              },
            },
            '.text-primary': {
              color: theme('colors.brand-blue.DEFAULT'),
            },
            // Add these lines for table centering
            'tbody td, thead th': {
              textAlign: 'center',
            },
            // Add image centering
            img: {
              marginLeft: 'auto',
              marginRight: 'auto',
            },
          },
        },
      }),
      colors: {
        'brand-blue': {
          50: '#f0fbff',
          100: '#e0f5fe',
          200: '#b9edfe',
          300: '#7ce0fd',
          400: '#36d2fa',
          500: '#0cbdeb',
          600: '#00a0d2',
          700: '#017aa3',
          800: '#066686',
          900: '#0b546f',
          950: '#07364a',
          DEFAULT: '#00a0d2', // This makes 'brand-blue' equivalent to 'brand-blue-600'
        },
        // Ensure coolGray is included
        gray: defaultTheme.colors.coolGray
      },
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    function({ addBase, theme }) {
      addBase({
        'ul:not(.list-none) > li::marker': {
          color: theme('colors.gray'),
        },
        'html': { scrollBehavior: 'smooth' },
        'ol:not(.list-none) > li::marker': {
          color: theme('colors.gray'),
        },
      });
    },
  ],
};
