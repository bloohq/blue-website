// middleware/removeTrailingSlash.js
export default defineNuxtRouteMiddleware((to, from) => {
  // Check if the path is not the root path and has a trailing slash
  if (to.path !== '/' && to.path.endsWith('/')) {
    const { path, query, hash } = to;

    // If the path is /resources/, do not redirect
    if (path === '/resources/') {
      return;
    }

    // Remove the trailing slash
    const nextPath = path.replace(/\/+$/, '') || '/';
    const nextRoute = { path: nextPath, query, hash };
    return navigateTo(nextRoute, { redirectCode: 301 });
  }
});
